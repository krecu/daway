<?php

/**
 * Template preprocess function for socials_links.
 */
function template_preprocess_social_links(&$vars, $hook) {
	$config = social_api_get_config();
	$providers = array();
	foreach ($config['providers'] as $key => $provider) {

		$attributes = array(
			'target' => '_blank',
			'onClick' => 'popupWin = window.open(this.href, "contacts", "location,width=400,height=300"); popupWin.focus(); return false;',
		);


		if ($provider['enabled']) $providers[] = l($key, 'social/callback/'.$key.'/login', array('attributes' => $attributes));
	}


	$vars['providers'] = $providers;
}
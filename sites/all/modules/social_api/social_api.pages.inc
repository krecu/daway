<?php

function social_api_page_endpoint(){
	if ($path = libraries_get_path('hybridauth')) {
		try {
			require_once $path . '/index.php';
		} catch(Exception $e) {
			watchdog_exception('Social API', $e);
		}
	}
}

function social_api_page_callback($provider, $op){
	try  {
		$hybridauth = social_api_get_instance();

		if (is_object($hybridauth)) {

			switch ($op) {
				case 'logoutall' :
					$hybridauth->logoutAllProviders();
					break;
				case 'login' :
					$adapter = $hybridauth->authenticate($provider);
					$profile = $adapter->getUserProfile();
					if ($profile) {
						$_SESSION['SOCIAL_API_LOGIN'] = TRUE;
						$data = social_api_process_auth($provider, $profile);

						if (!social_api_get_profile($data['uid'], $provider)) {
							$contacts = $adapter->getUserContacts();
							if ($contacts) {
								$contact_list = array();
								foreach ($contacts as $item) {
									$contact_list[] = (array)$item;
								}
								$data['data']['friends'] = $contact_list;
							} else {
								$data['data']['friends'] = array();
							}
							$data['data'] = serialize($data['data']);
							social_api_save_provider($data);
						}

						print '<script>window.opener.location.reload(false); window.self.close();</script>';
					} else {
						$_SESSION['SOCIAL_API_LOGIN'] = FALSE;
					}

					break;
				case 'contacts' :
					$adapter = $hybridauth->authenticate($provider);
					$contacts = $adapter->getUserContacts();
					break;
			}

		} else {
			watchdog_exception('Social API', $hybridauth);
			$error = $hybridauth;
		}

	} catch( Exception $e )  {
		watchdog_exception('Social API', $e);
		$error = $e->getCode();
	}

}
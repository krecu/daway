<?php


function template_preprocess_daway_trip_route_map(&$vars, $hook) {

  if ($path = libraries_get_path('gmap')) {
    drupal_add_js($path . '/gmap3.min.js');
  }

  $shema = daway_trip_get_points($vars['node']->nid, 'inline');
  $i = 0;
  foreach ($shema as $item) {
    if ($i == 0) {
      $vars['departure'] = $item;
    }
    elseif ($i == count($shema) - 1) {
      $vars['arrival'] = $item;
    }
    else {
      $vars['points'][] = $item;
    }
    $i++;

  }
//	$vars['departure'] = $shema['departure'];
//	$vars['arrival'] = $shema['arrival'];
//	$vars['points'] = $shema['points'];
  $vars['hash'] = md5(microtime());
}

function template_preprocess_daway_trip_route_shema(&$vars, $hook) {

  drupal_add_library('system', 'ui.slider');
  $shema = daway_trip_get_routes_by_node($vars['node']->nid);

  $parent = NULL;
  $backward = NULL;
  $thitherward = NULL;

  foreach ($shema as $item) {
    switch ($item['type']) {
      case 'thitherward' :
        $thitherward[] = $item;
        break;
      case 'backward' :
        $backward[] = $item;
        break;
      case 'parent' :
        $parent[] = $item;
        break;
    }
  }


  $vars['points'] = daway_trip_get_points($vars['node']->nid, 'inline');
  $vars['shema'] = $shema;
  $vars['parent'] = $parent;
  $vars['backward'] = $backward;
  $vars['thitherward'] = $thitherward;
}

function daway_trip_preprocess_node(&$vars) {

  if ($vars['type'] == 'trip') {

    // Load node by entity
    $node = $vars['elements']['#node'];
    $wrapper = entity_metadata_wrapper('node', $node);

    // Pre-collected class
    $class = array('node', 'trip', 'node-' . $node->nid);

    // Pre-collected global variables

    // Trip type
    $vars['trip_global_type'] = $wrapper->field_trip_type->value();

    // Get shema trip
    $vars['trip_global_shema'] = $wrapper->field_trip_shema->value();

    // Trip by route param
    if (isset($vars['route'])) {
      $route = routes_load($vars['route']['rid']);

      // Default link trip
      $vars['trip_global_link'] = l(geopoint_name($route->pid1, 'short') . ' → ' . geopoint_name($route->pid2, 'short'), 'node/' . $node->nid,
        array(
          'query' => array(
            'route' => $vars['route']['rid'],
            'date' => $vars['route']['date']
          )
        ));

      // Default trip date
      $vars['trip_global_date'] = $vars['route']['date'];
      // Default trip price
      $vars['trip_global_price'] = $vars['route']['price'];
      // Create theme price
      $vars['trip_theme_price'] = theme('daway_language_currency_field', array('value' => $vars['route']['price']));

      // If set user geo information get distance value
      if (isset($vars['route']['geo'])) {
        $distance =
          sqrt(
            (($vars['route']['geo']['user_lat'] - geopoint_load($route->pid1)->latitude) * ($vars['route']['geo']['user_lat'] - geopoint_load($route->pid2)->latitude)) +
            (($vars['route']['geo']['user_lng'] - geopoint_load($route->pid1)->longitude) * ($vars['route']['geo']['user_lng'] - geopoint_load($route->pid2)->longitude))
          ) * 100;

        // Set distance from point trip to user geo
        $vars['trip_global_distance'] = round($distance, 5);
      }
      else {
        $vars['trip_global_distance'] = 0;
      }

    }
    else {
      $vars['trip_global_link'] = 'Not set';
    }

    switch ($vars['elements']['#view_mode']) {
      case 'viewfull' :

        // Implements template
        $vars['theme_hook_suggestions'][] = 'node__trip__grid__' . $vars['trip_global_type'];

        // Collected class
        $class = array_merge($class, array('grid', 'viewfull'));

        // add class about trip type
        $class[] = 'type-' . $vars['trip_global_type'];

        // Get routes trip
        $wrapper_routes = $wrapper->field_trip_routes->value();
        $routes = array();
        foreach ($wrapper_routes as $wr) {
          $wrapper_wr = entity_metadata_wrapper('field_collection_item', $wr);
          $wrapper_wr_route = $wrapper_wr->field_ftr_route->value();
          $routes[$wrapper_wr_route->rid] = array(
            'type' => $wrapper_wr->field_ftr_type->value(),
            'route' => $wrapper_wr_route,
          );
        }


        $tgsil = array();
        foreach ($vars['trip_global_shema'] as $tgsi) {

          $tgsil[$tgsi->pid] = array(
            'data' => l(geopoint_name($tgsi->pid, 'short'), 'geopoint/' . $tgsi->pid),
            'class' => array('point')
          );
        }

        if ($routes[$route->rid]['type'] == 'backward') {
          $tgsil = array_reverse($tgsil, TRUE);
        }

        $tgsi_class = FALSE;
        foreach ($tgsil as $pid => $tgsi) {
          if ($pid == $route->pid1) {
            $tgsi_class = TRUE;
          }
          if ($pid == $route->pid2) {
            $tgsil[$pid]['class'][] = 'active';
            break;
          }
          if ($tgsi_class) {
            $tgsil[$pid]['class'][] = 'active';
          }

        }

        $vars['trip_theme_shema'] = theme('item_list', array(
          'items' => $tgsil,
          'attributes' => array('class' => array('route'))
        ));

        switch ($vars['trip_global_type']) {
          case 'driver' :

            // Pre load user
            $vars['trip_global_driver'] = user_load($node->uid);
            $vars['trip_theme_driver'] = render(user_view($vars['trip_global_driver'], 'tripviewfull'));

            // Trip passenger count
            $vars['trip_global_passenger'] = $wrapper->field_trip_passenger->value();

            // Constract extra options
            $trip_extra = daway_trip_get_extra($vars['node']);

            if (arg(0) == 'routes') {
              $variables = get_defined_vars();
              if (($variables['route']->pid1 == $route->pid1) && ($variables['route']->pid2 == $route->pid2)) {
                $trip_extra['field_fte_line'] = array(
                  'label' => t('Прямая поездка'),
                  'value' => t('Прямая поездка'),
                  'class' => 'true',
                );
              }
            }

            $vars['trip_global_extra'] = theme('daway_extra_list', array('items' => $trip_extra));

            // Pre load user
            $body = $wrapper->body->value();
            if (!empty($body['value'])) {
              $vars['trip_global_body'] = text_summary($body['value'], 'plain_text', 70) . '...';
            }
            else {
              $vars['trip_global_body'] = '';
            }

            break;

          case 'passenger' :

            // Pre load user
            $vars['trip_global_passenger'] = user_load($node->uid);
            $vars['trip_theme_passenger'] = render(user_view($vars['trip_global_passenger'], 'tripviewfull'));

            // Constract extra options
            $trip_extra = daway_trip_get_extra($vars['node']);
            $vars['trip_global_extra'] = theme('daway_extra_list', array('items' => $trip_extra));

            // Pre load user
            $body = $wrapper->body->value();
            if (!empty($body['value'])) {
              $vars['trip_global_body'] = text_summary($body['value'], 'plain_text', 100) . '...';
            }
            else {
              $vars['trip_global_body'] = '';
            }
            break;
        }

        break;

      case 'viewsmall' :

        // Implements template
        $vars['theme_hook_suggestions'][] = 'node__trip__inline__' . $vars['trip_global_type'];

        // Collected class
        $class = array_merge($class, array('inline', 'viewsmall'));

        // Load node by entity
        $node = $vars['elements']['#node'];
        $wrapper = entity_metadata_wrapper('node', $node);

        // add class about trip type
        $class[] = 'type-' . $vars['trip_global_type'];

        // Trip type
        $vars['trip_global_type'] = $wrapper->field_trip_type->value();

        // Trip passenger count
        $vars['trip_global_passenger'] = $wrapper->field_trip_passenger->value();

        // Trip by route param
        if (isset($vars['route'])) {
          $route = routes_load($vars['route']['rid']);

          // Default link trip
          $vars['trip_global_link'] = l(geopoint_name($route->pid1, 'short') . ' →</br> ' . geopoint_name($route->pid2, 'short'), 'node/' . $node->nid,
            array(
              'query' => array(
                'route' => $vars['route']['rid'],
                'date' => $vars['route']['date']
              ),
              'html' => TRUE
            ));

          // Default trip date
          $vars['trip_global_date'] = $vars['route']['date'];
          // Default trip price
          $vars['trip_global_price'] = $vars['route']['price'];
          // Default global route
          $vars['trip_global_route'] = $vars['route']['rid'];
          // Create theme price
          $vars['trip_theme_price'] = theme('daway_language_currency_field', array('value' => $vars['route']['price']));

          $class[] = 'route-' . $vars['route']['rid'];

          // If set user geo information get distance value
          if (isset($vars['route']['geo'])) {

            $vars['route']['geo']['user_lat'] = $vars['route']['geo']['user_lat'] * 0.1;
            $vars['route']['geo']['user_lng'] = $vars['route']['geo']['user_lng'] * 0.1;

            $distance =
              sqrt(
                (($vars['route']['geo']['user_lat'] - geopoint_load($route->pid1)->latitude) * ($vars['route']['geo']['user_lat'] - geopoint_load($route->pid2)->latitude)) +
                (($vars['route']['geo']['user_lng'] - geopoint_load($route->pid1)->longitude) * ($vars['route']['geo']['user_lng'] - geopoint_load($route->pid2)->longitude))
              ) * 100;

            // Set distance from point trip to user geo
            $vars['trip_global_distance'] = ceil($vars['route']['geo']['userdistance'] * 1000000);
          }
          else {
            $vars['trip_global_distance'] = 0;
          }

        }
        else {
          $vars['trip_global_link'] = 'Not set';
        }

        break;

      case 'favorite' :

        // Implements template
        $vars['theme_hook_suggestions'][] = 'node__trip__favorite';

        // Collected class
        $class = array_merge($class, array('inline', 'viewsmall'));

        // Load node by entity
        $node = $vars['elements']['#node'];
        $wrapper = entity_metadata_wrapper('node', $node);

        // add class about trip type
        $class[] = 'type-' . $vars['trip_global_type'];

        // Trip type
        $vars['trip_global_type'] = $wrapper->field_trip_type->value();

        // Trip passenger count
        $vars['trip_global_passenger'] = $wrapper->field_trip_passenger->value();

        // Trip by route param

        $routes = daway_trip_get_routes_tree_by_node($vars['node']);
        $route = $routes['parent'];
        if (isset($route)) {
          // Default link trip
          $vars['trip_global_link'] = l(
            geopoint_name($route['route']->pid1, 'short') . ' →</br> ' . geopoint_name($route['route']->pid2, 'short'), 'node/' . $node->nid,
            array(
              'html' => TRUE
            ));

          // Default trip date
          $vars['trip_global_date'] = $route['date'][0]['value'];
          // Default trip price
          $vars['trip_global_price'] = $route['price'];
          // Create theme price
          $vars['trip_theme_price'] = theme('daway_language_currency_field', array('value' => $route['price']));

        }
        else {
          $vars['trip_global_link'] = 'Not set';
        }

        break;


      case 'full' :
        drupal_add_js(drupal_get_path('module', 'daway_trip') . '/daway_trip.js');
        $vars['trip_map'] = theme('daway_trip_route_map', array('node' => $vars['node']));

        $trip_extra = daway_trip_get_extra($vars['node']);
        $vars['trip_extra'] = theme('daway_extra_list', array('items' => $trip_extra));

        $routes = daway_trip_get_routes_tree_by_node($vars['node']);
        $vars['trip_search_link'] = l(geopoint_name($routes['parent']['route']->pid1, 'short') . ' → ' . geopoint_name($routes['parent']['route']->pid2, 'short'), 'routes/' . $routes['parent']['route']->rid);

        $vars['trip_route'] = theme('daway_trip_route_shema', array('node' => $vars['node']));


        if (isset($vars['node']->field_trip_car['und']) && ($vars['node']->field_trip_car['und'][0]['target_id'] != '')) {
          $vars['trip_car'] = theme('daway_user_cars', array(
            'cars' => array($vars['node']->field_trip_car['und'][0]['target_id']),
            'view_mode' => 'trip'
          ));
        }
        else {
          $vars['trip_car'] = '';
        }
        $vars['trip_route_info'] = array(
          'distance' => $routes['parent']['route']->distance,
          'duration' => $routes['parent']['route']->duration,
        );

        // if user is owner then create user list who contacted him
        global $user;
        if ($node->uid == $user->uid) {
          $trip_contact_user = theme('daway_contact_user_list', array('nid' => $node->nid));
          $vars['trip_contact_user'] = $trip_contact_user;
        }
        else {
          $vars['trip_contact_user'] = '';
        }


        break;

      case 'contact' :
        $class[] = 'viewsmall trip contact';
        $class[] = 'type-' . $vars['trip_global_type'];
        // Pre load user
        $vars['trip_global_user'] = user_load($node->uid);
        $vars['trip_theme_user'] = render(user_view($vars['trip_global_user'], 'tripviewfull'));

        break;

    }


    $points = daway_trip_get_points($vars['node']->nid, 'inline');


    $vars['class'] = implode(' ', $class);
  }
}


function daway_trip_preprocess_geopoint(&$vars) {
//	dsm($_SESSION['USER_FILTERS']);
//	dsm(time());
  if (arg(0) == 'admin') {
    return;
  }

  $geopoint = $vars['content']['geopoint'];

  $radius = daway_trip_get_default_radius($geopoint->pid);

  $only = isset($_GET['only']) ? $_GET['only'] : FALSE;

  // задаем стиль отображения по умолчанию
  $display = variable_get('DAWAY_GEOPOINT_LIST_DISPLAY', 'viewsmall');
  // задаем дату по умолчанию на текущий момент
  $date = time();
  // задаем период даты по умолчанию
  $dateend = variable_get('DAWAY_GEOPOINT_LIST_DATEEND', 'all');
  // задаем диапозон цен по умолчанию
  $price = FALSE;
  // задаем диапозон свободных мест по умолчанию
  $seats = FALSE;
  // задаем параметры багажа по умолчанию
  $baggage = 'all';
  // задаем параметры EXTRA по умолчанию
  $extra = FALSE;

  // если пользователь уже просматривал эту страницу то проверяем

  $path = explode('/', $_GET['q']);
  $path = $path[0] . '/' . $path[1];
//	unset($_SESSION['USER_FILTERS']);

  if (isset($_SESSION['USER_FILTERS'][$path])) {
    $filters = $_SESSION['USER_FILTERS'][$path];

    // применял ли он стиль отображения
    if (isset($filters['display'])) {
      $display = (!empty($filters['display'])) ? $filters['display'] : $display;
    }
    // применял ли он дату
    if (isset($filters['date'])) {
      $date = (!empty($filters['date'])) ? $filters['date'] : $date;
    }

    // применял ли он конечную дату
    if (isset($filters['dateend'])) {
      $dateend = (!empty($filters['dateend'])) ? $filters['dateend'] : $dateend;
    }

    // применял ли он дату
    if (isset($filters['r1'])) {
      $radius = (!empty($filters['r1'])) ? $filters['r1'] : $radius;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['price'])) {
      $price = (!empty($filters['price'])) ? $filters['price'] : $price;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['seats'])) {
      $seats = (!empty($filters['seats'])) ? $filters['seats'] : $seats;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['baggage'])) {
      $baggage = (!empty($filters['baggage'])) ? $filters['baggage'] : $baggage;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['extra'])) {
      $extra = (!empty($filters['extra'])) ? $filters['extra'] : $extra;
    }
  }

  $vars['display'] = $display;
  $vars['geopoint'] = $geopoint;
  $vars['date'] = $date;
  $vars['dateend'] = $dateend;
  $vars['extra'] = $extra;
  $vars['price'] = $price;
  $vars['seats'] = $seats;
  $vars['baggage'] = $baggage;


  // собираем все поездки
  $query = db_select('node', 'n')->fields('n', array('nid', 'title', 'uid'))->condition('n.type', 'trip')
    ->extend('PagerDefault');

  // собираем все поля направления в поездках
  $query->innerJoin('field_data_field_trip_routes', 'route_field', 'route_field.entity_id = n.nid');

  // собираем все типы направлений в поездках
  $query->innerJoin('field_data_field_ftr_type', 'route_type', 'route_type.entity_id = route_field.field_trip_routes_value');
  $query->addField('route_type', 'field_ftr_type_value', 'type');

  // собираем все направления по полям
  $query->innerJoin('field_data_field_ftr_route', 'route_id', 'route_id.entity_id = route_field.field_trip_routes_value');
  $query->innerJoin('routes', 'r', 'r.rid = route_id.field_ftr_route_target_id');
  $query->addField('r', 'name', 'name');
  $query->addField('r', 'rid', 'rid');

  // собираем поля дат поездок
  $query->innerJoin('field_data_field_ftr_datedeparture', 'date', 'date.entity_id = route_field.field_trip_routes_value');
  $query->addField('date', 'field_ftr_datedeparture_value', 'datestart');
  $query->addField('date', 'field_ftr_datedeparture_value2', 'dateend');

  // собираем поля цен поездок
  $query->innerJoin('field_data_field_ftr_price', 'price', 'price.entity_id = route_field.field_trip_routes_value');
  $query->addField('price', 'field_ftr_price_value', 'price');

  // собираем пользователей
  $query->innerJoin('users', 'user', 'user.uid = n.uid');

  // собираем значения рейтинга пользователей
  $query->innerJoin('field_data_field_user_rate', 'rate', 'rate.entity_id = user.uid');

  // собираем EXTRA поля
  $query->leftJoin('field_data_field_trip_extra', 'extra_field', 'extra_field.entity_id = n.nid');

  // собираем значение типа поездки
  $query->innerJoin('field_data_field_trip_type', 'type', 'type.entity_id = n.nid');

  // собираем значения количества мест в поездке
  $query->innerJoin('field_data_field_trip_passenger', 'seat', 'seat.entity_id = n.nid');
  $query->addField('seat', 'field_trip_passenger_value', 'seat');


  if ($only) {
    // || (($geopoint->type != 'street_address') && ($geopoint->type != 'street_number') && ($geopoint->type != 'route'))
    // select points items by index
//		$query->innerJoin('geopoint', 'gi');
//		$query->innerJoin('geopoint_index', 'gii', 'gii.pid = gi.pid');
//		$query->condition('gii.pid_original', $geopoint->pid);
//		$query->innerJoin('geopoint', 'g', '(g.pid = r.pid1 OR g.pid = r.pid2) AND g.pid = gi.pid');
    $query->innerJoin('geopoint_index', 'g', 'g.pid = r.pid1 OR g.pid = r.pid2');
    $query->condition('g.pid_original', $geopoint->pid);
    $query->condition('g.pid', $geopoint->pid, '!=');
  }
  else {
    // select points items
    $query->innerJoin('geopoint', 'g', 'g.pid = r.pid1 OR g.pid = r.pid2');
    $query->addExpression(
      '
      CEIL(
          sqrt(
              ((' . $geopoint->latitude . ' - g.latitude)   * (' . $geopoint->latitude . ' - g.latitude)) +
					((' . $geopoint->longitude . ' - g.longitude) * (' . $geopoint->longitude . ' - g.longitude))
				) * 100
			)
			'
      , 'distance');

    // Потомучто с алиасами работать можно только через GROUP BY HAVING и ORDER ))) 3 часа на решение ушло(((
    $query->havingCondition('distance', $radius, '<=');
  }


  if (arg(2)) {
    switch (arg(2)) {
      case 'drivers' :
        $query->condition('type.field_trip_type_value', 'driver');
        break;
      case 'passengers' :
        $query->condition('type.field_trip_type_value', 'passenger');
        break;
      default :
        drupal_not_found();
    }
  }


  // накладываем фильтр по датам
  if ($dateend == 'all') {
    $query->condition('date.field_ftr_datedeparture_value', $date, '>=');
  }
  else {
    $query->condition('date.field_ftr_datedeparture_value', array($date, $date + $dateend * 84000), 'BETWEEN');
  }


  // накладываем фильтр по цене
  if ($price) {
    $price = explode(',', $price);
    if (!(($price[0] == 0) && ($price[1] == 0))) {
      $query->condition('price.field_ftr_price_value', array($price[0], $price[1]), 'BETWEEN');
    }
  }

  // накладываем фильтр по количеству свободных мест
  if ($seats) {
    $query->condition('seat.field_trip_passenger_value', $seats);
  }

  // накладываем фильтр по багажу
  if ($baggage) {
    if ($baggage != 'all') {
      $query->innerJoin('field_data_field_fte_baggage', 'baggage_field', 'baggage_field.entity_id = extra_field.field_trip_extra_value');
      $query->condition('baggage_field.field_fte_baggage_value', $baggage);
    }
  }

  // накладываем фильтр по EXTRA полям
  if ($extra = explode(',', $extra)) {
    if (count($extra) > 0) {
      foreach ($extra as $extra_field) {
        if ($extra_field == '') {
          continue;
        }
        $extra_field = explode(':', $extra_field);
        $extra_field_val = $extra_field[1];
        $extra_field = $extra_field[0];
        $query->innerJoin('field_data_' . $extra_field, $extra_field . '_field', $extra_field . '_field.entity_id = extra_field.field_trip_extra_value');
        $query->condition($extra_field . '_field.' . $extra_field . '_value', $extra_field_val);
      }
    }
  }

  //dsm((string)$query);
  $query->orderBy('datestart', 'ASC');
  $query->groupBy('datestart');
  $query->groupBy('route_field.field_trip_routes_value');
  $result = $query->limit(variable_get('DAWAY_GEOPOINT_LIST_PERPAGE', 20))->execute();

  $trips = array();
  foreach ($result as $key => $item) {
    $node = node_load($item->nid);
    $data = array(
      'rid' => $item->rid,
      'date' => $item->datestart,
      'price' => $item->price,
    );
    $node->route = $data;

    $node_view_tmp = node_view($node, $display);
    $trips[] = render($node_view_tmp);
  }

  $vars['trips'] = $trips;
}


function daway_trip_preprocess_routes(&$vars) {
//	dsm($_SESSION['USER_FILTERS']);
//	dsm(time());
  if (arg(0) == 'admin') {
    return;
  }

  $routes = $vars['content']['routes'];

  $pid1 = geopoint_load($routes->pid1);
  $pid2 = geopoint_load($routes->pid2);

  $r1 = ceil(geopoint_get_radius($routes->pid1) / 1000);
  $r2 = ceil(geopoint_get_radius($routes->pid2) / 1000);

  // задаем стиль отображения по умолчанию
  $display = variable_get('DAWAY_GEOPOINT_LIST_DISPLAY', 'viewsmall');
  // задаем дату по умолчанию на текущий момент
  $date = time();
  // задаем период даты по умолчанию
  $dateend = variable_get('DAWAY_GEOPOINT_LIST_DATEEND', 'all');
  // задаем диапозон цен по умолчанию
  $price = FALSE;
  // задаем диапозон свободных мест по умолчанию
  $seats = FALSE;
  // задаем параметры багажа по умолчанию
  $baggage = 'all';
  // задаем параметры EXTRA по умолчанию
  $extra = FALSE;

  // если пользователь уже просматривал эту страницу то проверяем

  $path = explode('/', $_GET['q']);
  $path = $path[0] . '/' . $path[1];
//	unset($_SESSION['USER_FILTERS']);

  if (isset($_SESSION['USER_FILTERS'][$path])) {
    $filters = $_SESSION['USER_FILTERS'][$path];

    // применял ли он стиль отображения
    if (isset($filters['display'])) {
      $display = (!empty($filters['display'])) ? $filters['display'] : $display;
    }
    // применял ли он дату
    if (isset($filters['date'])) {
      $date = (!empty($filters['date'])) ? $filters['date'] : $date;
    }

    // применял ли он дату
    if (isset($filters['r1'])) {
      $r1 = (!empty($filters['r1'])) ? $filters['r1'] : $r1;
    }

    // применял ли он дату
    if (isset($filters['r2'])) {
      $r2 = (!empty($filters['r2'])) ? $filters['r2'] : $r2;
    }

    // применял ли он конечную дату
    if (isset($filters['dateend'])) {
      $dateend = (!empty($filters['dateend'])) ? $filters['dateend'] : $dateend;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['price'])) {
      $price = (!empty($filters['price'])) ? $filters['price'] : $price;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['seats'])) {
      $seats = (!empty($filters['seats'])) ? $filters['seats'] : $seats;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['baggage'])) {
      $baggage = (!empty($filters['baggage'])) ? $filters['baggage'] : $baggage;
    }

    // применял ли он фильтр по ценам
    if (isset($filters['extra'])) {
      $extra = (!empty($filters['extra'])) ? $filters['extra'] : $extra;
    }
  }

  $vars['display'] = $display;
  $vars['routes'] = $routes;
  $vars['pid1'] = $pid1;
  $vars['pid2'] = $pid2;
  $vars['r1'] = $r1;
  $vars['r2'] = $r2;
  $vars['date'] = $date;
  $vars['dateend'] = $dateend;
  $vars['extra'] = $extra;
  $vars['price'] = $price;
  $vars['seats'] = $seats;
  $vars['baggage'] = $baggage;


  // собираем все поездки
  $query = db_select('node', 'n')->fields('n', array('nid', 'title', 'uid'))->condition('n.type', 'trip')
    ->extend('PagerDefault');

  // собираем пользователей
  $query->innerJoin('users', 'user', 'user.uid = n.uid');

  // собираем значения рейтинга пользователей
  $query->innerJoin('field_data_field_user_rate', 'rate', 'rate.entity_id = user.uid');

  // собираем EXTRA поля
  $query->leftJoin('field_data_field_trip_extra', 'extra_field', 'extra_field.entity_id = n.nid');

  // собираем значение типа поездки
  $query->innerJoin('field_data_field_trip_type', 'type', 'type.entity_id = n.nid');

  // собираем значения количества мест в поездке
  $query->innerJoin('field_data_field_trip_passenger', 'seat', 'seat.entity_id = n.nid');
  $query->addField('seat', 'field_trip_passenger_value', 'seat');

  // собираем все поля направления в поездках
  $query->innerJoin('field_data_field_trip_routes', 'route_field', 'route_field.entity_id = n.nid');

  // собираем все типы направлений в поездках
  $query->innerJoin('field_data_field_ftr_type', 'route_type', 'route_type.entity_id = route_field.field_trip_routes_value');
  $query->addField('route_type', 'field_ftr_type_value', 'type');

  // собираем все направления по полям
  $query->innerJoin('field_data_field_ftr_route', 'route_id', 'route_id.entity_id = route_field.field_trip_routes_value');
  $query->innerJoin('routes', 'r', 'r.rid = route_id.field_ftr_route_target_id');
  $query->addField('r', 'name', 'name');
  $query->addField('r', 'rid', 'rid');

  // собираем поля дат поездок
  $query->innerJoin('field_data_field_ftr_datedeparture', 'date', 'date.entity_id = route_field.field_trip_routes_value');
  $query->addField('date', 'field_ftr_datedeparture_value', 'datestart');
  $query->addField('date', 'field_ftr_datedeparture_value2', 'dateend');

  // собираем поля цен поездок
  $query->innerJoin('field_data_field_ftr_price', 'price', 'price.entity_id = route_field.field_trip_routes_value');
  $query->addField('price', 'field_ftr_price_value', 'price');

//	$query->addExpression('MIN(price.field_ftr_price_value)', 'minprice');
//	$query->addExpression('MAX(price.field_ftr_price_value)', 'maxprice');

  // собираем все геопоинты поездок по PID1
  $query->innerJoin('geopoint', 'p1', 'p1.pid = r.pid1');
  $query->addField('p1', 'pid', 'route_pid1');
  $query->addField('p1', 'short', 'route_name1');

  // вычисляем дистанцию от точки поиска до точки в базе
  $query->addExpression(
    '
    CEIL(
        sqrt(
            ((' . $pid1->latitude . ' - p1.latitude)   * (' . $pid1->latitude . ' - p1.latitude)) +
					((' . $pid1->longitude . ' - p1.longitude) * (' . $pid1->longitude . ' - p1.longitude))
				) * 100
			)
			'
    , 'distancetopid1');


  // собираем все геопоинты поездок по PID2
  $query->innerJoin('geopoint', 'p2', 'p2.pid = r.pid2');
  $query->addField('p2', 'pid', 'route_pid2');
  $query->addField('p2', 'short', 'route_name2');

  // вычисляем дистанцию от точки поиска до точки в базе
  $query->addExpression(
    '
    CEIL(
        sqrt(
            ((' . $pid2->latitude . ' - p2.latitude)   * (' . $pid2->latitude . ' - p2.latitude)) +
					((' . $pid2->longitude . ' - p2.longitude) * (' . $pid2->longitude . ' - p2.longitude))
				) * 100
			)
			'
    , 'distancetopid2');


  $query->havingCondition('distancetopid1', $r1, '<=');
  $query->havingCondition('distancetopid2', $r2, '<=');


  if (arg(2)) {
    switch (arg(2)) {
      case 'drivers' :
        $query->condition('type.field_trip_type_value', 'driver');
        break;
      case 'passengers' :
        $query->condition('type.field_trip_type_value', 'passenger');
        break;
      default :
        drupal_not_found();
    }
  }


  // накладываем фильтр по датам
  if ($dateend == 'all') {
    $query->condition('date.field_ftr_datedeparture_value', $date, '>=');
  }
  else {
    $query->condition('date.field_ftr_datedeparture_value', array($date, $date + $dateend * 84000), 'BETWEEN');
  }


  // накладываем фильтр по цене
  if ($price) {
    $price = explode(',', $price);
    if (!(($price[0] == 0) && ($price[1] == 0))) {
      $query->condition('price.field_ftr_price_value', array($price[0], $price[1]), 'BETWEEN');
    }
  }

  // накладываем фильтр по количеству свободных мест
  if ($seats) {
    $query->condition('seat.field_trip_passenger_value', $seats);
  }

  // накладываем фильтр по багажу
  if ($baggage) {
    if ($baggage != 'all') {
      $query->innerJoin('field_data_field_fte_baggage', 'baggage_field', 'baggage_field.entity_id = extra_field.field_trip_extra_value');
      $query->condition('baggage_field.field_fte_baggage_value', $baggage);
    }
  }

  // накладываем фильтр по EXTRA полям
  if ($extra = explode(',', $extra)) {
    if (count($extra) > 0) {
      foreach ($extra as $extra_field) {
        if ($extra_field == '') {
          continue;
        }
        $extra_field = explode(':', $extra_field);
        $extra_field_val = $extra_field[1];
        $extra_field = $extra_field[0];
        $query->innerJoin('field_data_' . $extra_field, $extra_field . '_field', $extra_field . '_field.entity_id = extra_field.field_trip_extra_value');
        $query->condition($extra_field . '_field.' . $extra_field . '_value', $extra_field_val);
      }
    }
  }

  //dsm((string)$query);
  $query->groupBy('datestart');
  $query->groupBy('route_field.field_trip_routes_value');

  $query->orderBy('datestart', 'ASC');

  $result = $query->limit(variable_get('DAWAY_GEOPOINT_LIST_PERPAGE', 20))->execute();

  $trips = array();
  $minprice = 0;
  $maxprice = 0;

  foreach ($result as $key => $item) {
    $node = node_load($item->nid);
    $data = array(
      'rid' => $item->rid,
      'date' => $item->datestart,
      'price' => $item->price,
    );

    $node->route = $data;


    if ($minprice > $item->price) {
      $minprice = ceil($item->price);
    }

    if ($maxprice < $item->price) {
      $maxprice = ceil($item->price);
    }


    $trips[] = render(node_view($node, $display));
  }


  drupal_add_js('
	jQuery(document).ready(function($){
		$(".r1.form-item").data("default", "' . $r1 . '");
		$(".r2.form-item").data("default", "' . $r2 . '");

		$(".date.form-item").data("default", "' . format_date($date, 'custom', 'm/d/Y') . '");
		$(".price.form-item").data("min", ' . $minprice . ');
		$(".price.form-item").data("max", ' . $maxprice . ');
	})
	', array('type' => 'inline'));

  $vars['trips'] = $trips;
}
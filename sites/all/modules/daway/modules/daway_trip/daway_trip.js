(function ($) {
    Drupal.behaviors.daway_trip = {
        attach: function($context, $settings) {

            var $tabs = $('#tabs', $context);

            $('a', $tabs).click(function(){
                $active = $(this);
                $('.tab', $context).removeClass('active');
                $('a', $tabs).removeClass('active');

                $active.addClass('active');
                $('.tab.'+$active.data('panel')).addClass('active');

                setTimeout(function(){
                $.event.trigger('resizeMapUpdate');
                }, 500)
                return false;
            });

        }
    }

})(jQuery);
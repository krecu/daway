<?php

/**
 * Provide mass operations for user trip page.
 * @return array
 */
function daway_trip_trip_mass_operations() {
  $operations = array(
    'publish' => array(
      'label' => t('Publish'),
      'callback' => 'daway_trip_trip_mass_update',
      'callback arguments' => array('updates' => array('status' => 0)),
    ),
    'unpublish' => array(
      'label' => t('Unpublish'),
      'callback' => 'daway_trip_trip_mass_update',
      'callback arguments' => array('updates' => array('status' => 1)),
    ),
  );
  return $operations;
}

/**
 * Callback for mass operations
 * @param $items
 * @param $op
 * @see daway_trip_trip_mass_operations
 */
function daway_trip_trip_mass_update($items, $op) {
  switch ($op['status']) {
    // publish trip
    case '0' :
      foreach ($items as $item) {
        $node = node_load($item);
        $node->status = NODE_PUBLISHED;
        node_save($node);
      }
      break;
    // unpublish trip
    case '1' :
      foreach ($items as $item) {
        $node = node_load($item);
        $node->status = NODE_NOT_PUBLISHED;
        node_save($node);
      }
      break;
  }
}

/**
 * Callback submit for mass operations
 * @param $form
 * @param $form_state
 * @see daway_trip_user_page_trips
 */
function daway_trip_trip_mass_op_submit($form, &$form_state) {
  $operations = daway_trip_trip_mass_operations();
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes
  $trips = array_filter($form_state['values']['trips']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($trips), $operation['callback arguments']);
    }
    else {
      $args = array($trips);
    }
    call_user_func_array($function, $args);
    cache_clear_all();
  }
  else {
    $form_state['rebuild'] = TRUE;
  }
}


/**
 * Implements hook_node_operations().
 */
function daway_trip_user_contacts_mass_operations() {
  $operations = array(
    'publish' => array(
      'label' => t('Publish'),
      'callback' => 'daway_trip_trip_mass_update',
      'callback arguments' => array('updates' => array('status' => 0)),
    ),
    'unpublish' => array(
      'label' => t('Spam'),
      'callback' => 'daway_trip_trip_mass_update',
      'callback arguments' => array('updates' => array('status' => 1)),
    ),
  );
  return $operations;
}


/**
 * page callback
 */
function daway_trip_user_page() {

  if (arg(2)) {
    switch (arg(2)) {
      case 'contacts' :
        return daway_trip_user_page_contacts();
        break;
      case 'rating' :
        return daway_trip_user_page_rating();
        break;
    }
  }
  elseif (arg(2) == '') {
    return daway_trip_user_page_trips();
  }
  else {
    drupal_not_found();
  }

}


/**
 * page callback
 */
function daway_trip_user_page_trips() {
  drupal_set_title(t('My trips'));

  global $user;
  $query = db_select('node', 'n')->extend('PagerDefault');
  $query->fields('n', array('nid'));
  $query->condition('n.uid', $user->uid);
  $query->condition('n.type', 'trip');

  // select field collection
  $query->innerJoin('field_data_field_trip_routes', 'route_field', 'route_field.entity_id = n.nid');

  // select route items
  $query->innerJoin('field_data_field_ftr_route', 'route_id', 'route_id.entity_id = route_field.field_trip_routes_value');
  $query->innerJoin('routes', 'r', 'r.rid = route_id.field_ftr_route_target_id');
  $query->addField('r', 'name', 'name');
  $query->addField('r', 'rid', 'rid');

  //select date
  $query->innerJoin('field_data_field_ftr_datedeparture', 'date', 'date.entity_id = route_field.field_trip_routes_value');
  $query->addField('date', 'field_ftr_datedeparture_value', 'datestart');
  $query->addField('date', 'field_ftr_datedeparture_value2', 'dateend');

  //select price
  $query->innerJoin('field_data_field_ftr_price', 'price', 'price.entity_id = route_field.field_trip_routes_value');
  $query->addField('price', 'field_ftr_price_value', 'price');

  $query->orderBy('datestart');
  $query->groupBy('n.nid');

  $query->limit(20);
  $result = $query->execute();

  $form['tabs'] = array(
    '#markup' => '
		<ul id="trips-tab" class="row">
			<li class="first">' . l(t('List trips'), 'user/trips') . '</li>
			<li class="last">' . l(t('Contacts users'), 'user/trips/contacts') . '</li>
			<li class="last">' . l(t('Comment/Rating'), 'user/trips/rating') . '</li>
		</ul>',
    '#weight' => -1
  );

  $header = array('view' => '');
  $options = array();
  foreach ($result as $key => $node) {
    $node_obj = node_load($node->nid);
    $nv_tmp = node_view($node_obj, 'admin');
    $options[$node_obj->nid] = array(
      'view' => array('data' => render($nv_tmp), 'class' => array('view')),
    );
  }
  $form['options'] = array(
    '#prefix' => '<div id="trip-operations" class="row">',
    '#suffix' => '</div>',
    '#weight' => 0,
  );

  $opt = array();
  foreach (daway_trip_trip_mass_operations() as $func => $item) {
    $opt[$func] = $item['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation') . ':',
    '#options' => $opt,
    '#default_value' => 'publish',
  );
  $form['options']['submit'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => array('daway_trip_trip_mass_op_submit'),
  );

  $form['trips'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => TRUE,
    '#sticky' => FALSE,
    '#empty' => t('Content not found'),
    '#prefix' => '<div id="trips-list">',
    '#suffix' => '</div>',
  );
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}


function daway_trip_user_page_contacts() {

  drupal_set_title(t('My contacts'));

  global $user;
  $query = db_select('users', 'u')->extend('PagerDefault');
  $query->fields('u', array('uid'));
  $query->condition('u.uid', $user->uid);
  $query->join('field_data_field_user_review', 'field_review', 'field_review.entity_id = u.uid');

  $query->leftJoin('field_data_field_fur_answer', 'review_answer', 'review_answer.entity_id = field_review.field_user_review_value');
  $query->addField('review_answer', 'field_fur_answer_value', 'review_answer');

  $query->leftJoin('field_data_field_fur_rate', 'review_rate', 'review_rate.entity_id = field_review.field_user_review_value');
  $query->addField('review_rate', 'field_fur_rate_value', 'review_rate');

  $query->leftJoin('field_data_field_fur_review', 'review_review', 'review_review.entity_id = field_review.field_user_review_value');
  $query->addField('review_review', 'field_fur_review_value', 'review_review');

  $query->leftJoin('field_data_field_fur_time', 'review_time', 'review_time.entity_id = field_review.field_user_review_value');
  $query->addField('review_time', 'field_fur_time_value', 'review_time');

  $query->leftJoin('field_data_field_fur_trip', 'review_trip', 'review_trip.entity_id = field_review.field_user_review_value');
  $query->addField('review_trip', 'field_fur_trip_target_id', 'review_trip');

  $query->leftJoin('field_data_field_fur_user', 'review_user', 'review_user.entity_id = field_review.field_user_review_value');
  $query->addField('review_user', 'field_fur_user_target_id', 'review_user');


//	$query->groupBy('review_user.field_fur_user_target_id');
  $query->orderBy('review_time.field_fur_time_value', 'DESC');
  $query->limit(20);
  $result = $query->execute();

  $form['tabs'] = array(
    '#markup' => '
		<ul id="trips-tab" class="row">
			<li class="first">' . l(t('List trips'), 'user/trips') . '</li>
			<li class="last">' . l(t('Contacts users'), 'user/trips/contacts') . '</li>
			<li class="last">' . l(t('Comment/Rating'), 'user/trips/rating') . '</li>
		</ul>',
    '#weight' => -1
  );


  $header = array('view' => '');
  $options = array();
  foreach ($result as $key => $review) {
    $data = array(
      'answer' => $review->review_answer,
      'rate' => ceil($review->review_rate),
      'review' => $review->review_review,
      'trip' => $review->review_trip,
      'user' => $review->review_user,
      'time' => ($review->review_time) ? $review->review_time : time(),
    );

    $options[$key] = array(
      'view' => array('data' => theme('daway_profile_contact_item', array('data' => $data)), 'class' => array('view')),
    );

  }
  $form['options'] = array(
    '#prefix' => '<div id="trip-operations" class="row">',
    '#suffix' => '</div>',
    '#weight' => 0,
  );

  $opt = array();
  foreach (daway_trip_user_contacts_mass_operations() as $func => $item) {
    $opt[$func] = $item['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation') . ':',
    '#options' => $opt,
    '#default_value' => 'publish',
  );
  $form['options']['submit'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
    '#type' => 'submit',
    '#value' => t('Apply'),
    '#submit' => array('daway_trip_trip_mass_op_submit'),
  );

  $options = array();
  $form['trips'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => TRUE,
    '#sticky' => FALSE,
    '#empty' => t('NEED PSD!!!'),
    '#prefix' => '<div id="trips-list">',
    '#suffix' => '</div>',
  );
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

function daway_trip_user_page_rating() {
  drupal_set_title(t('My rating'));

  global $user;
  $query = db_select('users', 'u')->extend('PagerDefault');
  $query->fields('u', array('uid'));
  $query->condition('u.uid', $user->uid);
  $query->join('field_data_field_user_review', 'field_review', 'field_review.entity_id = u.uid');

  $query->leftJoin('field_data_field_fur_answer', 'review_answer', 'review_answer.entity_id = field_review.field_user_review_value');
  $query->addField('review_answer', 'field_fur_answer_value', 'review_answer');

  $query->leftJoin('field_data_field_fur_rate', 'review_rate', 'review_rate.entity_id = field_review.field_user_review_value');
  $query->addField('review_rate', 'field_fur_rate_value', 'review_rate');

  $query->leftJoin('field_data_field_fur_review', 'review_review', 'review_review.entity_id = field_review.field_user_review_value');
  $query->addField('review_review', 'field_fur_review_value', 'review_review');

  $query->leftJoin('field_data_field_fur_time', 'review_time', 'review_time.entity_id = field_review.field_user_review_value');
  $query->addField('review_time', 'field_fur_time_value', 'review_time');

  $query->leftJoin('field_data_field_fur_trip', 'review_trip', 'review_trip.entity_id = field_review.field_user_review_value');
  $query->addField('review_trip', 'field_fur_trip_target_id', 'review_trip');

  $query->leftJoin('field_data_field_fur_user', 'review_user', 'review_user.entity_id = field_review.field_user_review_value');
  $query->addField('review_user', 'field_fur_user_target_id', 'review_user');


//	$query->groupBy('review_user.field_fur_user_target_id');
  $query->orderBy('review_time.field_fur_time_value', 'DESC');
  $query->limit(20);
  $result = $query->execute();

  $form['tabs'] = array(
    '#markup' => '
		<ul id="trips-tab" class="row">
			<li class="first">' . l(t('List trips'), 'user/trips') . '</li>
			<li class="last">' . l(t('Contacts users'), 'user/trips/contacts') . '</li>
			<li class="last">' . l(t('Comment/Rating'), 'user/trips/rating') . '</li>
		</ul>',
    '#weight' => -1
  );

  $header = array();
  $rows = array();
  foreach ($result as $key => $review) {

    $data = array(
      'answer' => $review->review_answer,
      'rate' => ceil($review->review_rate),
      'review' => $review->review_review,
      'trip' => $review->review_trip,
      'user' => $review->review_user,
      'time' => ($review->review_time) ? $review->review_time : time(),
    );

    $row = array();
    $row[] = theme('daway_profile_review_item', array('data' => $data));
    $rows[] = $row;
  }

  $table = theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));

  $table = "NEED PSD!!!";


  $form['trips'] = array(
    '#prefix' => '<div id="trips-list" style="padding: 15px 0 0;">',
    '#suffix' => '</div>',
    '#markup' => $table
  );

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}
<?php

function daway_trip_intercity_page(){

	$default_country = variable_get('DAWAY_DEFAULT_INTERCITY_COUNTRY', FALSE);
	$default_city = variable_get('DAWAY_DEFAULT_INTERCITY_CITY', FALSE);

	if (arg(1)) $default_country = arg(1);
	if (arg(2)) $default_city = arg(2);

	$data = daway_trip_get_routes_tree();

	$countries = geopoint_load_multiple(array_keys($data));
	if ($default_country) $countries[$default_country]->status = 'active';

	$city = array();
	$routes = array();

	if ($default_country) {
		$city = geopoint_load_multiple(array_keys($data[$default_country]));
		if ($default_city) {
			$routes = routes_load_multiple(array_keys($data[$default_country][$default_city]));
		} else {
			foreach ($data[$default_country] as $city_item) {
				$routes = array_merge($routes, array_keys($city_item));
			}
			$routes = routes_load_multiple($routes);
		}
	} else {
		foreach ($data as $country_item) {
			$city = array_merge($city, array_keys($country_item));
			foreach ($country_item as $city_item) {
				$routes = array_merge($routes, array_keys($city_item));
			}
		}
		$city = geopoint_load_multiple($city);
		$routes = routes_load_multiple($routes);
	}

	$routes_departure = array();
	$routes_arrival = array();
	if ($default_country) {
		if ($default_city) {
			foreach ($routes as $item) {
				if ($default_city == $item->pid1) {
					$routes_departure[] = $item;
				} else {
					$routes_arrival[] = $item;
				}
			}
		} else {
			if (count($routes) > 2) {
				$items = array_chunk($routes, ceil(count($routes))/2);
				$routes_departure = $items[0];
				$routes_arrival = $items[1];
			} else {
				$routes_departure = $routes;
				$routes_arrival = array();
			}

		}
	} else {
		if (count($routes) > 2) {
			$items = array_chunk($routes, ceil(count($routes))/2);
			$routes_departure = $items[0];
			$routes_arrival = $items[1];
		} else {
			$routes_departure = $routes;
			$routes_arrival = array();
		}
	}
	$routes = array(
		'departure' => array_slice($routes_departure, 0, 20),
		'arrival' => array_slice($routes_arrival, 0, 20),
	);

	return theme('daway_trip_intercity_page', array('countries' => $countries, 'city' => $city, 'routes' => $routes));
}
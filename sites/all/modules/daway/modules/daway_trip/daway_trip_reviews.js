(function ($) {
    Drupal.behaviors.daway_trip_reviews = {
        attach: function($context, $settings) {
          $('a.answer').click(function(){
            $(this).next().show();
            return false;
          })
          $('#review-list input[type="checkbox"]').change(function(){
            if ($(this).attr('checked')) {
              $(this).addClass('active');
            } else {
              $(this).removeClass('active');
            }
          })

          $('.form-radio').each(function(){
            $(this).parent().addClass('simpleRadiobox');
          })
        }
    }

})(jQuery);
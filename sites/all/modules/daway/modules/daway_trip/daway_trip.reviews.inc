<?php

/**
 * Get reviews by user
 * @param $uid
 * @param bool $nid
 * @param bool $count
 * @return array
 */
function daway_trip_get_review($uid, $nid = FALSE, $count = FALSE) {

  $account = user_load($uid);
  $data = array();
  $n = 0;

  $query = db_select('node', 'n');

  $query->fields('n', array('nid'));

  $query->join('field_data_field_treview_user', 'user', 'user.entity_id = n.nid');
  $query->addField('user', 'field_treview_user_target_id', 'uid');

  $query->join('field_data_field_treview_trip', 'trip', 'trip.entity_id = n.nid');
  $query->addField('trip', 'field_treview_trip_target_id', 'tid');

  $query->condition('n.type', 'trip_review');
  if ($nid) {
    $query->condition('trip.field_treview_trip_target_id', $nid);
  }
  $query->condition('user.field_treview_user_target_id', $uid);


  $results = $query->execute();

  $data = array();

  foreach ($results as $item) {
    if ($count) {
      if ($n == $count) break;
    }

    $node = node_load($item->nid);
    $entity_wrapper = entity_metadata_wrapper('node', $node);

    $trip = $entity_wrapper->field_treview_trip->value();

    if (!$trip) {
      continue;
    }
    $data[$node->nid] = array(
      'user' => $node->uid,
      'rate' => $entity_wrapper->field_treview_rate->value(),
      'body' => $entity_wrapper->body->value(),
      'trip' => $trip,
      'answer' => FALSE,
    );

    $answer = $entity_wrapper->field_treview_answer->value();
    if (!empty($answer)) {
      $data[$node->nid]['answer'] = $answer;
    }

    $n++;
  }


  return $data;
}

/**
 * Reviews list form
 * @param $form
 * @param $form_state
 * @param $uid
 * @param $nid
 * @return mixed
 */
function daway_trip_reviews_list_form($form, &$form_state, $uid, $nid) {

  global $user;

  $reviews = daway_trip_get_review($uid, $nid);

  $form_state['uid'] = $uid;
  $form_state['nid'] = $nid;

  $form['#attributes']['id'] = 'daway-trip-reviews-list-form';
  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'daway_trip').'/daway_trip_reviews.js',
    'type' => 'file'
  );

  if (count($reviews)) {
    $form['reviews'] = array(
      '#prefix' => '<h4 class="title">'.t('Rate You').':</h4><div id="review-list">',
      '#suffix' => '</div>'
    );

    foreach ($reviews as $nid => $item) {
      $form['reviews']['review_'.$nid] = array(
        '#prefix' => '<div class="review-wrapper">',
        '#suffix' => '</div>'
      );
      $form['reviews']['review_'.$nid]['body'] = array(
        '#prefix' => '<div class="review-body">',
        '#suffix' => '</div>',
        '#markup' => theme('daway_trip_review_item', array('data' => $item))
      );
      $form['reviews']['review_'.$nid]['answer'] = array(
        '#prefix' => '<div class="review-answer">',
        '#suffix' => '</div>',
      );

      // if user is owner trip and review not set then show answer form
      if (($user->uid == $uid) && !$item['answer']) {
        $form['reviews']['review_'.$nid]['answer']['link_'.$nid] = array(
          '#markup' => '<a class="answer" href="#show">'.t('Answer').'</a>',
        );


        $form['reviews']['review_'.$nid]['answer']['item_'.$nid] = array(
          '#prefix' => '<div class="review-answer-wrapper row">',
          '#suffix' => '</div>',
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['avatar_'.$nid] = array(
          '#markup' => daway_profile_get_avatar(user_load($item['user']), TRUE),
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['rate_'.$nid] = array(
          '#type' => 'radios',
          '#options' => drupal_map_assoc(array(1, -1)),
          '#default_value' => 1,
          '#attributes' => array('class' => array('rate'))
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['body_'.$nid] = array(
          '#type' => 'textarea',
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['user_'.$nid] = array(
          '#type' => 'value',
          '#value' => $item['user'],
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['actions'] = array(
          '#prefix' => '<div class="form-actions">',
          '#suffix' => '</div>',
        );
        $form['reviews']['review_'.$nid]['answer']['item_'.$nid]['actions']['save'] = array(
          '#type' => 'button',
          '#href' => '',
          '#name' => 'save_'.$nid,
          '#value' => t('Save'),
          '#ajax' => array(
            'callback' => 'daway_trip_reviews_list_form_create_answer',
            'wrapper' => 'daway-trip-reviews-list-form',
          ),
        );
      }
      // else show render review item
      else if (!empty($item['answer'])) {
        $entity_wrapper = entity_metadata_wrapper('node', $item['answer']);
        $data = array(
          'user' => $item['answer']->uid,
          'rate' => $entity_wrapper->field_treview_rate->value(),
          'body' => $entity_wrapper->body->value(),
          'trip' => $entity_wrapper->field_treview_trip->value(),
          'answer' => FALSE,
        );

        $form['reviews']['review_'.$nid]['answer']['body'] = array(
          '#markup' => theme('daway_trip_review_item', array('data' => $data))
        );
      }

    }
  }

  $contact_user_list = daway_contact_get_by_nid($form_state['nid']);
  $is_contacted = FALSE;
  foreach ($contact_user_list as $cul) {
    if (($cul['fuid'] == $user->uid) && ($cul['status'])){
      $is_contacted = TRUE;
      foreach ($reviews as $rcul) {
        if ($rcul['user'] == $user->uid) {
          $is_contacted = FALSE;
          break;
        }
      }
      break;
    }
  }

  if ($is_contacted) {
    $form['create'] = array(
      '#prefix' => '<h4 class="title">'.t('My rate and review').':</h4><div class="review-list-create">',
      '#suffix' => '</div>',
    );
    $form['create']['avatar'] = array(
      '#markup' => daway_profile_get_avatar(user_load($user->uid), TRUE),
    );
    $form['create']['rate'] = array(
      '#type' => 'radios',
      '#options' => drupal_map_assoc(array(1, -1)),
      '#default_value' => 1,
      '#attributes' => array('class' => array('rate'))
    );
    $form['create']['body'] = array(
      '#type' => 'textarea',
    );
    $form['create']['actions'] = array(
      '#prefix' => '<div class="form-actions">',
      '#suffix' => '</div>',
    );
    $form['create']['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#ajax' => array(
        'callback' => 'daway_trip_reviews_list_form_create_new',
        'wrapper' => 'daway-trip-reviews-list-form',
      ),
    );
  }

  return $form;
}

/**
 * Reviews callback form global submit
 * @param $form
 * @param $form_state
 * @see daway_trip_reviews_list_form
 */
function daway_trip_reviews_list_form_submit($form, &$form_state) {
  $form_state['input'] = array();
  $form_state['rebuild'] = TRUE;
}

/**
 * Reviews form callback for created new review
 * @param $form
 * @param $form_state
 * @return array
 * @see daway_trip_reviews_list_form
 */
function daway_trip_reviews_list_form_create_new($form, &$form_state) {

  if (empty($form_state['values']['body'])) {
    $errors = array();
    $errors[] = ajax_command_css('textarea[name="body"]', array('border-color' => 'red'));
    return array(
      '#type' => 'ajax',
      '#commands' => $errors
    );
  }

  global $user;
  $nid = $form_state['nid'];
  $uid = $form_state['uid'];
  $review_entity = entity_create('node', array('type' => 'trip_review'));
  $review_entity->uid = $user->uid;
  $review_wrapper = entity_metadata_wrapper('node', $review_entity);
  $review_wrapper->title->set('review #'.rand(1, 1000));
  $review_wrapper->body->set(array('value' => $form_state['values']['body']));
  $review_wrapper->field_treview_trip->set($nid);
  $review_wrapper->field_treview_rate->set($form_state['values']['rate']);
  $review_wrapper->field_treview_user->set($uid);

  $review_wrapper->save();

  $form = drupal_get_form('daway_trip_reviews_list_form', $uid, $nid);

  $commands = array();
  $commands[] = ajax_command_replace('#daway-trip-reviews-list-form', drupal_render($form));
  return array(
    '#type' => 'ajax',
    '#commands' => $commands
  );
}

/**
 * Reviews form callback for create answer review
 * @param $form
 * @param $form_state
 * @return array
 * @see daway_trip_reviews_list_form
 */
function daway_trip_reviews_list_form_create_answer($form, &$form_state) {

  if (strpos($form_state['clicked_button']['#name'], 'ave_')) {

    global $user;
    $nid = $form_state['nid'];
    $uid = $form_state['uid'];
    $tmp = explode('_', $form_state['clicked_button']['#name']);
    $answer_nid = $tmp[1];
    $user_uid = $form_state['values']['user_'.$answer_nid];

    if (empty($form_state['values']['body_'.$answer_nid])) {
      $errors = array();
      $errors[] = ajax_command_css('textarea[name="body_'.$answer_nid.'"]', array('border-color' => 'red'));
      return array(
        '#type' => 'ajax',
        '#commands' => $errors
      );
    }

    // create node review as answer
    $review_entity = entity_create('node', array('type' => 'trip_review'));
    $review_entity->uid = $user->uid;
    $review_wrapper = entity_metadata_wrapper('node', $review_entity);
    $review_wrapper->title->set('review #'.rand(1, 1000));
    $review_wrapper->body->set(array('value' => $form_state['values']['body_'.$answer_nid]));
    $review_wrapper->field_treview_trip->set($nid);
    $review_wrapper->field_treview_rate->set($form_state['values']['rate_'.$answer_nid]);
    $review_wrapper->field_treview_user->set($user_uid);
    $review_wrapper->save();

    $review_nid = $review_wrapper->nid->value();

    // load and insert answer value to select review
    $node = node_load($answer_nid);
    $entity_wrapper = entity_metadata_wrapper('node', $node);
    $entity_wrapper->field_treview_answer->set($review_nid);
    $entity_wrapper->save();

    // load new form
    $form = drupal_get_form('daway_trip_reviews_list_form', $uid, $nid);

    $commands = array();
    $commands[] = ajax_command_replace('#daway-trip-reviews-list-form', drupal_render($form));
    return array(
      '#type' => 'ajax',
      '#commands' => $commands
    );
  }
}
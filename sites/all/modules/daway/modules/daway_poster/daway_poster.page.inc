<?php


function daway_poster_user_create_form($form, &$form_state, $arg){


	if ($path = libraries_get_path('gmap')) {
		drupal_add_js($path.'/gmap3.min.js');
	}


	drupal_add_library('system', 'ui.datepicker');

	ctools_add_js('ajax-responder');

	$node = FALSE;

	if ($arg == 'create') {
		$op = 'create';
	} elseif (is_numeric($arg)) {
		$op = 'edit';
	}

	if ($op == 'edit') {
		global $user;
		$node = node_load($arg);
		if (!$node) drupal_not_found();
		drupal_set_title(t('Editing').': '.$node->title);
		$form['#node'] = $node;
	}


	$form = array();
	$form['pane_1'] = array(
		'#type' => 'fieldset',
	);
	$form['pane_1']['title'] = array(
		'#markup' => '<span class="fieldset-legend">'.t('Default').'</span>',
	);

	$form['pane_1']['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#attributes' => array('placeholder' => t('Name')),
		'#default_value' => ($node) ? $node->title : '',
	);

	$form['pane_1']['address'] = array(
		'#type' => 'textfield',
		'#title' => t('Address'),
		'#default_value' => ($node) ? geopoint_load($node->field_action_geopoint['und'][0]['target_id'])->name : '',
		'#description' => t('Example: Canary Wharf, London'),
		'#attributes' => array('placeholder' => t('Poster address (address, city, station...)'), 'class' => array('geopoint'))
	);

	$type_term = taxonomy_get_tree(6);
	$type_poster[0] = '';
	foreach ($type_term as $t) {
		$type_poster[$t->tid] = t($t->name);
	}

	$form['pane_1']['row'] = array(
		'#prefix' => '<div class="row">',
		'#suffix' => '</div>',
	);
	$form['pane_1']['row']['type'] = array(
		'#type' => 'select',
		'#options' => $type_poster,
		'#prefix' => '<div class="col left">',
		'#suffix' => '</div>',
		'#attributes' => array('data-placeholder' => t('Select type...'),),
		'#default_value' => ($node) ? $node->field_action_type['und'][0]['target_id'] : '',
	);
	$form['pane_1']['row']['date'] = array(
		'#type' => 'textfield',
		'#default_value' => '',
		'#prefix' => '<div class="col right">',
		'#suffix' => '</div>',
		'#attributes' => array('placeholder' => t('Date'), 'class' => array('tooltip-date', 'time')),
		'#default_value' => ($node) ? format_date($node->field_action_date['und'][0]['value'], 'custom', 'm/d/Y H:i') : '',
	);

	$form['pane_2'] = array(
		'#type' => 'fieldset',
	);
	$form['pane_2']['title'] = array(
		'#markup' => '<span class="fieldset-legend">'.t('Advance').'</span>',
	);

	$form['pane_2']['desc'] = array(
		'#title' => t('Desc'),
		'#type' => 'textarea',
		'#attributes' => array('placeholder' => t('Enter description about your poster')),
		'#default_value' => ($node) ? $node->body['und'][0]['value'] : '',
	);

	$form['pane_2']['image'] = array(
		'#title' => t('Image'),
		'#type' => 'managed_file',
		'#upload_validators' => array(
			'file_validate_extensions' => array('gif png jpg jpeg'),
//			'file_validate_size' => array(1 * 500 * 500),
		),
		'#default_value' => ($node) ? $node->field_action_preview['und'][0]['fid'] : '',
		'#upload_location' => 'public://poster/',
	);

	$form['actions'] = array(
		'#prefix' => '<div class="form-actions">',
		'#suffix' => '</div>',
	);

	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#ajax' => array(
			'callback' => 'daway_poster_user_create_form_callback',
		),
	);

	return $form;
}

function daway_poster_user_create_form_callback($form, &$form_state) {
	$errors = array();
	if (empty($form_state['values']['name'])) {
		$errors[] = ajax_command_css('input[name="name"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['type'])) {
		$errors[] = ajax_command_css('select[name="type"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['date'])) {
		$errors[] = ajax_command_css('input[name="date"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['date'])) {
		$errors[] = ajax_command_css('input[name="address"]', array('border-color' => 'red'));
	}

	if (count($errors) > 0) {
		return array(
			'#type' => 'ajax',
			'#commands' => $errors
		);
	} else {

		// prepend field
		$point = new GeoPoint();
		$point->name = $form_state['values']['address'];
		$point = geopoint_save($point);

		$date = strtotime($form_state['values']['date']);



		// create & save node

		global $user;
		if (isset($form['#node'])) {
			$node = $form['#node'];
		} else {
			$node = new stdClass();
			$node->type = "action";
			$node->language = LANGUAGE_NONE;
			$node->status = NODE_PUBLISHED;
			node_object_prepare($node);
			$node->uid = $user->uid;
		}


//		$file = file_load($form_state['values']['image']);
//
//		if (!isset($file->uri)) {
//			$file = file_save_upload('image', array('file_validate_is_image' => array()), 'public://poster');
//			if ($file) {
//				$new_filename = date('YdmHis') . '.' . pathinfo($file->filename, PATHINFO_EXTENSION);
//  			$file = file_move($file, 'public://images/' . $new_filename);
//			}
//		}

//		$file = file_load($form_state['values']['image']);
//		$file->status = FILE_STATUS_PERMANENT;
//		file_usage_add($file, 'file', 'node', $node->nid);
//		file_save($file);

//		$node->field_action_preview['und'][0] = (array)$file;

		$node->field_action_date['und'][0]['value'] = $date;
		$node->field_action_geopoint['und'][0]['target_id'] = $point->pid;
		$node->field_action_type['und'][0]['target_id'] = $form_state['values']['type'];
		// field_action_preview

		$node->title = $form_state['values']['name'];
		$node->body['und'][0]['value'] = strip_tags($form_state['values']['desc']);

		$node = node_submit($node);
		node_save($node);

		return array(
			'#type' => 'ajax',
			'#commands' =>  array(array(
				'command' => 'redirect',
				'url' => url('node/'.$node->nid, array()),
				'delay' => 0,
			)),
		);

	}
}
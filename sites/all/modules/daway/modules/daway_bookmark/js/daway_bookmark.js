/**
 * @file
 *
 * Adyax test JS file
 */

(function ($) {

  /**
   * Ajax delivery command to change color in selector.
   */
  Drupal.ajax.prototype.commands.bookmarkDelete = function (ajax, response, status) {
    if (status == 'success') {
      var wrapper = response.wrapper;
      $(wrapper).animate({
        opacity: 0.25,
        left: "-=567",
      }, 1000, function() {
        $(this).parents('tr').remove();
      });
    }
  };


})(jQuery);
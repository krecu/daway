//(function($) {
//  Drupal.ProfileCarEdit = {
//    form_id: 'daway-profile-addcar-popup'
//  };
//  Drupal.behaviors.ProfileCarEdit = {
//    attach: function(context, settings) {
//      // We extend Drupal.ajax objects for all AJAX elements in our form
//      for (ajax_el in settings.ajax) {
//        if (typeof Drupal.ajax[ajax_el] != 'undefined' && Drupal.ajax[ajax_el].element.form) {
//          if (Drupal.ajax[ajax_el].element.form.id === Drupal.ProfileCarEdit.form_id) {
//            Drupal.ajax[ajax_el].beforeSend = Drupal.ProfileCarEdit.beforeSend;
//            Drupal.ajax[ajax_el].success = Drupal.ProfileCarEdit.success;
//            Drupal.ajax[ajax_el].error = Drupal.ProfileCarEdit.error;
//          }
//        }
//      }
//    }
//  };
//  // Disable form
//  Drupal.ProfileCarEdit.beforeSend = function (first, second) {
//    Drupal.ajax.prototype.beforeSend.call(this, first, second);
//  }
//  Drupal.ProfileCarEdit.success = function (response, status) {
//    Drupal.ajax.prototype.success.call(this, response, status);
//    jQuery('.form-item-car-vendor select').chosen({disable_search: true});
//    jQuery('.form-item-car-year select').chosen({disable_search: true});
//    jQuery('.form-item-car-model select').chosen({disable_search: true});
//    jQuery('.form-item-car-mode select').chosen({disable_search: true});
//  }
//  Drupal.ProfileCarEdit.error = function (response, uri) {
//    Drupal.ProfileCarEdit.enableForm(this.element.form);
//    Drupal.ajax.prototype.error.call(this, response, uri);
//  }
//})(jQuery);

(function ($) {

  /**
   * Init JS module
   * @type {{attach: attach}}
   */
  Drupal.behaviors.ProfileCarInint= {
    attach : function () {
      $('select#car-type').change(function(){
        var settings = {url : "/js/carfield/get_vendor/" + $(this).val()};
        var ajax = new Drupal.ajax(false, false, settings);
        ajax.eventResponse(ajax, {});
      })
      $('select#car-vendor').change(function(){
        var settings = {url : "/js/carfield/get_year/" + $('select#car-type').val() + "/" + $(this).val() };
        var ajax = new Drupal.ajax(false, false, settings);
        ajax.eventResponse(ajax, {});
        $('input[name="vendor"]').val($(this).val());
      })
      $('select#car-year').change(function(){
        var settings = {url : "/js/carfield/get_model/" + $('select#car-type').val() + "/" + $('select#car-vendor').val() + "/" + $(this).val() };
        var ajax = new Drupal.ajax(false, false, settings);
        ajax.eventResponse(ajax, {});
        $('input[name="year"]').val($(this).val());
      })
      $('select#car-model').change(function(){
        var settings = {url : "/js/carfield/get_mode/" + $(this).val() };
        var ajax = new Drupal.ajax(false, false, settings);
        ajax.eventResponse(ajax, {});
        $('input[name="model"]').val($(this).val());
      })
      $('select#car-mode').change(function(){
        $('input[name="mode"]').val($(this).val());
      })
    }
  }

  /**
   * Ajax delivery command to change color in selector.
   */
  Drupal.ajax.prototype.commands.ProfileCarEdit = function (ajax, response, status) {
    if (status == 'success') {
      var data = response;

      items = data.items;

      container = $("select#car-"+data.type);
      container.empty();
      container.append("<option value='0'></option>");
      count = 0;

      if (data.type == "vendor") {
        for (var index in items) {
          count++;
          container.append($("<option value='" + index + "'>" + items[index].name + "</option>"))
        }
      }

      if (data.type == "year") {
        for (var index in items) {
          count++;
          container.append($("<option value='" + index + "'>" + items[index] + "</option>"))
        }
      }

      if (data.type == "model") {
        for (var index in items) {
          count++;
          container.append($("<option value='" + index + "'>" + items[index].name + "</option>"))
        }
      }

      if (data.type == "mode") {
        for (var index in items) {
          count++;
          container.append($("<option value='" + index + "'>" + items[index].name + "</option>"))
        }
      }

      if (count > 0) {
        container.attr("disabled", false);
        $('input[name="'+data.type+'"]').val('');
      } else {
        container.attr("disabled", true);
        $('input[name="'+data.type+'"]').val('');
      }
      container.trigger("chosen:updated");

    }
  };

})(jQuery);
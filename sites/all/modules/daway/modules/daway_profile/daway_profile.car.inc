<?php

/**
 * theme js ctools link
 */
function daway_profile_editcar_js_popuplink($account, $type, $nid) {
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $custom_style = array(
    'daway-profile' => array(
      'modalSize' => array(
        'width' => 'auto',
        'height' => 'auto',
        'type' => 'fixed',
        'contentRight' => 0,
        'contentBottom' => 0,
      ),
      'closeText' => t('Close'),
      'throbber' => theme('image', array(
        'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
        'alt' => t('Loading...'),
        'title' => t('Loading')
      )),
      'animation' => 'fadeIn',
      'animationSpeed' => 'fast',
    ),
  );
  drupal_add_js($custom_style, 'setting');
  $title = 'Edit';
  switch ($type) {
    case 'editcar' :
      $title = t('Edit car');
      break;
  }

  $output = ctools_modal_text_button(t($title), 'js/nojs/profile/' . $account->uid . '/' . $type . '/' . $nid, t($title), 'ctools-modal-daway-profile edit ');
  ctools_include('plugins');

  return $output;
}


/**
 * Form for create new car
 * @param $form
 * @param $form_state
 * @return mixed
 */
function daway_profile_addcar_popup($form, &$form_state) {

  $form['#attributes']['id'] = 'daway-profile-addcar-popup';
  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'daway_profile') . '/daway_profile_car.js' => array('type' => 'file',),
    ),
  );

  $types = carfield_get_types();
  $options = array();
  $options[] = '';
  foreach ($types as $id => $type) {
    $options[$id] = t($type->name, array(), array('context' => 'Car'));
  }

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#attributes' => array('data-placeholder' => t('Select type'), 'id' => 'car-type'),
  );

  $form['vendor'] = array(
    '#title' => t('Vendor'),
    '#type' => 'hidden',
  );
  $form['vendor_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-vendor" data-placeholder="' . t('Select vendor') . '"></select>
    </div>',
  );

  $form['year'] = array(
    '#title' => t('Year'),
    '#type' => 'hidden',
  );
  $form['year_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-year" data-placeholder="' . t('Select year') . '"></select>
    </div>',
  );

  $form['model'] = array(
    '#title' => t('Model'),
    '#type' => 'hidden',
  );
  $form['model_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-model" data-placeholder="' . t('Select model') . '"></select>
    </div>',
  );

  $form['mode'] = array(
    '#title' => t('Modification'),
    '#type' => 'hidden',
  );
  $form['mode_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-mode" data-placeholder="' . t('Select modification') . '"></select>
    </div>',
  );

  $colors_tree = taxonomy_get_tree(12);
  $options = array();
  $options[] = t('Select color');
  foreach ($colors_tree as $item) {
    $options[$item->tid] = $item->name;
  }
  $form['color'] = array(
    '#title' => t('Color'),
    '#type' => 'select',
    '#options' => $options,
  );

  $form['comfort'] = array(
    '#prefix' => '<div class="comfort row">',
    '#suffix' => '</div>'
  );
  $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
  foreach ($fields_info as $field_name => $value) {
    $field_data = field_info_field($field_name);
    $form['comfort'][$field_name] = array(
      '#type' => 'checkbox',
      '#title' => $value['label'],
      '#options' => array(
        0 => $field_data['settings']['allowed_values'][0],
        1 => $field_data['settings']['allowed_values'][1]
      ),
      '#default_value' => 0,
      '#attributes' => array('class' => array('dawayCheckbox'))
    );
  }


  $form['images'] = array(
    '#prefix' => '<div class="images row">',
    '#suffix' => '</div>'
  );

  for ($i = 1; $i <= 4; $i++) {
    $form['images']['img' . $i] = array(
      '#title' => 'Image',
      '#type' => 'managed_file',
      '#upload_location' => 'public://cars/',
      '#theme' => 'daway_profile_image_crop',
      '#pre_render' => array('daway_profile_image_crop_file_pre_render'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );
  }


  $form['actions'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ajax' => array(
      'callback' => "daway_profile_addcar_popup_create",
    ),
  );

  return $form;
}

/**
 * Form submit callback
 * @param $form
 * @param $form_state
 * @see daway_profile_addcar_popup()
 */
function daway_profile_addcar_popup_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Form js callback
 * @param $form
 * @param $form_state
 * @return array
 * @see daway_profile_addcar_popup()
 */
function daway_profile_addcar_popup_create($form, &$form_state) {

  $errors = array();
  if (empty($form_state['values']['type'])) {
    form_set_error('mode', t('You need select type car'));
    $errors[] = ajax_command_css('#car_type_chosen a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['vendor'])) {
    form_set_error('mode', t('You need select vendor car'));
    $errors[] = ajax_command_css('#car_vendor_chosen a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['year'])) {
    form_set_error('mode', t('You need select year car'));
    $errors[] = ajax_command_css('#car_year_chosen a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['model'])) {
    form_set_error('mode', t('You need select model car'));
    $errors[] = ajax_command_css('#car_model_chosen a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['mode'])) {
    form_set_error('mode', t('You need select modification car'));
    $errors[] = ajax_command_css('#car_mode_chosen a', array('border-color' => 'red'));
  }

  if (count($errors) > 0) {
    $errors[] = ajax_command_remove('div#better-messages-wrapper');
    $errors[] = ajax_command_append('#messages', theme('status_messages'));
    return array(
      '#type' => 'ajax',
      '#commands' => $errors
    );
  }
  else {

    global $user;

    if (!empty($form_state['#node'])) {
      $car_entity = $form_state['#node'];
    }
    else {
      $car_entity = entity_create('node', array('type' => 'car'));
      $car_entity->uid = $user->uid;
    }

    $car_wrapper = entity_metadata_wrapper('node', $car_entity);

    // car data
    $car_data = array(
      'manufactures' => $form_state['values']['vendor'],
      'year' => $form_state['values']['year'],
      'model' => $form_state['values']['model'],
      'modification' => $form_state['values']['mode'],
      'type' => $form_state['values']['type']
    );

    // collect title
    $manufactures = array_values(carfield_get_manufactures($car_data['manufactures']));
    $model = array_values(carfield_get_models($car_data['model']));
    $mode = array_values(carfield_get_modifications($car_data['modification']));

    $title = $manufactures[0]->name . " " . $model[0]->name . "/" . $mode[0]->short;

    $car_wrapper->title->set($title);
    $car_wrapper->body->set(array('value' => " "));
    $car_wrapper->field_car_car->set($car_data);
    $car_wrapper->field_car_color->set($form_state['values']['color']);

    $images = array();
    for ($i = 1; $i <= 4; $i++) {

      $fid = $form_state['values']['img' . $i]['fid'];
      if ($fid) {
        $img = file_load($fid);
        $images[] = (array) $img;
      }
    }
    if (count($images)) {
      $car_wrapper->field_car_gallery->set($images);
    }
    $car_wrapper->save();

    $field_car_comfort = entity_create('field_collection_item', array('field_name' => 'field_car_comfort'));
    $field_car_comfort->setHostEntity('node', $car_entity);
    $collection = entity_metadata_wrapper('field_collection_item', $field_car_comfort);
    $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
    foreach ($fields_info as $field_name => $value) {
      $val = (!empty($form_state['values'][$field_name])) ? 1 : 0;
      $collection->{$field_name}->set($val);
    }

    $collection->save();

    ctools_include('ajax');
    ctools_include('modal');

    $command = array();
    $command[] = ctools_modal_command_display(t('Success!'), t('Car has been saved and page will be reloaded in few seconds'));
    $command[] = ctools_ajax_command_reload();

    return array(
      '#type' => 'ajax',
      '#commands' => $command
    );
  }
}

function daway_profile_editcar_popup($form, &$form_state) {

  $nid = 668; //(empty($form_state['storage'])) ? arg(5) : $form_state['storage']['node'];
//  dsm($form_state['storage']);
  $node = node_load($nid);

  $form['node'] = array(
    '#type' => 'hidden',
    '#default_value' => $nid
  );

  $form['#attributes']['id'] = 'daway-profile-addcar-popup';

  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'daway_profile') . '/daway_profile_car.js' => array('type' => 'file',),
    ),
  );

  $car_wrapper = entity_metadata_wrapper('node', $node);
  $car_data = $car_wrapper->field_car_car->value();
  $car_color = $car_wrapper->field_car_color->value();
  $car_comfort = $car_wrapper->field_car_comfort->value();
  $car_images = $car_wrapper->field_car_gallery->value();

  $manufactures = array_values(carfield_get_manufactures($car_data['manufactures']));
  $model = array_values(carfield_get_models($car_data['model']));
  $mode = array_values(carfield_get_modifications($car_data['modification']));
  $type = array_values(carfield_get_types($car_data['type']));

  $car_data = array(
    'manufactures' => ((!empty($manufactures[0])) ? $manufactures[0]->id : ''),
    'model' => ((!empty($model[0])) ? $model[0]->id : ''),
    'type' => ((!empty($type[0])) ? $type[0]->id : ''),
    'year' => $car_data['year'],
    'modification' => ((!empty($mode[0])) ? $mode[0]->id : ''),
  );


  $types = carfield_get_types();
  $options = array();
  $options[] = '';
  foreach ($types as $id => $type) {
    $options[$id] = t($type->name, array(), array('context' => 'Car'));
  }

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#attributes' => array('data-placeholder' => t('Select type'), 'id' => 'car-type'),
    '#default_value' => $car_data['type'],
  );

  $form['vendor'] = array(
    '#title' => t('Vendor'),
    '#type' => 'hidden',
    '#default_value' => $car_data['manufactures'],
  );
  $form['vendor_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-vendor" data-placeholder="' . $manufactures[0]->name . '"></select>
    </div>',
  );

  $form['year'] = array(
    '#title' => t('Year'),
    '#type' => 'hidden',
    '#default_value' => $car_data['year']
  );
  $form['year_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-year" data-placeholder="' . $car_data['year'] . '"></select>
    </div>',
  );

  $form['model'] = array(
    '#title' => t('Model'),
    '#type' => 'hidden',
    '#default_value' => $car_data['model']
  );
  $form['model_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-model" data-placeholder="' . $model[0]->name . '"></select>
    </div>',
  );

  $form['mode'] = array(
    '#title' => t('Modification'),
    '#type' => 'hidden',
    '#default_value' => $car_data['modification']
  );
  $form['mode_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-mode" data-placeholder="' . $mode[0]->name . '"></select>
    </div>',
  );

  $colors_tree = taxonomy_get_tree(12);
  $options = array();
  $options[] = t('Select color');
  foreach ($colors_tree as $item) {
    $options[$item->tid] = $item->name;
  }
  $form['color'] = array(
    '#title' => t('Color'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $car_color->tid
  );

  $form['comfort'] = array(
    '#prefix' => '<div class="comfort row">',
    '#suffix' => '</div>'
  );
  $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
  foreach ($fields_info as $field_name => $value) {
    $field_data = field_info_field($field_name);
    $default_value = (isset($car_comfort->{$field_name}['und'])) ? $car_comfort->{$field_name}['und'][0]['value'] : 0;
    $form['comfort'][$field_name] = array(
      '#type' => 'checkbox',
      '#title' => $value['label'],
      '#options' => array(
        0 => $field_data['settings']['allowed_values'][0],
        1 => $field_data['settings']['allowed_values'][1]
      ),
      '#default_value' => $default_value,
      '#attributes' => array('class' => array('dawayCheckbox'))
    );
  }


  $form['images'] = array(
    '#prefix' => '<div class="images row">',
    '#suffix' => '</div>'
  );

  for ($i = 1; $i <= 4; $i++) {

    $default_value = (isset($car_images[$i - 1])) ? $car_images[$i - 1]['fid'] : '';

    $form['images']['img' . $i] = array(
      '#title' => 'Image',
      '#type' => 'managed_file',
      '#upload_location' => 'public://cars/',
      '#theme' => 'daway_profile_image_crop',
      '#pre_render' => array('daway_profile_image_crop_file_pre_render'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
      '#default_value' => $default_value,
    );
  }


  $form['actions'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ajax' => array(
      'callback' => "daway_profile_addcar_popup_create",
    ),
  );
  return $form;
}

function daway_profile_editcar_popup_submit($form, &$form_state) {
  $form_state['storage']['node'] = 668; //$form_state['values']['node'];
  $form_state['rebuild'] = TRUE;
}


function daway_profile_addcartrip_popup($form, &$form_state) {

  $form['#attributes']['id'] = 'daway-profile-addcar-popup';
  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'daway_profile') . '/daway_profile_car.js' => array('type' => 'file',),
    ),
  );

  $types = carfield_get_types();
  $options = array();
  $options[] = '';
  foreach ($types as $id => $type) {
    $options[$id] = t($type->name, array(), array('context' => 'Car'));
  }

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#attributes' => array('data-placeholder' => t('Select type'), 'id' => 'car-type'),
  );

  $form['vendor'] = array(
    '#title' => t('Vendor'),
    '#type' => 'hidden',
  );
  $form['vendor_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-vendor" data-placeholder="' . t('Select vendor') . '"></select>
    </div>',
  );

  $form['year'] = array(
    '#title' => t('Year'),
    '#type' => 'hidden',
  );
  $form['year_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-year" data-placeholder="' . t('Select year') . '"></select>
    </div>',
  );

  $form['model'] = array(
    '#title' => t('Model'),
    '#type' => 'hidden',
  );
  $form['model_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-model" data-placeholder="' . t('Select model') . '"></select>
    </div>',
  );

  $form['mode'] = array(
    '#title' => t('Modification'),
    '#type' => 'hidden',
  );
  $form['mode_html'] = array(
    '#markup' => '
    <div class="form-item form-type-select">
      <select disabled="true" id="car-mode" data-placeholder="' . t('Select modification') . '"></select>
    </div>',
  );

  $colors_tree = taxonomy_get_tree(12);
  $options = array();
  $options[] = t('Select color');
  foreach ($colors_tree as $item) {
    $options[$item->tid] = t($item->name);
  }
  $form['color'] = array(
    '#title' => t('Color'),
    '#type' => 'select',
    '#options' => $options,
  );

  $form['comfort'] = array(
    '#prefix' => '<div class="comfort row">',
    '#suffix' => '</div>'
  );
  $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
  foreach ($fields_info as $field_name => $value) {
    $field_data = field_info_field($field_name);
    $form['comfort'][$field_name] = array(
      '#type' => 'checkbox',
      '#title' => $value['label'],
      '#options' => array(
        0 => $field_data['settings']['allowed_values'][0],
        1 => $field_data['settings']['allowed_values'][1]
      ),
      '#default_value' => 0,
      '#attributes' => array('class' => array('dawayCheckbox'))
    );
  }


  $form['images'] = array(
    '#prefix' => '<div class="images row">',
    '#suffix' => '</div>'
  );

  for ($i = 1; $i <= 4; $i++) {
    $form['images']['img' . $i] = array(
      '#title' => 'Image',
      '#type' => 'managed_file',
      '#upload_location' => 'public://cars/',
      '#theme' => 'daway_profile_image_crop',
      '#pre_render' => array('daway_profile_image_crop_file_pre_render'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );
  }


  $form['actions'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#ajax' => array(
      'callback' => "daway_profile_addcartrip_popup_create",
    ),
  );

  return $form;
}

function daway_profile_addcartrip_popup_create($form, &$form_state) {

  $errors = array();
  if (empty($form_state['values']['type'])) {
    $errors[] = ajax_command_css('#car_type_chzn a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['vendor'])) {
    $errors[] = ajax_command_css('#car_vendor_chzn a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['year'])) {
    $errors[] = ajax_command_css('#car_year_chzn a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['model'])) {
    $errors[] = ajax_command_css('#car_model_chzn a', array('border-color' => 'red'));
  }
  if (empty($form_state['values']['mode'])) {
    $errors[] = ajax_command_css('#car_mode_chzn a', array('border-color' => 'red'));
  }

  if (count($errors) > 0) {
    return array(
      '#type' => 'ajax',
      '#commands' => $errors
    );
  }
  else {

    global $user;

    if (!empty($form_state['#node'])) {
      $car_entity = $form_state['#node'];
    }
    else {
      $car_entity = entity_create('node', array('type' => 'car'));
      $car_entity->uid = $user->uid;
    }

    $car_wrapper = entity_metadata_wrapper('node', $car_entity);

    // car data
    $car_data = array(
      'manufactures' => $form_state['values']['vendor'],
      'year' => $form_state['values']['year'],
      'model' => $form_state['values']['model'],
      'modification' => $form_state['values']['mode'],
      'type' => $form_state['values']['type']
    );

    // collect title
    $manufactures = array_values(carfield_get_manufactures($car_data['manufactures']));
    $model = array_values(carfield_get_models($car_data['model']));
    $mode = array_values(carfield_get_modifications($car_data['modification']));

    $title = $manufactures[0]->name . " " . $model[0]->name . "/" . $mode[0]->short;

    $car_wrapper->title->set($title);
    $car_wrapper->body->set(array('value' => " "));
    $car_wrapper->field_car_car->set($car_data);
    $car_wrapper->field_car_color->set($form_state['values']['color']);


    $images = array();
    for ($i = 1; $i <= 4; $i++) {

      $fid = $form_state['values']['img' . $i];
      if ($fid) {
        $img = file_load($fid);
        $images[] = (array) $img;
      }
    }
    if (count($images)) {
      $car_wrapper->field_car_gallery->set($images);
    }
    $car_wrapper->save();

    $field_car_comfort = entity_create('field_collection_item', array('field_name' => 'field_car_comfort'));
    $field_car_comfort->setHostEntity('node', $car_entity);
    $collection = entity_metadata_wrapper('field_collection_item', $field_car_comfort);
    $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
    foreach ($fields_info as $field_name => $value) {
      $val = (!empty($form_state['values'][$field_name])) ? 1 : 0;
      $collection->{$field_name}->set($val);
    }

    $collection->save();

    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    $commands[] = ajax_command_append('#cars select', '<option value="' . $car_entity->nid . '">' . $title . '</option>');
    $commands[] = ajax_command_invoke(NULL, 'ChosenUpdate', array('#cars select'));
    $commands[] = ctools_modal_command_dismiss();
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
}
<?php

/**
 * Custom render user avatar in profile
 */
function daway_profile_get_avatar($account, $page = FALSE) {
  global $user;
  $output = '';
  $class = array();

  if (!empty($account->picture)) {
    if ($page) {
      $class[] = 'anonymous';
      $thumbnail = theme('image_style', array(
        'style_name' => 'preview_94x94',
        'path' => $account->picture->uri,
        'alt' => $account->name,
      ));
      $output = $thumbnail;
      return '<div class="avatar ' . implode(' ', $class) . '">' . $output . '</div>';
    }
    else {
      if ($user->uid == $account->uid) {
        $class[] = 'edit';
        $thumbnail = theme('image_style', array(
          'style_name' => 'preview_100x100',
          'path' => $account->picture->uri,
          'alt' => $account->name,
        ));
        $output = $thumbnail;
        $output .= daway_profile_js_popup_link($account, 'avatar');
        return '<div class="avatar ' . implode(' ', $class) . '">' . $output . '</div>';
      }
    }
  }
  else {
    $uri = '/' . drupal_get_path('theme', 'develix') . '/images/';
    if ($page) {
      if (isset($account->field_user_gender['und'])) {
        $output = '<img src="' . $uri . 'no-avatar-' . $account->field_user_gender['und'][0]['value'] . '.png">';
      }
      else {
        $output = '<img src="' . $uri . 'noavatar.gif" >';
      }
    }
    else {
      if (arg(0) == 'user') {
        $class[] = 'empty';
        $output = daway_profile_js_popup_link($account, 'avatar');
      }
      else {
        if (isset($account->field_user_gender['und'])) {
          $output = '<img src="' . $uri . 'no-avatar-' . $account->field_user_gender['und'][0]['value'] . '.png">';
        }
        else {
          $output = '<img src="' . $uri . 'no-avatar-male.png" >';
        }
      }
    }
  }

  return '<div class="avatar ' . implode(' ', $class) . '">' . $output . '</div>';
}

/**
 * Custom render user name in profile
 */
function daway_profile_get_name($account) {
  $output = format_username($account);
  global $user;
  return '<div class="name">' . l($output, 'user/' . $account->uid) . '</div>';
}

/**
 * Custom render age in profile
 */
function daway_profile_get_age($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  if (isset($account->field_user_age['und'])) {
    $date = $account->field_user_age['und'][0]['value'];
    $time = time();
    $age = ceil(($time - $date) / 31556926);
//		$value = format_plural($age, '1 year', '@count '.t('years'));
    $value = format_plural($age, t('1 year'), '@count ' . t('years'));
  }

  return '<div class="age item">' . $value . '</div>';
}

/**
 * Custom render gender in profile
 */
function daway_profile_get_gender($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  if (isset($account->field_user_gender['und'])) {
    $value = $account->field_user_gender['und'][0]['value'];
  }

  return '<div class="age item">' . $value . '</div>';
}

/**
 * Custom render get mail in profile
 */
function daway_profile_get_mail($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  if (isset($account->mail)) {
    $value = $account->mail;
  }

  return '<div class="mail item">' . $value . '</div>';
}

/**
 * Custom render gender in profile
 */
function daway_profile_get_phone($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  if (isset($account->field_user_phone['und'])) {
    $value = $account->field_user_phone['und'][0]['value'];
  }

  return '<div class="phone item">' . $value . '</div>';
}

/**
 * Custom render gender in profile
 */
function daway_profile_get_address($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  if (isset($account->field_user_address['und'])) {
    $value = $account->field_user_address['und'][0]['target_id'];
    $geopoint = geopoint_load($value);
  }

  return '<div class="age item">Address: ' . implode(', ', geopoint_get_full_name($geopoint->pid)) . '</div>';
}

/**
 * Custom render about in profile
 */
function daway_profile_get_about($account) {
  $account = user_load($account->uid);
  $value = t('not set');
  global $user;
  $output = '';
  $class = array();

  if (isset($account->field_user_about['und'])) {
    $value = $account->field_user_about['und'][0]['value'];
  }

  if (arg(1) != $user->uid) {
    $class[] = 'anonymous';
    $output = $value;
  }
  else {
    $class[] = 'edit';
    $output = $value;
    $output .= daway_profile_js_popup_link($account, 'about');
  }


  return '<div class="about ' . implode(' ', $class) . '"><h4 class="label">' . t('About') . '</h4>' . $output . '</div>';

}

/**
 * Custom get global rate user
 * @param $uid
 * @return int
 */
function daway_profile_get_rate($uid) {
  $rate = 0;
  $account = user_load($uid);
  if (isset($account->field_user_rate['und'])) {
    $rate = $account->field_user_rate['und'][0]['value'];
  }
  else {
    $rate = 0;
  }
  return $rate;
}

/**
 * Calculate user rate by custom algo
 * @param $uid
 * @return float|int
 */
function daway_profile_set_rate($uid) {
  $rate = 0;
  $account = user_load($uid);

  // calculate rate by reviews
  if (isset($account->field_user_review['und'])) {
    $fields_info = field_info_instances('field_collection_item', 'field_user_review');
    $review_entity_id = $account->field_user_review['und'][0]['value'];
    $field_collection = entity_metadata_wrapper('field_collection_item', $review_entity_id);
    $rate += $field_collection->field_fur_rate->value();
  }

  // calculate by profile complite
  $profile_complite = daway_user_get_percent_profile($account);
  $rate += $profile_complite;


  $account->field_user_rate['und'][0]['value'] = ceil($rate);
  user_save($account);
  return $rate;
}

/**
 * Simple function to get MAX rate from all users
 * @return mixed
 */
function daway_profile_get_maxrate() {
  $query = db_select('field_data_field_user_rate', 'rate');
  $query->addExpression('MAX(field_user_rate_value)', 'max');
  $max = $query->execute()->fetchField();
  return $max;
}
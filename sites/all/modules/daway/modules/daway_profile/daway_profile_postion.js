(function ($) {

    Drupal.behaviors.daway_profile = {
        attach: function (context, settings) {

            $form = $('#daway-profile-geoposition-popup');


            var $map = $('.map', $form);
//            $map.gmap3({
//                getgeoloc:{
//                    callback : function(latLng){
//                        if (latLng){
//                            $(this).gmap3({
//                                marker:{
//                                    latLng:latLng
//                                },
//                                map:{
//                                    options:{
//                                        zoom: 5
//                                    }
//                                },
//                                getaddress:{
//                                    latLng: latLng,
//                                    callback:function(results){
//                                        content = results && results[1] ? results && results[1].formatted_address : '';
//                                        $('input[type="text"]', $form).val(content);
//                                    }
//                                }
//                            });
//                        }
//                    }
//                }
//            });



            $map.bind('resizeMapUpdate', function () {
                createMap($map);
            });

            function createMap($map) {
//                var $map = $('.map', $form);
//                $map = $('<div class="map test"></div>');

                $map.gmap3({
                    clear : {
                        options:{
                            name: 'marker'
                        }
                    }
                });
                $map.gmap3({
                    getlatlng: {
                        address: $('input[type="text"]', $form).val(),
                        callback: function (results) {
                            if (!results) return;
                            $(this).gmap3({
                                marker: {
                                    latLng: results[0].geometry.location
                                },
                                map: {
                                    options: {
                                        zoom: 5,
                                        center: results[0].geometry.location
                                    }
                                }
                            });
                        }
                    }
                });
            }

            $('input[type="text"]', $form).bind("geocode:result", function (event, result) {
                $map.trigger('resizeMapUpdate');
            })
        }
    };

})(jQuery);
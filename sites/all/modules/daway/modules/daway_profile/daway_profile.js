//(function ($, Drupal, window, document, undefined) {

//    var $avatar_input = jQuery('input[type="file"]', '#wrapper-avatar');
//    $avatar_input.change(function(){
//        $avatar_input.next().click();
//        console.log('start upload');
//    })


(function ($) {

    Drupal.behaviors.daway_profile = {
        attach: function(context, settings) {

            if (!$('#wrapper-avatar a.button',context).hasClass('avatar-process')) {
                $('#wrapper-avatar a.button',context).addClass('avatar-process');
                $('#wrapper-avatar a.button',context).click(function(){
                    $('#wrapper-avatar input[type="file"]', context).trigger('click');
                })
            }

        }
    };

})(jQuery);

//    Drupal.behaviors.daway_profile  = function(context) {
//            console.log(1);
//        Drupal.behaviors.ajaxRegister = {
//            attach: function (context){

//                if (jQuery('#avatar-original').length) {
//                    var $avatar_original = jQuery('#avatar-original');
//                    $avatar_original.resizePhoto(500, 500);
//                }

//            }
//        }

//    }

//})(jQuery, Drupal, this, this.document);
<?php

/**
 * Daway profile trust E-Mail form
 * @param $form
 * @param $form_state
 * @return array
 */
function daway_profile_validate_email_popup($form, &$form_state) {

  global $user;
  $account = user_load($user->uid);

  $form = array();

  $form['#attributes']['id'] = 'daway-profile-validate';

  if (isset($account->mail)) {
    $email = $account->mail;
  }

  $form['form'] = array(
    '#prefix' => '<div class="container">',
    '#suffix' => '</div>',
  );

  $form['form']['text'] = array(
    '#markup' => '<div class="phone">' . t('The confirmation code will be sent to: ') . $email . '</div>',
  );

  $form['form']['email'] = array(
    '#type' => 'value',
    '#value' => $email,
  );

  $form['form']['code'] = array(
    '#title' => t('Code'),
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => t('Enter confirmation code')),
  );

  $form['form']['actions'] = array(
    '#prefix' => '<div id="actttttt" class="form-actions">',
    '#suffix' => '</div>',
  );

  $form['form']['actions']['apply'] = array(
    '#value' => t('Apply'),
    '#type' => 'button',
    '#name' => 'apply',
    '#attributes' => array('style' => 'display:none'),
    '#ajax' => array(
      'callback' => 'daway_profile_validate_email_popup_callback',
    ),
  );

  $form['form']['actions']['send'] = array(
    '#value' => t('Send code'),
    '#type' => 'button',
    '#name' => 'send',
    '#ajax' => array(
      'callback' => 'daway_profile_validate_email_popup_callback',
    ),
  );
  return $form;
}

/**
 * Ajax callback trust E-Mail form
 * @param $form
 * @param $form_state
 * @return mixed
 * @see daway_profile_validate_email_popup()
 */
function daway_profile_validate_email_popup_callback($form, &$form_state) {
  $commands = array();
  global $user;
  $account = user_load($user->uid);

  // if click apply button
  if ($form_state['triggering_element']['#name'] == 'apply') {

    if (!empty($form_state['values']['code'])) {

      // if true code then save value
      if ($form_state['values']['code'] == $_SESSION['DAWAY_PROFILE_VALIDATE_EMAIL']) {
        // reset code
        unset($_SESSION['DAWAY_PROFILE_VALIDATE_EMAIL']);

        if (isset($account->field_user_security['und'])) {
          $security_entity_id = $account->field_user_security['und'][0]['value'];
          $field_collection = entity_metadata_wrapper('field_collection_item', $security_entity_id);
          $field_collection->field_fus_email = 1;
          $field_collection->save();
          user_save($account);

          // invoke rule
          rules_invoke_event('daway_profile_mail_validation_apply', $account);

          ctools_include('ajax');

          $commands[] = ctools_modal_command_display(t('Success!'), t('Verification complete and page will be reloaded in few seconds'));
          $commands[] = ctools_ajax_command_reload();

          return array(
            '#type' => 'ajax',
            '#commands' => $commands,
          );
        }
      }
      // else alert
      else {
        form_set_error('code', t('You´ve entered an invalid confirmation code. Please try again.'));
      }
    }
    // if code not enter then alert
    else {
      form_set_error('code', t('Empty confirmation code. Please try again.'));
    }
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
  // else click send
  else {
    // generate randome code
    $code = mt_rand(1000, 9999);

    // push code to session
    $_SESSION['DAWAY_PROFILE_VALIDATE_EMAIL'] = $code;

    // load all user field
    $account = entity_metadata_wrapper('user', $user->uid);

    // invoke rule
    rules_invoke_event('daway_profile_mail_validation_send', $account, $code);

    drupal_set_message(t('Confirmation code was sent to your email.'));

    $commands[] = ajax_command_css('input[name="apply"]', array('display' => 'block'));
    $commands[] = ajax_command_css('input[name="send"]', array('display' => 'none'));
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
}

/**
 * Daway profile trust Passport form
 * @param $form
 * @param $form_state
 * @return array
 */
function daway_profile_validate_passport_popup($form, &$form_state) {

  global $user;
  $account = user_load($user->uid);

  $form = array();

  $form['#attributes']['id'] = 'daway-profile-validate';

  $form['form'] = array(
    '#prefix' => '<div class="container">',
    '#suffix' => '</div>',
  );

  $form['form']['text'] = array(
    '#markup' => '<div class="phone">' . t('Please upload a copy of the passport') . '</div>',
  );

  $form['form']['file'] = array(
    '#type' => 'file',
  );

  $form['form']['actions']['apply'] = array(
    '#value' => t('Apply'),
    '#type' => 'button',
    '#name' => 'apply',
    '#ajax' => array(
      'callback' => 'daway_profile_validate_passport_callback',
    ),
  );

  return $form;
}

/**
 * Ajax callback trust Passport form
 * @param $form
 * @param $form_state
 * @return mixed
 * @see daway_profile_validate_passport_popup()
 */
function daway_profile_validate_passport_callback($form, &$form_state) {

}






/***********************************************************************************************************************
 *  CODE NEED REVIEW!!!!
 */

function daway_profile_validate_mobile_popup($form, &$form_state) {

  global $user;
  $account = user_load($user->uid);

  $form = array();
  $form['#attributes']['class'][] = 'daway-profile-validate-mobile-popup';
  $form['form'] = array(
    '#prefix' => '<div id="validate-form">',
    '#suffix' => '</div>',
  );

  $phone = '';
  if (isset($account->field_user_phone['und']) || isset($form_state['values']['phone'])) {
    $phone = $account->field_user_phone['und'][0]['value'];
  }

  if ($phone) {
    $form['form']['text'] = array(
      '#markup' => '<div class="phone">' . t('The confirmation code will be send to: ') . $phone . '</div>',
    );

    $form['form']['phone'] = array(
      '#type' => 'value',
      '#value' => $phone,
    );
  }
  else {

    $form['form']['phone'] = array(
      '#title' => t('Mobile phone'),
      '#type' => 'textfield',
      '#attributes' => array('placeholder' => t('Enter your phone number'), 'class' => array('mobile')),
    );
  }

  $form['form']['code'] = array(
    '#title' => t('Code'),
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => t('Enter code')),
  );

  $form['form']['actions'] = array(
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  );

  $form['form']['actions']['send'] = array(
    '#value' => t('Send confirmation code'),
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'daway_profile_validate_mobile_popup_send',
      'wrapper' => 'validate-form',
    ),
  );
  $form['form']['actions']['submit'] = array(
    '#value' => t('Apply confirmation'),
    '#type' => 'submit',
    '#access' => FALSE,
    '#ajax' => array(
      'callback' => 'daway_profile_validate_mobile_popup_apply',
    ),
  );
  return $form;
}

function daway_profile_validate_mobile_popup_send(&$form, &$form_state) {
  $code = mt_rand(1000, 9999);
  $_SESSION['DAWAY_PROFILE_VALIDATE_MOBILE'] = $code;

  if ($form_state['values']['phone'] == '') {
    form_set_error('phone', t('Check your phone'));
    $commands[] = ajax_command_after(NULL, '<div id="messages">' . theme('status_messages') . '</div>');
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );

  }
  else {

    $phone = $form_state['values']['phone'];

    $phone = str_replace(' ', '', $phone);

    $account = user_load($form_state['user']);
    $account->field_user_phone['und'][0]['value'] = $phone;
    user_save($account);

//    if (hqsms_send($phone, $code)) {
    if (TRUE) {
      $form_state['rebuild'] = TRUE;
      $form['form']['actions']['send']['#access'] = FALSE;
      $form['form']['actions']['phone']['#access'] = FALSE;
      $form['form']['actions']['submit']['#access'] = TRUE;
      drupal_set_message('Confirmation code was sent to your phone.');
      return $form['form'];
    }
    else {
      form_set_error('phone', t('Confirmation code is not sent, please try later.'));
      $commands[] = ajax_command_after(NULL, '<div id="messages">' . theme('status_messages') . '</div>');
      return array(
        '#type' => 'ajax',
        '#commands' => $commands,
      );
    }
  }

}

function daway_profile_validate_mobile_popup_apply(&$form, &$form_state) {
  if (isset($form_state['values']['code'])) {
    if ($form_state['values']['code'] == $_SESSION['DAWAY_PROFILE_VALIDATE_MOBILE']) {
      unset($_SESSION['DAWAY_PROFILE_VALIDATE_MOBILE']);
      $account = user_load($form_state['user']);
      if (isset($account->field_user_security['und'])) {
        $security_entity_id = $account->field_user_security['und'][0]['value'];
        $field_collection = entity_metadata_wrapper('field_collection_item', $security_entity_id);
        $field_collection->field_fus_mobile = 1;
        $field_collection->save();
        user_save($account);
      }
      return array(
        '#type' => 'ajax',
        '#commands' => array(
          array(
            'command' => 'drupal_goto',
            'url' => url('user/' . $account->uid),
          ),
        ),
      );

    }
    else {
      form_set_error('phone', t('Check your code.'));
      return $form['form'];
      $commands[] = ajax_command_alert('BAD');
//      $commands[] = ajax_command_after(NULL, '<div id="messages">' . theme('status_messages') . '</div>');
      return array(
        '#type' => 'ajax',
        '#commands' => $commands,
      );
    }
  }
  else {
    form_set_error('phone', t('Check your code.'));
    return $form['form'];
    $commands[] = ajax_command_alert('BAD');
//    $commands[] = ajax_command_after(NULL, '<div id="messages">' . theme('status_messages') . '</div>');
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
}
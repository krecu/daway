<div id="slider">
	<div class="slider-wrapper">
		<span class="prefix"></span>
		<span class="opacity"></span>
		<ul class="slider">
		<?php foreach ($slides as $slide) { ?>
			<li><img src="<?php print $slide ?>" /></li>
		<?php } ?>
		</ul>
	</div>
</div>
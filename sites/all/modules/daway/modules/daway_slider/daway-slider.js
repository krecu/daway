(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.daway_slider_init = {
    attach: function (context, settings) {
      if ($('#slider ul.slider li', context).length > 1) {
        $('#slider ul.slider', context).once('init', function () {
          var slider_wrapper = $(this);
          var slider = slider_wrapper.bxSlider({
            mode: 'fade',
            auto: true,
            controls: false,
            useCSS: false,
            onSliderLoad: function(currentIndex){
              $('.bx-pager-item a').click(function(){
                var i = $(this).data('slide-index');
                slider.goToSlide(i);
                slider.stopAuto();
                setTimeout(function(){
                  slider.startAuto();
                },500);
              });
            }
          });
        })
      }
    }
  }
})(jQuery, Drupal, this, this.document);
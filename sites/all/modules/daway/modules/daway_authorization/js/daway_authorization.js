(function ($) {
    Drupal.theme.prototype.daway_authorization = function () {
        var html = ''
        html += '  <div id="ctools-modal-authorization">'
        html += '    <div class="ctools-modal-content">' // panels-modal-content
        html += '        <a class="close authorization" href="#">';
        html +=            Drupal.CTools.Modal.currentSettings.closeText;
        html += '        </a>';
        html += '      <div id="modal-wrapper">';
        html += '       <div id="modal-content" class="modal-content">';
        html += '       </div>';
        html += '      </div>';
        html += '    </div>';
        html += '  </div>';

        return html;
    }

})(jQuery);

(function ($, Drupal, window, document, undefined) {

    jQuery(document).ready(function($){
        $('ul.tabs a', '#daway-authorization').click(function(){
            $('ul.tabs a', '#daway-authorization').removeClass('active');
            $(this).addClass('active');
            $('.tab', '#daway-authorization').hide();
            $('.tab[data-tab="'+$(this).data('tab')+'"]', '#daway-authorization').show();
            return false;
        });

        $('.options input[type="radio"]', '#daway-authorization').change(function(){
            $('.form li', '#daway-authorization').hide();
            $('.form li.'+$(this).val(), '#daway-authorization').show();
        });
    })
})(jQuery, Drupal, this, this.document);
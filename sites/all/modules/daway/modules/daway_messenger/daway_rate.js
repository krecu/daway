(function ($) {

    Drupal.behaviors.daway_messenger = {
        attach: function(context, settings) {
            if ($('input#snipper', context).length) {
                $counter = $('input#snipper', context);
                if (!$counter.parents('.form-item-rate').find('a').length) {
                    $up = $('<a class="up" href="#up">');
                    $down = $('<a class="down" href="#up">');

                    $up.click(function() {
                        $val = $counter.val();
                        if ($val != '1') { $counter.val('1'); }
                        return false;
                    })

                    $down.click(function() {
                        $val = $counter.val();
                        if ($val != '-1') { $counter.val('-1'); }
                        return false;
                    })

                    $up.insertBefore($counter);
                    $down.insertAfter($counter);
                }
            }
        }
    };

})(jQuery);

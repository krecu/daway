(function ($) {
    Drupal.behaviors.daway_messenger = {
        attach: function(context, settings) {
            if ($('input#snipper', context).length) {
                $counter = $('input#snipper', context);
                if (!$counter.parents('.form-item-rate').find('a').length) {
                    $up = $('<a class="up" href="#up">');
                    $down = $('<a class="down" href="#up">');

                    $up.click(function() {
                        $val = $counter.val();
                        if ($val != '1') { $counter.val('1'); }
                        return false;
                    })

                    $down.click(function() {
                        $val = $counter.val();
                        if ($val != '-1') { $counter.val('-1'); }
                        return false;
                    })

                    $up.insertBefore($counter);
                    $down.insertAfter($counter);
                }
            }



            if ($('#messenger-list').length) {

                $(window).resize(function(){
                    $height = $(window).height() - 420;
                    $('#messenger-list').height($height);
                })
                $(window).trigger('resize');

                if (!$('#messenger-list').hasClass('mCustomScrollbar')) {
                    $("#messenger-list").mCustomScrollbar({
                        theme:"light",
                        mouseWheelPixels: 50,
                        scrollInertia:500
                    });
                    setTimeout(function(){
                    $('#messenger-list').mCustomScrollbar("scrollTo","bottom",{scrollInertia:200});
                    }, 500);
                } else {
                    $('#messenger-list').mCustomScrollbar("update");
                    $('#messenger-list').mCustomScrollbar("scrollTo","bottom",{scrollInertia:200});
                }
            }

        }
    };
})(jQuery);
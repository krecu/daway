<?php


function daway_company_user_create_form($form, &$form_state, $arg){


	if ($path = libraries_get_path('gmap')) {
		drupal_add_js($path.'/gmap3.min.js');
	}


	drupal_add_library('system', 'ui.datepicker');

	ctools_add_js('ajax-responder');

	$node = FALSE;
	$form = array();
	if ($arg == 'create') {
		$op = 'create';
	} elseif (is_numeric($arg)) {
		$op = 'edit';
	}

	if ($op == 'edit') {
		global $user;
		$node = node_load($arg);
		if (!$node) drupal_not_found();
		drupal_set_title(t('Editing').': '.$node->title);
		$form['#node'] = $node;
	}


	$form['pane_1'] = array(
		'#type' => 'fieldset',
	);
	$form['pane_1']['title'] = array(
		'#markup' => '<span class="fieldset-legend">'.t('Default').'</span>',
	);

	$form['pane_1']['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#attributes' => array('placeholder' => t('Name')),
		'#default_value' => ($node) ? $node->title : '',
	);

	$form['pane_1']['address'] = array(
		'#type' => 'textfield',
		'#title' => t('Address'),
		'#default_value' => ($node) ? geopoint_load($node->field_company_geopoint['und'][0]['target_id'])->name : '',
		'#description' => t('Example: Canary Wharf, London'),
		'#attributes' => array('placeholder' => t('Company address (address, city, station...)'), 'class' => array('geopoint'))
	);

	$type_term = taxonomy_get_tree(7);
	$type_company[0] = '';
	foreach ($type_term as $t) {
		$type_company[$t->tid] = t($t->name);
	}

	$form['pane_1']['row'] = array(
		'#prefix' => '<div class="row">',
		'#suffix' => '</div>',
	);
	$form['pane_1']['row']['type'] = array(
		'#type' => 'select',
		'#options' => $type_company,
		'#prefix' => '<div class="col left">',
		'#suffix' => '</div>',
		'#attributes' => array('data-placeholder' => t('Select type...'),),
		'#default_value' => ($node) ? $node->field_company_type['und'][0]['target_id'] : '',
	);

	$services_term = taxonomy_get_tree(8);
	$services_company[0] = '';
	foreach ($services_term as $term) {
		if($term->depth == 0){
			$parent = $term->name;
		}else{
				$services_company[$parent][$term->tid] = $term->name;
		}
	}

	$services = array();
	if ($node) {
		foreach ($node->field_company_services['und'] as $s) {
			$services[] = $s['target_id'];
		}
	}

	$form['pane_1']['row']['services'] = array(
		'#type' => 'select',
		'#options' => $services_company,
		'#prefix' => '<div class="col right">',
		'#suffix' => '</div>',
		'#multiple' => TRUE,
		'#attributes' => array('data-placeholder' => t('Select services...'),),
		'#default_value' => ($node) ? $services : '',
	);

	$form['pane_2'] = array(
		'#type' => 'fieldset',
	);
	$form['pane_2']['title'] = array(
		'#markup' => '<span class="fieldset-legend">'.t('Advance').'</span>',
	);

	$form['pane_2']['desc'] = array(
		'#title' => t('Desc'),
		'#type' => 'textarea',
		'#attributes' => array('placeholder' => t('Enter description about your company')),
		'#default_value' => ($node) ? $node->body['und'][0]['value'] : '',
	);

	$form['pane_2']['logo'] = array(
		'#title' => t('Logo'),
		'#type' => 'managed_file',
		'#upload_validators' => array(
			'file_validate_extensions' => array('gif png jpg jpeg'),
//			'file_validate_size' => array(1 * 500 * 500),
		),
		'#default_value' => ($node) ? $node->field_company_logo['und'][0]['fid'] : '',
		'#upload_location' => 'public://poster/',
	);

	$form['actions'] = array(
		'#prefix' => '<div class="form-actions">',
		'#suffix' => '</div>',
	);

	$form['actions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#ajax' => array(
			'callback' => 'daway_company_user_create_form_callback',
		),
	);

	return $form;
}

function daway_company_user_create_form_callback($form, &$form_state) {
	$errors = array();
	if (empty($form_state['values']['name'])) {
		$errors[] = ajax_command_css('input[name="name"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['type'])) {
		$errors[] = ajax_command_css('select[name="type"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['services'])) {
		$errors[] = ajax_command_css('input[name="services"]', array('border-color' => 'red'));
	}
	if (empty($form_state['values']['address'])) {
		$errors[] = ajax_command_css('input[name="address"]', array('border-color' => 'red'));
	}

	if (count($errors) > 0) {
		return array(
			'#type' => 'ajax',
			'#commands' => $errors
		);
	} else {

		// prepend field
		$point = new GeoPoint();
		$point->name = $form_state['values']['address'];
		$point = geopoint_save($point);

		$services = array();
		foreach ($form_state['values']['services'] as $s) {
			$services[] = array('target_id' => $s);
		}

		// create & save node

		global $user;
		if (isset($form['#node'])) {
			$node = $form['#node'];
		} else {
			$node = new stdClass();
			$node->type = "company";
			$node->language = LANGUAGE_NONE;
			$node->status = NODE_PUBLISHED;
			node_object_prepare($node);
			$node->uid = $user->uid;
		}


		$node->field_company_services['und'] = $services;
		$node->field_company_geopoint['und'][0]['target_id'] = $point->pid;
		$node->field_company_type['und'][0]['target_id'] = $form_state['values']['type'];
		// field_action_preview

		$node->title = $form_state['values']['name'];
		$node->body['und'][0]['value'] = strip_tags($form_state['values']['desc']);

		$node = node_submit($node);
		node_save($node);

		return array(
			'#type' => 'ajax',
			'#commands' =>  array(array(
				'command' => 'redirect',
				'url' => url('node/'.$node->nid, array()),
				'delay' => 0,
			)),
		);

	}
}
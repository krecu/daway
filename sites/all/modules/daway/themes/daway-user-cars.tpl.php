<?php if ($view_mode == 'profile') { ?>
  <div id="profile-cars">
    <div class="title"><?php print t('Vehicle Information') ?>:</div>
    <?php if (count($cars) > 0) { ?>
      <ul class="cars" class="row">
        <?php foreach ($cars as $item) { ?>
          <li class="cars-item">
            <div class="row">
              <?php $model = daway_car_get_model($item); ?>
              <div class="col left info col-1">
                <ul>
                  <li class="model"><span class="label"><?php print t('Vendor') ?>:</span><span
                      class="value"><?php print $model['vendor'] ?></span></li>
                  <li class="model"><span class="label"><?php print t('Model') ?>:</span><span
                      class="value"><?php print $model['model'] ?></span></li>
                  <li class="year"><span class="label"><?php print t('Type') ?>:</span><span
                      class="value"><?php print $model['type'] ?></span></li>
                  <li class="year"><span class="label"><?php print t('Year') ?>:</span><span
                      class="value"><?php print $model['year'] ?></span></li>
                  <li class="year"><span class="label"><?php print t('Color') ?>:</span><span
                      class="value"><?php print t($model['color']) ?></span></li>
                </ul>
              </div>
              <div class="col left info col-2">
                <ul>
                  <li class="model"><span class="label"><?php print t('Number') ?>:</span><span class="value">*</span>
                  </li>
                  <li class="year"><span class="label"><?php print t('Seat') ?>:</span><span class="value">*</span></li>
                  <?php $comform = daway_car_get_comfort($item) ?>
                  <?php foreach ($comform as $key => $value) { ?>
                    <li class="<?php print $key . ' ' . $value['class'] ?>"><span
                        class="label"><?php print $value['label'] ?>:</span><span
                        class="value"><?php print $value['value'] ?></span></li>
                  <?php } ?>
                  <li class="comfort"><span class="label"><?php print t('Comfort') ?>:</span><span
                      class="value"><?php print theme('daway_rate', array('average' => daway_car_get_rate($item))) ?></span>
                  </li>
                </ul>
              </div>
              <div class="col right slider">
                <?php $slides = daway_car_get_images($item) ?>
                <?php print theme('daway_user_cars_slider', array('images' => $slides)) ?>
              </div>
            </div>
          </li>
        <?php } ?>
      </ul>
    <?php }
    else { ?>
      <ul class="cars empty">
        <li class="cars-item"><a href="#"></a></li>
      </ul>
    <?php } ?>

  </div>
<?php }
elseif ($view_mode == 'trip') { ?>

  <?php if (count($cars) > 0) { ?>

  <div id="profile-cars" class="trip">
    <?php foreach ($cars as $item) { ?>
      <li class="cars-item">
        <div class="row">
          <?php $data = daway_car_get_model($item) ?>
          <div class="col left info">
            <ul class="property">
              <li class="vendor row item"><span class="label"><?php print t('Vendor') ?>:</span><span
                  class="value"><?php print $data['vendor'] ?></span></li>
              <li class="model row item"><span class="label"><?php print t('Model') ?>:</span><span
                  class="value"><?php print $data['model'] ?></span></li>
              <li class="type row item"><span class="label"><?php print t('Type') ?>:</span><span
                  class="value"><?php print $data['type'] ?></span></li>
              <li class="year row item"><span class="label"><?php print t('Year') ?>:</span><span
                  class="value"><?php print $data['year'] ?></span></li>
              <li class="color row item"><span class="label"><?php print t('Color') ?>:</span><span
                  class="value"><?php print t($data['color']) ?></span></li>
              <li class="comfort row item"><span class="label"><?php print t('Comfort') ?>:</span><span
                  class="value"><?php print theme('daway_rate', array('average' => daway_car_get_rate($item))) ?></span>
              </li>
              <li class="extra row item">
                <?php print theme('daway_extra_list', array('items' => daway_car_get_comfort($item))); ?>
              </li>
            </ul>
          </div>
          <div class="col right slider">
            <?php $slides = daway_car_get_images($item) ?>
            <?php print theme('daway_user_cars_slider', array('images' => $slides)) ?>
          </div>
        </div>
      </li>
    <?php } ?>
    </ul>
  </div>

  <?php } ?>
<?php }
elseif ($view_mode == 'admin') { ?>
  <?php global $user; ?>
  <div id="profile-cars" class="edit row">
    <div class="title"><?php print t('Vehicle Information') ?>:</div>
    <ul class="cars" class="row">
      <?php foreach ($cars as $item) { ?>
        <?php $slides = daway_car_get_images($item); ?>
        <li class="cars-item edit">
          <?php
          $user->car = $item;
          print $slides[0];
          print daway_profile_editcar_js_popuplink($user, 'editcar', $item);
          ?>
        </li>
      <?php } ?>
      <li class="cars-item add"><?php print daway_profile_js_popup_link($user, 'addcar') ?></li>
    </ul>
  </div>
<?php } ?>

<?php
	$hash = md5(microtime());
?>
<script>
	jQuery(document).ready(function($){

		var $map = $("#daway-map-<?php print $hash ?>");


		$map.bind('resizeMapUpdate', function(){
			createMap($map);
		});

		function createMap($map) {
			$map.gmap3({
				marker:{
					latLng: [<?php print $point->latitude ?>, <?php print $point->longitude ?>]
				},
				map:{
					options:{
						zoom: 5
					}
				}
			});
		}
	})
</script>
<div class="daway-map" id="daway-map-<?php print $hash ?>" style="width: 542px; height: 500px; border: 1px #ccc solid; background: #ccc url(http://daway.ru/sites/all/themes/develix/images/ajax-loader.gif) center no-repeat"></div>


<?php if ($average) { ?>
	<?php if ($type == 'inline') { ?>
	<div class="daway-rate <?php print $type ?>">
		<span class="average" style="width: <?php print $average ?>%"></span>
		<span class="bg"></span>
	</div>
	<?php } else { ?>
	<div class="daway-rate <?php print $type ?>">
		<?php for ($i = 1; $i<=5; $i++) { ?>
			<a class="<?php ($i < $average) ? print 'active' : '' ?>"><?php print $i ?></a>
		<?php } ?>
	</div>
	<?php } ?>
<?php } ?>
<?php
ctools_include('ajax');
ctools_include('modal');
ctools_modal_add_js();

if (is_numeric($to)) { $to = user_load($to); }

$user_to = user_view($to, 'modal');

$custom_style = array(
	'daway-messenger' => array(
		'modalSize' => array(
			'width' => 500,
			'height' => 240,
			'type' => 'fixed',
			'contentRight' => 0,
			'contentBottom' => 0,
		),
		'closeText' => t('Close'),
		'userHtml' => render($user_to),
		'throbber' => theme('image', array('path' => ctools_image_path('ajax-loader.gif', 'ajax_register'), 'alt' => t('Loading...'), 'title' => t('Loading'))),
		'animation' => 'fadeIn',
		'animationSpeed' => 'fast',
		'modalTheme' => 'daway_messenger',
	),
);
drupal_add_js($custom_style, 'setting');
ctools_add_js('daway_messenger', 'daway_messenger');

$output = ctools_modal_text_button(t('Send message'), 'js/nojs/messenger/'.$to->uid, t('Send message'), 'ctools-modal-daway-messenger');

ctools_include('plugins');

print $output;

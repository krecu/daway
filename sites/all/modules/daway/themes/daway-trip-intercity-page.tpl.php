
<div id="intercity">

	<div class="row" id="countries-list">
		<ul class="countries">
		<?php foreach ($countries as $key => $countries_point) { ?>
			<?php
			$class = array();
			$class[] = 'item';
			if (arg(1) == $key) $class[] = 'active';


			?>
			<li class="country"><?php print l(geopoint_name($countries_point->pid, 'short'), 'intercity/'.$countries_point->pid, array('attributes' => array('class' => $class))) ?></li>
		<?php } ?>
		</ul>
	</div>

	<div class="row">
		<div class="col left" id="city-list">
			<ul class="city">
			<?php foreach ($city as $city_point) { ?>
				<?php
				$class = array();
				$class[] = 'item';
				if (arg(2) == $key) $class[] = 'active';
				?>
				<li class="city"><?php print l(geopoint_name($city_point->pid, 'short'), 'intercity/'.$city_point->shema['country'].'/'.$city_point->pid, array('attributes' => array('class' => $class))) ?></li>
			<?php } ?>
			</ul>
		</div>
		<div class="col left <?php (!arg(2)) ? print 'default' : '' ?>" id="routes-from">
			<?php
			if (arg(2)) {
				print '<h2 class="title">Из '.geopoint_name($city[arg(2)]->pid, 'short').'</h2>';
			} elseif (arg(1)) {
				print '<h2 class="title full">'.t('Popular routes to').' '.geopoint_name($countries[arg(1)]->pid, 'short').'</h2>';
			} else {
				print '<h2 class="title full">'.t('Popular routes').'</h2>';
			}
			?>
			<ul class="routes">
				<?php foreach ($routes['departure'] as $item) { ?>
					<li class="route"><?php print l(geopoint_name($item->pid1, 'short') . ' → ' . geopoint_name($item->pid2, 'short'), 'routes/'.$item->rid, array('attributes' => array('class' => 'item'))) ?></li>
				<?php }  ?>
			</ul>
			<?php
			if (arg(2)) {
				print '<h3 class="title">'.l(t('All routes from').' '.geopoint_name($city[arg(2)]->pid, 'short'), 'geopoint/'.arg(2)).'</h3>';
			} elseif (arg(1111)) {
				print '<h3 class="title">'.l(t('All routes from').' '.geopoint_name($countries[arg(1)]->pid, 'short'), 'geopoint/'.arg(1)).'</h3>';
			}
			?>
		</div>
		<div class="col left <?php (!arg(2)) ? print 'default' : '' ?>" id="routes-to">
			<?php
			if (arg(2)) {
				print '<h2 class="title">'.t('To').' '.geopoint_name($city[arg(2)]->pid, 'short').'</h2>';
			}
			?>
			<ul class="routes">
				<?php foreach ($routes['arrival'] as $item) { ?>
					<li class="route"><?php print l(geopoint_name($item->pid1, 'short') . ' → ' . geopoint_name($item->pid2, 'short'), 'routes/'.$item->rid, array('attributes' => array('class' => 'item'))) ?></li>
				<?php }  ?>
			</ul>
			<?php
			if (arg(2111)) {
				print '<h3 class="title">'.l(t('All routes from').' '.geopoint_name($city[arg(2)]->pid, 'short'), 'geopoint/'.arg(2)).'</h3>';
			}
			?>
		</div>
	</div>

</div>
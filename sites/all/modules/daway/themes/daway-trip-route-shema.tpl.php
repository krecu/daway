<?php

$route_shema = array();
$route_shema_date = array();
$route_shema_parent = FALSE;
$route_shema_default = FALSE;

$route_shema_date_list = array();
$route_shema_parent_backward = false;

foreach ($shema as $item) {

	$route_item = routes_load($item['rid']);

	if (($route_item->pid1 != '') && ($route_item->pid2 != '')) {
		$route_shema_date_list[$route_item->pid1.'+'.$route_item->pid2]['route'] = array(
			'route' => $route_item,
			'href' => url('routes/'.$route_item->rid),
		);
		$route_shema_date_list[$route_item->pid1.'+'.$route_item->pid2]['date'][$item['date']] = array(
			'name' => geopoint_name($route_item->pid1, 'short') . ' → ' . geopoint_name($route_item->pid2, 'short'),
			'price' => htmlspecialchars(theme('daway_language_currency_field', array('value' => $item['price']))),
			'timestamp' => $item['date'],
			'start' => array(
				'pid' => $route_item->pid1,
				'date' => format_date($item['date'], 'custom', 'd F Y'),
				'time' => format_date($item['date'], 'custom', 'H:i'),
			),
			'end' => array(
				'pid' => $route_item->pid2,
				'date' => format_date($item['date1'], 'custom', 'd F Y'),
				'time' => format_date($item['date1'], 'custom', 'H:i'),
			)
		);
	}

	$route_shema[$item['rid']] = array(
		'route' => routes_load($item['rid']),
		'type' => $item['type'],
	);

}


$route_shema_parent = routes_load_by_geopoint($points[0]->pid, $points[count($points)-1]->pid);

$default_display = 'line';

if ($backward) {
	$route_shema_parent_backward = routes_load_by_geopoint($points[count($points)-1]->pid, $points[0]->pid);
}

if (isset($_GET['route'])) {
	$route_shema_default = $route_shema[$_GET['route']]['route'];

	if ($route_shema[$_GET['route']]['type'] == 'backward') {
		$default_display = 'back';
	}

} else {
	$route_shema_default = $route_shema_parent;
}


$point_value = array();
$slider_count = count($points);

// create slider values and list values
foreach ($points as $key => $item) {
	$slider_value[] = $key;
}

?>

<script>

	jQuery(document).ready( function($){

		var $point_slider = $("#points-range div");
		var $default_display = "<?php print $default_display ?>";
		var $point_item = $("#points-item ul."+$default_display);

		$('a[data-pid="'+<?php print $route_shema_default->pid1 ?>+'"]', $point_item).addClass('selected');
		$('a[data-pid="'+<?php print $route_shema_default->pid2 ?>+'"]', $point_item).addClass('selected');

		$pid_1 = $('a[data-pid="'+<?php print $route_shema_default->pid1 ?>+'"]', $point_item).data('key');
		$pid_2 = $('a[data-pid="'+<?php print $route_shema_default->pid2 ?>+'"]', $point_item).data('key');

		$point_slider.slider({
			orientation: "vertical",
			step: 1,
			min: 1,
			max : <?php print $slider_count ?>,
			range: true,
			values: [ $pid_2, $pid_1 ],
			slide: function( event, ui ) {

				if (ui.values[0] == ui.values[1]) return false;

				$data_point_1 = parseFloat(ui.values[0]);
				$data_point_2 = parseFloat(ui.values[1]);

				$('a', $point_item).removeClass('selected');

				$('a[data-key="'+$data_point_1+'"]', $point_item).addClass('selected');
				$('a[data-key="'+$data_point_2+'"]', $point_item).addClass('selected');

				$data_point_2_pid = $('a[data-key="'+$data_point_1+'"]', $point_item);
				$data_point_1_pid = $('a[data-key="'+$data_point_2+'"]', $point_item);


				$('.dates-list').removeClass('active');
				$('.dates-list[data-route="'+$data_point_1_pid.data('pid')+'+'+$data_point_2_pid.data('pid')+'"]').addClass('active');
				$('.dates-list.active a').first().click();

				$('.trip-nav .search a').attr('href', $('.dates-list[data-route="'+$data_point_1_pid.data('pid')+'+'+$data_point_2_pid.data('pid')+'"]').data('link'));
				$('.trip-nav .search a .middle.helper').text($('h1#page-title').text());
				$(window).trigger('resize');
			}
		});
		$point_slider.trigger('slidechange');
		$point_slider.height($point_item.height()-10);

		$('ul', '.dates-list').jcarousel({
			scroll: 1,
			visible: 3,
			itemFallbackDimension: 300
		});


		$('a', '.dates-list').click(function(){

			$('a', '.dates-list').removeClass('active');
			$(this).addClass('active');

			$('a[data-pid="'+$(this).data('start')+'"] .time', $point_item).text('<?php print t('departure in') ?>: '+$(this).data('time-start'));
			$('a[data-pid="'+$(this).data('end')+'"] .time', $point_item).text('<?php print t('arrival in') ?>: '+$(this).data('time-end'));

			$point_slider.trigger('slidechange');

			$('h1#page-title').text($(this).data('route-name'));
			$('.trip-driver-summ .last .value').html($(this).data('route-price'));

				var msg = $('.trip-driver-summ .last .value');
				msg.stop().fadeIn('slow',function(){
					msg.animate({opacity:1},100,function(){
						msg.fadeOut('slow',function(){
							msg.fadeIn('slow');
						})
					})
				});


			return false;
		})

		if ($('.dates-list.active a.active').length) {
			$('.dates-list.active a.active').click();
		} else {
			$('.dates-list.active a').first().click();
		}

		if ($('#route-shema-backward').length) {
			$('#route-shema-backward input').change(function(){
				if ($('#route-shema-backward input:checked').length == 0) {
					$point_item = $("#points-item ul.points.line")
					$("#points-item ul.points.back").hide();
					$("#points-item ul.points.line").show();

				} else {
					$point_item = $("#points-item ul.points.back");
					$("#points-item ul.points.line").hide();
					$("#points-item ul.points.back").show();
				}

				$('a', $point_item).removeClass('selected');
				$val = $point_slider.slider("option", "values");
				$('a[data-key="'+$val[0]+'"]', $point_item).addClass('selected');
				$('a[data-key="'+$val[1]+'"]', $point_item).addClass('selected');

				$(window).trigger('resize');


				if ($('.dates-list.active a.active').length) {
					$('.dates-list.active a.active').click();
				} else {
					$('.dates-list.active a').first().click();
				}
			})
		}


	})
</script>

<div id="route-shema" class="row">
	<div id="route-shema-range" class="row">
		<div id="points-range" class="col left"><div></div></div>
		<div id="points-item" class="col left">

			<ul class="points line" <?php if ($default_display != 'line') { ?>style="display: none" <?php } ?>>
			<?php foreach ($points as $key => $item) { ?>
				<li class="point <?php print ($key == 0) ? 'first' : ''; print ($key == count($points)-1) ? 'last' : '';?>">
					<a data-key="<?php print count($points)-$key ?>" data-pid="<?php print $item->pid ?>" href="<?php print url('geopoint/'.$item->pid) ?>">
						<span class="name"><?php print geopoint_name($item->pid, 'short') ?></span>
						<span class="time"><?php print t('departure in') ?></span>
					</a>
					<?php if ($key == 0) { ?>
					<span class="label"><?php print t('Departure') ?>:</span>
					<?php } elseif ($key == count($points)-1) {?>
					<span class="label"><?php print t('Stopover') ?>:</span>
					<?php } elseif (($key == ceil(count($points)/2)-1) && (count($points) > 2)) { ?>
						<span class="label"><?php print t('Arrival') ?>:</span>
					<?php } ?>
				</li>
			<?php } ?>
			</ul>

			<ul class="points back" <?php if ($default_display != 'back') { ?>style="display: none" <?php } ?>>
				<?php $points = array_reverse($points) ?>
				<?php foreach ($points as $key => $item) { ?>
					<li class="point <?php print ($key == 0) ? 'first' : ''; print ($key == count($points)-1) ? 'last' : '';?>">
						<a data-key="<?php print count($points)-$key ?>" data-pid="<?php print $item->pid ?>" href="<?php print url('geopoint/'.$item->pid) ?>">
							<span class="name"><?php print geopoint_name($item->pid, 'short') ?></span>
							<span class="time"><?php print t('departure in') ?></span>
						</a>
						<?php if ($key == 0) { ?>
							<span class="label"><?php print t('Departure') ?>:</span>
						<?php } elseif ($key == count($points)-1) {?>
							<span class="label"><?php print t('Stopover') ?>:</span>
						<?php } elseif (($key == ceil(count($points)/2)-1) && (count($points) > 2)) { ?>
							<span class="label"><?php print t('Arrival') ?>:</span>
						<?php } ?>
					</li>
				<?php } ?>
			</ul>

		</div>
	</div>
	<div id="route-shema-date" class="row">
		<span class="label">Когда:</span>
			<?php foreach ($route_shema_date_list as $route_shema_date_route => $route_shema_date_list_item) { ?>
			<div class="dates-list <?php if ($route_shema_date_route == $route_shema_default->pid1.'+'.$route_shema_default->pid2) {print 'active';} ?>" data-route="<?php print $route_shema_date_route ?>" data-link="<?php print $route_shema_date_list_item['route']['href'] ?>">
				<ul class="dates">
					<?php foreach ($route_shema_date_list_item['date'] as $key => $item) { ?>
					<li class="date">
						<a
							data-key="<?php print $key ?>"
							data-start="<?php print $item['start']['pid'] ?>"
							data-end="<?php print $item['end']['pid'] ?>"
							data-time-start="<?php print $item['start']['time'] ?>"
							data-time-end="<?php print $item['end']['time'] ?>"
							data-route-name="<?php print $item['name'] ?>"
							data-route-price="<?php print $item['price'] ?>"
							data-route-timestamp="<?php print $item['timestamp'] ?>"
							class="<?php if (isset($_GET['date'])) { if ($_GET['date'] == $item['timestamp']) { print 'active'; }} ?>"
							href="#"><?php print $item['start']['date'] ?></a>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>

	</div>


	<?php if ($backward) { ?>
		<div id="route-shema-backward" class="row trip-field trip-backward">
			<span class="trip-field-label"><?php print t('Backward') ?>:</span>
			<span class="trip-field-value">
				<form>
					<div class="form-item">
						<input type="checkbox" class="dawayCheckbox" checked="checked">
						<span class="desc"><?php print t('Show backward trip?') ?></span>
					</div>
				</form>
			</span>
		</div>
	<?php } ?>
</div>
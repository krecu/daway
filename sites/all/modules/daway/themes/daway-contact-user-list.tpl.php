<?php
$users = daway_contact_get_by_nid($nid);
if (!$users) return;
$form_user_list = drupal_get_form('daway_contact_user_list_form', $nid);
?>
<div class="users-list">
  <h4 class="title"><?php print t('Who of these people was your companion?') ?></h4>
  <div class="users-list-wrapper"><?php print drupal_render($form_user_list); ?></div>
</div>
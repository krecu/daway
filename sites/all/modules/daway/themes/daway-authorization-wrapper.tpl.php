<div id="daway-authorization">
	<ul class="tabs row">
		<li><a data-tab="social" class="first active" href="#"><?php print t('Easy sign in') ?></a></li>
		<li><a data-tab="drupal" class="last" href="#"><?php print t('Sign in with e-mail') ?></a></li>
	</ul>

	<div class="social row tab" data-tab="social">
		<span class="or"><?php print t('Or') ?></span>
		<div class="row">
			<div class="col left">
				<?php print social_api_get_link('Sign in with Facebook', 'Facebook', 'login') ?>
			</div>
			<div class="col right">
				<?php print social_api_get_link('Sign in with Twitter', 'Twitter', 'login') ?>
				<?php print social_api_get_link('Sign in with Google', 'Google', 'login') ?>
				<div class="desc"><?php print t('For login or register on the site you can use any social network or multiple') ?></div>
				<ul class="other">
					<li><?php print social_api_get_link('Sign in with Vkontakte', 'Vkontakte', 'login') ?></li>
					<li><?php print social_api_get_link('Sign in with Yandex', 'Yandex', 'login') ?></li>
					<li><?php print social_api_get_link('Sign in with Odnoklassniki', 'Odnoklassniki', 'login') ?></li>
					<li><?php print social_api_get_link('Sign in with Mailru', 'Mailru', 'login') ?></li>
					<li><?php print social_api_get_link('Sign in with Instagram', 'Instagram', 'login') ?></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="drupal row tab" data-tab="drupal" style="display: none;">
		<div class="options row">
			<div class="form-item form-type-radio simpleRadiobox"><input value="login" id="options-type-login" name="options-type" type="radio" checked><label for="options-type-login"><?php print t('Sign in') ?></label></div>
			<div class="form-item form-type-radio simpleRadiobox"><input value="register" id="options-type-reg" name="options-type" type="radio"><label for="options-type-reg"><?php print t('Register') ?></label></div>
			<div class="form-item form-type-radio simpleRadiobox"><input value="forgot" id="options-type-forgot" name="options-type" type="radio"><label for="options-type-forgot"><?php print t('Forgot password') ?></label></div>
		</div>
		<ul class="form">
			<li class="login"><?php print render($login_form) ?></li>
			<li class="register" style="display: none;"><?php print render($register_form) ?></li>
			<li class="forgot" style="display: none;"><?php print render($pass_form) ?></li>
		</ul>
	</div>
	<div class="terms"><?php print t('By accessing the Site you acknowledge that you have read '.l('terms of Use', 'node/1').' and agree with them.') ?></div>
</div>

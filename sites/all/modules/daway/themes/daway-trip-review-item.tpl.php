<?php
$account = user_load($data['user']);
?>
<div class="row">
  <div class="user col left">
    <?php print daway_profile_get_avatar($account, TRUE) ?>
    <span class="icon <?php print ($data['rate'] > 0) ? 'plus' : 'minus' ?>"></span>
  </div>
  <div class="desc col right">
    <div class="name"><?php print l(format_username($account), 'user/' . $account->uid) ?></div>
    <div class="body"><?php print $data['body']['value'] ?></div>
  </div>
</div>
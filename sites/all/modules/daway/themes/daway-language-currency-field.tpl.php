<?php

$amount_from = $value;
$exchange_rate = CurrencyExchanger::load('USD', daway_language_currency_default());
$price = bcmul($exchange_rate, $amount_from);

?>

<span class="price-value"><?php print number_format($price, 0, ',', ' ') ?> <span class="suffix"><?php print daway_language_currency_default() ?></span></span>
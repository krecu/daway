<div>
	<div id="trips-container">
		<ul id="trips-tab" class="row" <?php if (!$tabs) { ?>style="display: none;" <?php } ?>>
			<li class="first"><a data-triptype="65540" href="#"><?php print t('Drivers') ?></a></li>
			<li><a data-triptype="65541" href="#"><?php print t('Passengers') ?></a></li>
			<li class="last"><a data-triptype="*" class="active" href="#"><?php print t('All') ?></a></li>
		</ul>
		<div class="view-trips">

			<div class="row view-header" <?php if (!$header) { ?>style="display: none;" <?php } ?>>
				<div class="col left">
					<div id="trips-sorting" class="form-item sort">
						<label><?php print t('Sort by') ?></label>
						<select>
							<option value="date"><?php print t('by date') ?></option>
							<option value="price"><?php print t('by price') ?></option>
							<option value="rating"><?php print t('by rating') ?></option>
							<option value="seats"><?php print t('by seats') ?></option>
							<option value="distance"><?php print t('by distance from me') ?></option>
						</select>
						<input name="sorting" type="checkbox">
					</div>
				</div>
				<div class="col right">
					<?php
					$display = $view_mode;
					if (isset($_GET['display'])) $display = $_GET['display'];

					?>
					<ul id="trips-display" class="row">
						<li class="first"><a class="grid <?php print ($display=='grid') ? 'active' : '' ?>" data-tripdisplay="grid" href="?display=grid"><span class="icon"></span>grid</a></li>
						<li><a class="inline <?php print ($display=='inline') ? 'active' : '' ?>" data-tripdisplay="inline" href="?display=inline"><span class="icon"></span>inline</a></li>
						<li class="last"><a data-tripdisplay="map" class="map <?php print ($display=='map') ? 'active' : '' ?>" href="#"><span class="icon"></span>map</a></li>
					</ul>
				</div>
			</div>

			<div id="trips-list">
				<div class="view-content">
					<?php print implode('', $items) ?>
				</div>
			</div>
			<div id="paginator-container">
				<?php print theme('pager') ?>
			</div>
		</div>
	</div>

</div>
<script>
	jQuery(document).ready(function($){
		var waypts = [];
		<?php if (isset($points)) { ?>
			<?php foreach ($points as $item) { ?>
			waypts.push({ location: '<?php print $item->name ?>', stopover: true });
			<?php } ?>
		<?php } ?>


		var $map = $("#daway-trip-route-map-<?php print $hash ?>");

		$map.bind('resizeMapUpdate', function(){
			createMap($map);
		});

		function createMap($map) {
			$map.gmap3({
				getroute:{
					options:{
						origin: "<?php print $departure->name ?>",
						destination: "<?php print $arrival->name?>",
						travelMode: google.maps.DirectionsTravelMode.DRIVING,
						waypoints: waypts,
						optimizeWaypoints: true
					},
					callback: function(results){
						if (!results) {
							console.log('Map result fail');
							return;
						}
						$(this).gmap3({
							map:{
								options:{
									zoom: 13,
									center: [-33.879, 151.235]
								}
							},
							directionsrenderer:{
								options:{
									directions:results
								}
							}
						});
					}
				}
			});
		}


//		$map.trigger('resizeMapUpdate');
	})
</script>
<div class="daway-trip-route-map" id="daway-trip-route-map-<?php print $hash ?>" style="width: 542px; height: 500px; border: 1px #ccc solid; background: #ccc"></div>


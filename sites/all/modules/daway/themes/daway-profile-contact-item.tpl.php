<div class="contact node">
	<?php
	$date = format_date($data['time'], 'custom', 'H:i,d.m.Y');
	$node = node_load($data['trip']);

	$points = daway_trip_get_points($node->nid, 'other');
	$route = routes_load_by_geopoint($points[0]->pid, $points[count($points)-1]->pid);

	?>
	<div class="row">
		<div class="col left icon passenger"><a href="#"></a></div>
		<div class="col left user"><?php print render(user_view(user_load($data['user']), 'tripviewfull')) ?></div>
		<div class="col left time"><span><?php print str_replace(',', ',<br>', $date) ?></span></div>
		<div class="col left title"><?php print l(routes_get_normalise_name($route, ' →<br>'), 'node/'.$node->nid, array('html' => TRUE)) ?></div>

	</div>
</div>
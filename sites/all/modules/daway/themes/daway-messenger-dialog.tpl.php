<div class="row">
	<div class="col left with">
		<?php print daway_profile_get_avatar($data['with'], TRUE) ?>
		<div class="name"><?php print l($data['with']->name, 'user/'.$data['with']->uid) ?></div>
		<div class="created"><?php print format_date($data['created'], 'custom', 'd/m/Y H:i') ?></div>
	</div>
	<div class="col right message">
		<?php print daway_profile_get_avatar($data['who'], TRUE) ?>
		<?php print $data['message'] ?>
	</div>

</div>
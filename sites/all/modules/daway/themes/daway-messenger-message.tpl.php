<div class="row">
	<div class="col left">
    <?php print daway_profile_get_avatar($data['from'], TRUE) ?>
	</div>
	<div class="col left body">
		<div class="name"><?php print l($data['from']->name, 'user/'.$data['from']->uid) ?></div>
		<div class="text"><?php print $data['message'] ?></div>
	</div>
	<div class="col right created">
    <?php print format_date($data['created'], 'custom', 'd F Y H:i') ?>
  </div>
  <?php print daway_messenger_js_delete_popup_link('message', $data['mid']) ?>
</div>
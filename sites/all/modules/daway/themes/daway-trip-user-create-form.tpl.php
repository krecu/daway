<?php
global $user;
$account = user_load($user->uid);
if (!isset($account->field_user_address['und'])) {
  ?>
  <div class="geoposition" style="display: none;">
    <?php print daway_profile_js_popup_link($user, 'geoposition'); ?>
  </div>
<?php
}
else {
  ?>
  <div class="geoposition" style="display: none;"
       data-position="<?php print geopoint_load($account->field_user_address['und'][0]['target_id'])->name ?>"></div>
<?php } ?>

<script>
  jQuery(document).ready(function ($) {
    if ($('.geoposition a').length) {
      $('.geoposition a').click();
    }
  })
</script>

<div style="display: none" class="date-dialog" title="<?php print t('Create trip') ?>">
  <h4><?php print t('What kind of trip you want to make on that day?') ?></h4>

  <div class="form-item form-type-checkbox ">
    <input type="checkbox" class="form-checkbox" id="trip-date-outbound"><label
      for="trip-date-outbound"><span><?php print t('Outbound trip') ?></span></label>
  </div>
  <div class="form-item form-type-checkbox ">
    <input type="checkbox" class="form-checkbox" id="trip-date-return"><label
      for="trip-date-return"><span><?php print t('Return trip') ?></span></label>
  </div>
</div>

<div class="row">
  <div class="col left">


    <?php print drupal_render($form['pane_1']) ?>
    <?php print drupal_render($form['pane_2']) ?>
    <fieldset class="form-wrapper" id="edit-pane-3">
      <?php print drupal_render($form['pane_3']['dates_from']) ?>
      <?php print drupal_render($form['pane_3']['dates_to']) ?>
      <span class="fieldset-legend"><?php print t('Date') ?></span>

      <div class="form-item form-type-checkbox round-trip">
        <input type="checkbox" class="form-checkbox" id="trip-date-type">
        <label for="trip-date-type"><?php print t('Roundtrip') ?></label>
      </div>

      <div class="fieldset-wrapper">

        <div id="edit-pane-3-simple" class="onetime">
          <div class="form-item departure form-type-textfield">
            <label><?php print t('Departure date:') ?></label>
            <input type="text" placeholder="DD.MM.YY" class="datetimepicker">
          </div>
          <div class="form-item return form-type-textfield round">
            <label><?php print t('Return date:') ?></label>
            <input type="text" placeholder="DD.MM.YY" class="datetimepicker">
          </div>
        </div>

        <div id="edit-pane-3-recurring" class="recurring">
          <div class="row">
            <div class="col left">
              <div class="form-item form-type-week">
                <label><?php print t('Outbound days') ?></label>
                <ul class="day outbound">
                  <li class="first"><a data-calendar-index="1" data-recurring-type="outbound" href="#mon">Mo</a></li>
                  <li><a data-calendar-index="2" data-recurring-type="outbound" href="#tue">Tu</a></li>
                  <li><a data-calendar-index="3" data-recurring-type="outbound" href="#wed">We</a></li>
                  <li><a data-calendar-index="4" data-recurring-type="outbound" href="#thu">Th</a></li>
                  <li><a data-calendar-index="5" data-recurring-type="outbound" href="#fri">Fr</a></li>
                  <li><a data-calendar-index="6" data-recurring-type="outbound" href="#sat">Sa</a></li>
                  <li class="last"><a data-calendar-index="0" data-recurring-type="outbound" href="#sun">Su</a></li>
                </ul>
              </div>

              <div class="form-item form-type-week round">
                <label><?php print t('Return days') ?></label>
                <ul class="day return">
                  <li class="first"><a data-calendar-index="1" data-recurring-type="return" href="#mon">Mo</a></li>
                  <li><a data-calendar-index="2" data-recurring-type="return" href="#tue">Tu</a></li>
                  <li><a data-calendar-index="3" data-recurring-type="return" href="#wed">We</a></li>
                  <li><a data-calendar-index="4" data-recurring-type="return" href="#thu">Th</a></li>
                  <li><a data-calendar-index="5" data-recurring-type="return" href="#fri">Fr</a></li>
                  <li><a data-calendar-index="6" data-recurring-type="return" href="#sat">Sa</a></li>
                  <li class="last"><a data-calendar-index="0" data-recurring-type="return" href="#sun">Su</a></li>
                </ul>
              </div>

              <div class="form-item departure form-type-textfield">
                <label><?php print t('Starting from:') ?></label>
                <input type="text" placeholder="DD.MM.YY" class="datetimepicker">
              </div>

              <div class="form-item return form-type-textfield">
                <label><?php print t('Until:') ?></label>
                <input type="text" placeholder="DD.MM.YY" class="datetimepicker">
              </div>
            </div>
            <div class="col right">
              <div class="form-item form-type-datepicker-inline"></div>
            </div>
          </div>

        </div>

      </div>

    </fieldset>
    <fieldset class="form-wrapper" id="edit-pane-4">
      <?php print drupal_render($form['pane_4']['price']) ?>
      <span class="fieldset-legend"><?php print t('Price') ?></span>

      <div class="fieldset-wrapper">
        <div id="edit-pane-4-default"></div>
        <div id="edit-pane-4-route"></div>
      </div>

      <div class="form-item form-type-checkbox temp-route">
        <input type="checkbox" class="form-checkbox" id="trip-route-hide" checked="checked">
        <label for="trip-route-hide"><?php print t('Prices of intermediate routes calculated automatically') ?></label>
      </div>
    </fieldset>

    <?php print drupal_render_children($form); ?>
  </div>
  <div class="col right">
    <div id="route-map"></div>
  </div>
</div>

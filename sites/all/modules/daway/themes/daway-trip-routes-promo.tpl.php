<h2 class="title"><?php print geopoint_name($country->pid, 'short'); ?></h2>
<ul class="routes">
	<?php foreach ($routes as $item) { ?>
		<li><?php print l(geopoint_name($item->pid1, 'short') . ' → ' . geopoint_name($item->pid2, 'short'), 'routes/'.$item->rid, array('html' => TRUE)) ?></li>
	<?php } ?>
</ul>

<?php
$pid1 = 0;
if (isset($country->shema['country'])) {
  $pid1 = $country->shema['country'];
}



?>
<h3 class="country"><?php print l('Все маршруты '.geopoint_name($country->pid, 'short'), 'intercity/'.$pid1.'/'.$country->pid) ?></h3>
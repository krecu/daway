<script>
	jQuery(document).ready(function($){
		$('ul.car-slider').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			mode: 'fade',
			auto: true,
			controls: false,
			pager: true
		});
	})
</script>
<ul class="car-slider">
	<?php foreach ($images as $item) { ?>
	<li><?php print  $item ?></li>
	<?php } ?>
</ul>
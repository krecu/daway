<?php
function daway_car_get_comfort($node) {
  if (is_object($node)) {
    $node = node_load($node->nid);
  }
  else {
    $node = node_load($node);
  }
  $data = array();
  $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
  $comfort_entity_id = $node->field_car_comfort['und'][0]['value'];
  $field_collection = entity_metadata_wrapper('field_collection_item', $comfort_entity_id);

  foreach ($fields_info as $field_name => $value) {
    $field_data = field_info_field($field_name);
    $val = $field_collection->{$field_name}->value();

    $val = ($val) ? '1' : '0';
    $label = (isset($field_data['settings']['allowed_values'][$val])) ? $field_data['settings']['allowed_values'][$val] : 'N/A';
    $data[$field_name] = array(
      'label' => t($value['label']),
      'value' => t($label),
      'class' => ($val) ? 'active' : 'false',
    );
  }

  return $data;
}

/**
 * Get summary info about car
 * @param $node
 * @return array
 */
function daway_car_get_model($node) {
  if (is_object($node)) {
    $node = node_load($node->nid);
  }
  else {
    $node = node_load($node);
  }
  $car_wrapper = entity_metadata_wrapper('node', $node);
  $car_data = $car_wrapper->field_car_car->value();

  $manufactures = array_values(carfield_get_manufactures($car_data['manufactures']));
  $model = array_values(carfield_get_models($car_data['model']));
  $mode = array_values(carfield_get_modifications($car_data['modification']));
  $type = array_values(carfield_get_types($car_data['type']));

  $data = array(
    'vendor' => ((!empty($manufactures[0])) ? $manufactures[0]->name : ''),
    'model' => ((!empty($model[0])) ? $model[0]->name : ''),
    'color' => taxonomy_term_load($node->field_car_color['und'][0]['target_id'])->name,
    'type' => ((!empty($type[0])) ? $type[0]->name : ''),
    'year' => $car_data['year'],
    'modification' => ((!empty($mode[0])) ? $mode[0]->name : ''),
  );

  return $data;
}

/**
 * Return image wrapper for car
 * @param $node
 * @return array
 */
function daway_car_get_images($node) {
  if (is_object($node)) {
    $node = node_load($node->nid);
  }
  else {
    $node = node_load($node);
  }
  $data = array();
  if (isset($node->field_car_gallery['und'])) {
    foreach ($node->field_car_gallery['und'] as $item) {
      $image = array(
        'style_name' => 'preview_300x230',
        'path' => $item['uri'],
        'alt' => '',
        'title' => ''
      );
      $data[] = theme('image_style', $image);
    }
  }
  else {
    $default = drupal_get_path('theme', 'develix').'/images/no-photo.gif';
    $data[] = '<img src="/'.$default.'">';
  }
  return $data;
}

function daway_car_get_body($node) {
  if (is_object($node)) {
    $node = node_load($node->nid);
  }
  else {
    $node = node_load($node);
  }

  return $node->body['und'][0]['value'];
}

function daway_car_get_rate($node) {
  if (is_object($node)) {
    $node = node_load($node->nid);
  }
  else {
    $node = node_load($node);
  }
  $fields_info = field_info_instances('field_collection_item', 'field_car_comfort');
  $comfort_entity_id = $node->field_car_comfort['und'][0]['value'];
  $field_collection = entity_metadata_wrapper('field_collection_item', $comfort_entity_id);

  $count_field = count($fields_info);
  $no_empty = 0;
  foreach ($fields_info as $field_name => $value) {
    $field_data = field_info_field($field_name);
    $val = $field_collection->{$field_name}->value();
    if (!empty($val)) {
      $no_empty++;
    }
  }

  return ceil(($no_empty * 100) / $count_field);

}
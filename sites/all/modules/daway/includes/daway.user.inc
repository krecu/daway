<?php

function daway_user_get_social_link($uid, $provider){
	global $user;
	$friend = social_api_get_friends($uid, $provider);
	if ($friend) {
		$count = count($friend);
		$profile = social_api_get_profile($uid, $provider);
		$data = unserialize($profile['data']);
		// $prefix = 'друг';
		// if ($count % 10 > 1) $prefix = 'друга';
		// if ($count % 10 > 5) $prefix = 'друзей';

        $prefix = t('friend');
        if ($count % 10 > 1) $prefix = t('friends');
        if ($count % 10 > 5) $prefix = t(' friends ');

		return l($count.' '.$prefix, $data['profile']['profileURL'], array('type' => 'external'));
	} else {
		if ($user->uid == $uid) {
			return social_api_get_link(t('not connected') , $provider, 'login');
		} else {
			return t('not connected');
		}
	}
}

function daway_user_get_age($user) {
	$account = user_load($user->uid);
	if (isset($account->field_user_age['und'])) {
		$date = $account->field_user_age['und'][0]['value'];
		$time = time();
		$age = ceil(($time-$date)/31556926);

		return format_plural($age, '1 year', '@count '.t('years'));
	} else {
		return 'not set';
	}
}

function daway_user_get_cars($user) {
	$result = db_select('node', 'n')
		->condition('n.type', 'car')
		->condition('n.status', NODE_PUBLISHED)
		->condition('n.uid', $user->uid)
		->fields('n', array('nid'))
		->execute();
	$cars = array();
	foreach ($result as $item) {
		$cars[] = $item->nid;
	}

	return $cars;
}

function daway_user_get_review($user, $count = FALSE) {
	$account = user_load($user->uid);
	$data = array();
	$n = 0;
	if (isset($account->field_user_review['und'])) {
		foreach ($account->field_user_review['und'] as $review_id) {
			if ($count) {
				if ($n == $count) break;
			}
			$collection = entity_metadata_wrapper('field_collection_item', $review_id['value']);
			$data[] = array(
				'user' => $collection->field_fur_user->value(),
				'rate' => $collection->field_fur_rate->value(),
				'review' => $collection->field_fur_review->value(),
				'trip' => $collection->field_fur_trip->value(),
				'answer' => $collection->field_fur_answer->value(),
			);

			$n++;
		}
	}
	return $data;
}

function daway_user_get_security($user) {
	$account = user_load($user->uid);
	$data = array();
	$fields_info = field_info_instances('field_collection_item', 'field_user_security');
	if (isset($account->field_user_security['und'])) {
		$security_entity_id = $account->field_user_security['und'][0]['value'];
		$field_collection = entity_metadata_wrapper('field_collection_item', $security_entity_id);

		foreach ($fields_info as $field_name => $value) {
			$field_data = field_info_field($field_name);
			$index = $field_collection->{$field_name}->value();
			if ($index) {
				if ($index == 1) {
					$class = 'active';
				} else {
					$class = 'wait';
				}
			} else {
				$class = 'false';
			}
			$label = (isset($field_data['settings']['allowed_values'][$index])) ? $field_data['settings']['allowed_values'][$index] : 'N/A';

			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => $index,
				'class' => $class,
			);
		}
	} else {
		foreach ($fields_info as $field_name => $value) {
			$label =  'N/A';
			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => 0,
				'class' => 'false',
			);
		}
	}
	return $data;
}

function daway_user_get_habits($user) {
	if (is_numeric($user)) {
		$account = user_load($user);
	} elseif (is_object($user)) {
		$account = $user;
	}

	$data = array();
	$fields_info = field_info_instances('field_collection_item', 'field_user_habits');
	if (isset($account->field_user_habits['und'])) {
		$security_entity_id = $account->field_user_habits['und'][0]['value'];
		$field_collection = entity_metadata_wrapper('field_collection_item', $security_entity_id);

		foreach ($fields_info as $field_name => $value) {
			$field_data = field_info_field($field_name);
			$val = $field_collection->{$field_name}->value();

			$label = (isset($field_data['settings']['allowed_values'][$val])) ? $field_data['settings']['allowed_values'][$val] : 'N/A';
			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => t($label),
				'class' => 'type-'.$val,
			);
		}
	} else {
		foreach ($fields_info as $field_name => $value) {
			$label =  'N/A';
			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => t($label),
				'class' => 'false',
			);
		}
	}
	return $data;
}

function daway_user_get_percent_profile($user) {
	$fields_info = field_info_instances('user');
	$entity = entity_metadata_wrapper('user', $user);

	$count_field = count($fields_info['user']);
	$no_empty = 0;
	foreach ($fields_info['user'] as $field_name => $value) {
		if ($field_name == 'field_user_review') continue;
		$field_data = field_info_field($field_name);
		if ($field_data['type'] == 'field_collection') {
			$fields_info_fc = field_info_instances('field_collection_item', $field_name);
			$count_field += count($fields_info_fc);
			$count_field--;
			$val = $entity->{$field_name}->value();
			if ($val) {
				$entity_fc = entity_metadata_wrapper('field_collection_item', $val->item_id);
				foreach ($fields_info_fc as $field_name => $value) {
					$val_fc = $entity_fc->{$field_name}->value();
					if (!empty($val_fc)) $no_empty++;
				}
			}

		} else {
			$val = $entity->{$field_name}->value();
			if (!empty($val)) $no_empty++;
		}
	}

	return ceil(($no_empty*100)/$count_field);

}


function daway_user_get_by_geopoint($pid) {
	$query = db_select('users', 'u')->fields('u', array('uid'));
	$query->innerJoin('field_data_field_user_address', 'address', 'address.entity_id = u.uid');
	$query->addField('address', 'field_user_address_target_id', 'geopoint_id');
	$query->innerJoin('geopoint', 'g', 'g.pid = address.field_user_address_target_id');

	$query->innerJoin('geopoint_index', 'city_i', 'city_i.pid = g.pid');
	$query->condition('city_i.type', 'country');
	$query->innerJoin('geopoint', 'city_g', 'city_g.short = city_i.name');


	$db_or = db_or();
	$db_or->condition('city_i.pid', $pid);
	$query->condition($db_or);

	return $query->execute()->fetchCol();

}
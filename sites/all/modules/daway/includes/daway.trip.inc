<?php

function test_sort_abcde($a, $b) {
	if($a['time'] < $b['time'] ) {
		return -1;
	} else {
		return 1;
	}
}

function daway_trip_get_extra($node) {
	if (is_numeric($node)) {
		$node = user_load($node);
	} elseif (is_object($node)) {
		$node = $node;
	}

	$data = array();
	$fields_info = field_info_instances('field_collection_item', 'field_trip_extra');
	if (isset($node->field_trip_extra['und'])) {
		$node_entity_id = $node->field_trip_extra['und'][0]['value'];
		$field_collection = entity_metadata_wrapper('field_collection_item', $node_entity_id);

		foreach ($fields_info as $field_name => $value) {
			$field_data = field_info_field($field_name);
			$val = $field_collection->{$field_name}->value();
			$label = (isset($field_data['settings']['allowed_values'][$val])) ? $field_data['settings']['allowed_values'][$val] : 'N/A';
			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => t($label),
				'class' => 'type-'.$val,
			);
		}
	} else {
		foreach ($fields_info as $field_name => $value) {
			$label =  'N/A';
			$data[$field_name] = array(
				'label' => t($value['label']),
				'value' => t($label),
				'class' => 'false',
			);
		}
	}
	return $data;
}

function daway_trip_get_points($nid, $type = 'shema') {
	if (!is_numeric($nid)) {
		return FALSE;
	}
	$node = entity_metadata_wrapper('node', $nid);
	$data = $node->field_trip_shema->value();
	$data = array_values($data);
	if (!count($data)) return FALSE;
	$points = array();
	if ($type == 'shema') {
		$points['departure'] = $data[0];
		$points['arrival'] = $data[count($data)-1];
		unset($data[0]);
		unset($data[count($data)-1]);
		$points['points'] = $data;
	} else {
		$points = $data;
	}
	return $points;
}

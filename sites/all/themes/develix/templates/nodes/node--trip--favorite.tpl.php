<article
	id="node-<?php print $trip_global_date?>"
	class="<?php print $class ?>"
	data-price="<?php print $trip_global_price ?>"
	data-date="<?php print $trip_global_date ?>"
	data-seats="<?php print $trip_global_passenger ?>"
	data-display="inline"

	<div class="row">
		<div class="col left icon"><a href="#"></a></div>
		<div class="col left title"><?php print $trip_global_link ?></div>
		<div class="col left time"><span><?php print str_replace(',', ',<br>', format_date($trip_global_date, 'custom', 'H:j m.d.Y')) ?></span></div>
		<div class="col left places"><a class="count-<?php print $trip_global_passenger ?>" href="#"><?php print $trip_global_passenger ?></a></div>
		<div class="col left price"><span><?php print $trip_theme_price ?></span></div>
		<div class="col right bookmark"><?php  //print daway_bookmark_get_link_by_param($node)  ?></div>
	</div>
</article>
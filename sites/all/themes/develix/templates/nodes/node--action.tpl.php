<?php
if ($path = libraries_get_path('gmap')) {
  drupal_add_js($path . '/gmap3.min.js');
}

$pid = $node->field_action_geopoint['und'][0]['target_id'];
$geopoint = geopoint_load($pid);
?>
<?php if ($view_mode == 'full') { ?>

  <script>
    jQuery(document).ready(function ($) {
      var $tabs = $('#tabs');

      $('a', $tabs).click(function () {
        $active = $(this);
        $('.tab').removeClass('active');
        $('a', $tabs).removeClass('active');

        $active.addClass('active');
        $('.tab.' + $active.data('panel')).addClass('active');

        setTimeout(function () {
          $.event.trigger('resizeMapUpdate');
        }, 500)
        return false;
      });
    })
  </script>
  <article class="node-<?php print $node->nid; ?> node poster full">
    <div class="row">

      <div class="col left" id="poster-info">

        <ul id="tabs" class="row">
          <li><a href="#info" class="active first" data-panel="info"><?php print t('About this event') ?></a></li>
          <li><a href="#map" data-panel="map"><?php print t('Show on the map') ?></a></li>
        </ul>

        <div class="tab active info">
          <div class="row poster-field poster-what">
            <span class="poster-field-label"><?php print t('What') ?>:</span>
            <span class="poster-field-value"><?php print $node->title ?></span>
          </div>
          <div class="row poster-field poster-where">
            <span class="poster-field-label"><?php print t('Where') ?>:</span>
					<span class="poster-field-value">
						<?php print l(geopoint_name($geopoint->pid, 'full'), 'geopoint/' . $pid); ?>
					</span>
          </div>
          <div class="row poster-field poster-when">
            <span class="poster-field-label"><?php print t('When') ?>:</span>
            <span
              class="poster-field-value"><?php print format_date($node->field_action_date['und'][0]['value'], 'custom', 'd.m.Y H:i') ?></span>
          </div>

          <?php if (!empty($content['body'][0]['#markup'])) { ?>
            <div class="row poster-field poster-desc">
              <span class="poster-field-label"><?php print t('Info') ?>:</span>
              <span class="poster-field-value"><?php print render($content['body']) ?></span>
            </div>
          <?php } ?>

          <div class="row poster-field poster-trip">
            <span class="poster-field-label"><?php print t('Rides around this event') ?>:</span>
            <?php

            $items = daway_trip_get_trip_by_point($geopoint, $geopoint, 10, 'arrival', 5, 5, 'viewsmall', $node->field_action_date['und'][0]['value']);
            if ($items) {
              print theme('daway_trip_list_views', array(
                'items' => $items,
                'tabs' => FALSE,
                'header' => FALSE,
                'view_mode' => 'inline'
              ));
            } else {
              print daway_poster_js_sticky_link($node);
            }


            ?>
          </div>

        </div>


        <!--Tab trip route map-->
        <div class="tab map">
          <?php print theme('daway_map', array('point' => $geopoint)); ?>
        </div>

      </div>

      <div class="col right region-right" id="poster-right">
        <div class="poster-preview"><?php print render($content['field_action_preview']); ?></div>
        <?php
        $block = block_load('daway_poster', 'daway_poster_relative');
        $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
        print $output;
        ?>
      </div>
    </div>

  </article>


<?php }
elseif ($view_mode == 'viewsmall') { ?>
  <article class="node-<?php print $node->nid; ?> node poster viewsmall">
    <div class="poster-preview"><?php print render($content['field_action_preview']) ?></div>
    <h2 class="poster-title"><?php print l($node->title, 'node/' . $node->nid) ?></h2>
  </article>

<?php }
else { ?>
  <?php
  $trips = daway_trip_get_trip_by_point($geopoint, $geopoint, 10, 'arrival', 3, 3, 'promo', $node->field_action_date['und'][0]['value']);
  ?>
  <article
    class="node-<?php print $node->nid; ?> node poster teaser <?php ($trips) ? print 'withtrip' : '' ?> <?php ($node->sticky) ? print 'sticky' : '' ?>">
    <?php if ($node->sticky) { ?>
      <div class="row poster-sticky">
        <div class="col left">Топ-Афиша</div>
        <div class="col right"><a href="#">Заказать Топ-Афишу</a></div>
      </div>
    <?php } ?>
    <div class="row">
      <div class="col left poster-preview"><?php print render($content['field_action_preview']); ?></div>
      <div class="col left poster-desc">
        <h2 class="title"><?php print l($node->title, 'node/' . $node->nid) ?></h2>

        <div
          class="date"><?php print format_date($node->field_action_date['und'][0]['value'], 'custom', 'l H:i, d.m.Y') ?></div>
        <div class="address">
          <?php
          $points_name = geopoint_name($node->field_action_geopoint['und'][0]['target_id'], 'full');
          print l($points_name, 'geopoint/' . $node->field_action_geopoint['und'][0]['target_id']);
          ?>
        </div>
        <div class="desc">

          <div class="body"><?php print text_summary($node->body['und'][0]['value'] . '...', 'full_html', 180); ?></div>
          <?php print l(t('more'), 'node/' . $node->title, array('attributes' => array('class' => array('more')))) ?>
        </div>
      </div>
      <div class="col left poster-trips">
        <?php if ($trips) { ?>
          <span class="title"><?php print t('Go here') ?></span>
          <ul>
            <?php foreach ($trips as $trip) { ?>
              <li>
                <?php print $trip ?>
              </li>
            <?php } ?>
          </ul>
          <a href="<?php print url('geopoint/' . $geopoint->pid) ?>" class="more"><?php print t('Show all trips') ?></a>
        <?php }
        else { ?>
          <?php print daway_poster_js_sticky_link($node) ?>
        <?php } ?>
      </div>
    </div>
  </article>
<?php } ?>

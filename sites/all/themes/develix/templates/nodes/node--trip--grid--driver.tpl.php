<article
	id="node-<?php print $trip_global_date?>"
	class="<?php print $class ?>"
	data-price="<?php print $trip_global_price ?>"
	data-date="<?php print $trip_global_date ?>"
	data-seats="<?php print $trip_global_passenger ?>"
	data-display="grid"
	data-distance="<?php print $trip_global_distance ?>">

	<h3 class="trip-title"><?php print $trip_global_link; ?></h3>
	<div class="trip-price"><?php print $trip_theme_price ?></div>
	<div class="row">
		<div class="col left user-info"><?php print $trip_theme_driver; ?></div>
		<ul class="col right trip-info">
			<li class="trip-route row">
				<a class="trip-route-show" href="#"><?php print t('Show the route') ?></a>
				<div class="trip-route-items"><?php print $trip_theme_shema ?></div>
			</li>
			<li class="trip-date row">
				<span class="label col left"><?php print t('Drive on').' '.strtolower(format_date($trip_global_date, 'custom', 'l')) ?></span>
				<span class="value col right"><?php print format_date($trip_global_date, 'custom', 'd.m.Y') ?></span>
			</li>
			<li class="trip-time row">
				<span class="label col left"><?php print t('Depart at') ?></span>
				<span class="value col right"><?php print format_date($trip_global_date, 'custom', 'H:i'); print t('&plusmn; 15 min.') ?></span>
			</li>
			<li class="trip-time row">
				<?php print $trip_global_body ?>
			</li>
			<li class="trip-comfort row">
				<span class="label col left"><?php print t('Car comfort') ?></span>
				<span class="value col right"><?php print theme('daway_rate', array('average' => rand(1, 5),'type' => 'point')) ?></span>
			</li>
			<li class="trip-seats row">
				<span class="label col left"><?php print t('Free seats') ?></span>
				<span class="value col right"><?php print $trip_global_passenger ?> <?php print t('seats') ?></span>
			</li>
		</ul>
	</div>
	<div class="row trip-extra">
		<div class="col left contact"><?php print daway_contact_link($node->uid, $node->nid) ?></div>
		<div class="col right"><?php print $trip_global_extra ?></div>
	</div>


</article>
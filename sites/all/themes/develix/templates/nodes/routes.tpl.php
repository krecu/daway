<?php
$q = 'routes/' . arg(1);
$get_items = $_GET;
unset($get_items['q']);

$query = array();

$drivers_link = l(t('Drivers'), $q . '/drivers', array('query' => $get_items));
$passengers_link = l(t('Passengers'), $q . '/passengers', array('query' => $get_items));
$default_link = l(t('All trip'), $q, array('query' => $get_items));


$subtype = "";
if (arg(2)) {
  if (arg(2) == 'drivers') {
    $subtype = 'drivers';
  }
  if (arg(2) == 'passengers') {
    $subtype = 'passengers';
  }
}

$gender = "";
if (arg(3)) {
  if (arg(3) == 'man') {
    $gender = 'man';
  }
  if (arg(3) == 'woman') {
    $gender = 'woman';
  }
}

$gender_man_drivers_link = l(t('Man'), $q . '/drivers/man', array('query' => $get_items));
$gender_man_passengers_link = l(t('Man'), $q . '/passengers/man', array('query' => $get_items));
$gender_women_drivers_link = l(t('Man'), $q . '/drivers/woman', array('query' => $get_items));
$gender_women_passengers_link = l(t('Man'), $q . '/passengers/woman', array('query' => $get_items));


if ($display == 'viewsmall') {
  $display = 'inline';
}
elseif ($display == 'viewfull') {
  $display = 'grid';
}
elseif ($display == 'rss') {
  $display = 'rss';
}

?>

<div class="<?php print $classes; ?>">
  <div
    id="trips-container"
    data-display="<?php print $display ?>"
    data-type="routes"
    data-subtype="<?php print $subtype ?>"
    data-gender="<?php print $gender ?>"
    data-departure="<?php print $pid1->pid ?>"
    data-departure-radius="<?php print $r1 ?>"
    data-arrival="<?php print $pid2->pid ?>"
    data-arrival-radius="<?php print $r2 ?>"
    data-date="<?php print $date ?>"
    data-dateplus="<?php print $dateend ?>"
    data-price="<?php print $price ?>"
    data-seats="<?php print $seats ?>"
    data-extra="<?php print $extra ?>"
    data-gender=""
    data-baggage="<?php print $baggage ?>"
    data-cartype=""
    data-sortby="date"
    data-sortorder="ASC"
    >
    <ul id="trips-tab" class="row">
      <li class="first"><?php print $drivers_link ?></li>
      <li><?php print $passengers_link ?></li>
      <li class="last"><?php print $default_link ?></li>
    </ul>
    <div class="view-trips">
      <div class="row view-header">
        <div class="col left">
          <div id="trips-sorting" class="form-item sort">
            <label><?php print t('Sort by') ?></label>
            <select>
              <option value="date"><?php print t('Date') ?></option>
              <option value="price"><?php print t('Price') ?></option>
              <option value="rating"><?php print t('Rating') ?></option>
              <option value="seats"><?php print t('Seats') ?></option>
              <option value="distance"><?php print t('Distance from me') ?></option>
            </select>
            <input name="sorting" type="checkbox" value="ASC">
          </div>
        </div>
        <div class="col right">
          <ul id="trips-display" class="row">
            <li class="first"><a class="grid <?php print ($display == 'grid') ? 'active' : '' ?>"
                                 data-tripdisplay="grid" href="?display=grid"><span class="icon"></span>grid</a></li>
            <li><a class="inline <?php print ($display == 'inline') ? 'active' : '' ?>" data-tripdisplay="inline"
                   href="?display=inline"><span class="icon"></span>inline</a></li>
            <li class="last"><a data-tripdisplay="map" class="map <?php print ($display == 'map') ? 'active' : '' ?>"
                                href="#"><span class="icon"></span>map</a></li>
          </ul>
        </div>
      </div>

      <?php $temp_dgf_sub_form = drupal_get_form('daway_subscribe_form', 'routes', $routes);
      print render($temp_dgf_sub_form) ?>

      <div id="trips-list">
        <div class="view-content">
          <?php
          if (count($trips)) {
            print implode('', $trips);
          }
          ?>
        </div>
      </div>

      <div id="paginator-container">
        <?php print theme('pager') ?>
      </div>
    </div>
  </div>

</div>
<?php global $user; ?>
<?php if ($view_mode == 'full') { ?>


  <article class="node-<?php print $node->nid; ?> node full trip">
    <div class="row">

      <div class="col left" id="trip-info">

        <!--Defined tabs-->
        <ul id="tabs" class="row">
          <li><a href="#info" class="active first" data-panel="info"><?php print t('Trip information') ?></a></li>
          <li><a href="#route" data-panel="route"><?php print t('Route') ?></a></li>
        </ul>

        <!--Tab default info about trip-->
        <div class="tab active info">

          <!--Display trip routes shema-->
          <div class="row trip-field trip-route"><?php print $trip_route ?></div>

          <!--Display trip extra options-->
          <div class="row trip-field trip-options">
            <span class="trip-field-label"><?php print t('More') ?>:</span>
            <span class="trip-field-value"><?php print $trip_extra ?></span>
          </div>

          <!--Display trip body-->
          <?php if (!empty($content['body'][0]['#markup'])) { ?>
            <div class="row trip-field trip-desc">
              <span class="trip-field-label"><?php print t('Info') ?>:</span>
              <span class="trip-field-value"><?php print render($content['body']) ?></span>
            </div>
          <?php } ?>

          <!--Display trip nav-->
          <div class="row trip-field trip-nav">
            <ul>
              <li class="prev"><a href="#"><?php print t('Prev<br> trip') ?></a></li>
              <li class="search"><?php print $trip_search_link ?></li>
              <li class="next"><a href="#"><?php print t('Next<br> trip') ?></a></li>
            </ul>
          </div>
          <div class="row trip-field trip-nav-clear"></div>


          <!--Display trip car-->
          <?php if ($trip_car) { ?>
            <div class="row trip-field trip-cars">
              <?php print $trip_car ?>
            </div>
          <?php } ?>

          <!--Display trip contact list-->
          <?php if ($trip_contact_user) { ?>
            <div class="row trip-field trip-contact-users"><?php print $trip_contact_user; ?></div>
          <?php } ?>

          <div class="row trip-field trip-reviews">
            <?php $dgf_tmp = drupal_get_form('daway_trip_reviews_list_form', $node->uid, $node->nid);
            print drupal_render($dgf_tmp) ?>
          </div>


        </div>


        <!--Tab trip route map-->
        <div class="tab route">
          <?php
          // дистанция доступна по умолчанию
          $distance = (int) ($trip_route_info['distance'] / 1000);
          $seconds = $trip_route_info['duration'];
          $days = floor($seconds / 86400);
          $seconds = $seconds - ($days * 86400);
          $hours = floor($seconds / 3600);
          $seconds = $seconds - ($hours * 3600);
          $minutes = floor($seconds / 60);

          ?>
          <ul class="route-route-info">
            <li class="distance"><?php print t('Distance') ?>: <?php print $distance ?> <?php print t('km') ?></li>
            <li class="duration"><?php print t('Min. travel time') ?>
              : <?php print ($days) ? $days . t('days ') : '' ?><?php print $hours . t('hour') ?> <?php print $minutes . t('min') ?> </li>
          </ul>
          <div class="route-route-map"><?php print $trip_map ?></div>

        </div>

      </div>

      <div class="col right" id="trip-driver">
        <?php

        if (isset($node->field_trip_passenger['und'])) {
          $passenger = $node->field_trip_passenger['und'][0]['value'];
        }
        else {
          $passenger = 1;
        }

        ?>
        <ul class="trip-driver-summ">
          <li class="first">
            <span class="value"><?php print $passenger ?> <span class="suffix"><?php print t('seats') ?></span></span>
            <span class="desc"><?php print t('still free') ?></span>
          </li>
          <li class="separator"></li>
          <li class="last">
            <span
              class="value"><?php //print theme('daway_language_currency_field', array('value' => $route_default['price'])) ?></span>
            <span class="desc"><?php print t('for one place') ?></span>
          </li>
        </ul>
        <div class="trip-driver-contact">
          <span class="desc"><?php print t('To contact click on the button below') ?></span>
          <?php print daway_contact_link($node->uid, $node->nid) ?>
        </div>

        <?php
        if (isset($node->uid)) {
          $driver = user_load($node->uid);

          $user_view_tmp_1 = user_view($driver, 'trip');
          print render($user_view_tmp_1);
        }
        ?>
      </div>

    </div>

  </article>

<?php }
elseif ($view_mode == 'admin') { ?>
  <?php
  $class = array();
  if (isset($node->field_trip_type['und'])) {
    $class[] = 'type-' . $node->field_trip_type['und'][0]['value'];
  }

  $routes_items = daway_trip_get_routes_tree_by_node($node);
  $route_default = $routes_items['parent'];

  $price = $routes_items['parent']['price'];
  $date = format_date(time(), 'custom', 'H:i,d.m.Y');
  $seats = 0;
  if (isset($node->field_trip_passenger['und'])) {
    $seats = $node->field_trip_passenger['und'][0]['value'];
  }

  if ($node->status == 0) {
    $class[] = 'unpublic';
  }

  $is_autor = ($user->uid == $node->uid) ? TRUE : FALSE;
  ?>
  <article id="node-<?php print $node->nid; ?>"
           class="node-<?php print $node->nid; ?> inline node viewsmall trip admin <?php print implode(' ', $class) ?>">
    <div class="row">
      <div class="col left icon"><a href="#"></a></div>
      <div class="col left title">
        <?php
        print l(geopoint_name($routes_items['parent']['route']->pid1, 'short') . ' →<br>' . geopoint_name($routes_items['parent']['route']->pid2, 'short'), 'node/' . $node->nid, array('html' => TRUE));
        ?>
      </div>
      <div class="col left time"><span><?php print str_replace(',', ',<br>', $date) ?></span></div>
      <div class="col left places"><a class="count-<?php print $seats ?>" href="#"><?php print $seats ?></a></div>
      <div class="col left price">
        <span><?php print theme('daway_language_currency_field', array('value' => $price)) ?></span></div>
      <div class="col left habits">
        <ul>
          <?php $habits = daway_user_get_habits($node->uid) ?>
          <?php foreach ($habits as $key => $value) { ?>
            <li class="<?php print $key . ' ' . $value['class'] ?>"><a
                href="#"><?php print $value['label'] . ' ' . $value['value'] ?></a></li>
          <?php } ?>
        </ul>
      </div>

      <?php if ($is_autor) { ?>
        <ul class="edit col right row">
          <li><?php print daway_trip_js_op_link('reverse', t('Post a return trip'), $node->nid) ?></li>
          <li><?php print daway_trip_js_op_link('users', 'User', $node->nid) ?></li>
          <li><a class="edit" data-tooltip="<?php print t('Edit') ?>" target="_blank"
                 href="<?php print url('user/trips/' . $node->nid . '/edit') ?>"><?php print t('Edit') ?></a></li>
          <li><?php print daway_trip_js_op_link('duplicate', 'Duplicate', $node->nid) ?></li>
          <?php if ($node->status == NODE_NOT_PUBLISHED) { ?>
            <li><?php print daway_trip_js_op_link('publish', 'Publish', $node->nid) ?></li>
          <?php }
          else { ?>
            <li><?php print daway_trip_js_op_link('unpublish', 'Un publish', $node->nid) ?></li>
          <?php } ?>
        </ul>
      <?php } ?>

    </div>
  </article>
<?php }
elseif ($view_mode == 'promo') { ?>
  <?php
  $class = array();
  if (isset($node->field_trip_type['und'])) {
    $class[] = $node->field_trip_type['und'][0]['value'];
  }

  $routes_field_collection = $node->field_trip_routes['und'];
  $routes_items = array();
  foreach ($routes_field_collection as $item) {
    $field_collection_item = entity_metadata_wrapper('field_collection_item', $item['value']);
    $route_tmp = $field_collection_item->field_ftr_route->value();
    if (empty($route_tmp)) {
      continue;
    }
    $routes_items[$route_tmp->rid] = array(
      'route' => $route_tmp,
      'price' => $field_collection_item->field_ftr_price->value(),
      'type' => $field_collection_item->field_ftr_type->value(),
    );
  }

  $array_values_tmp = array_values($routes_items);
  $route_default = array_shift($array_values_tmp);
  foreach ($routes_items as $item) {
    if ($item['type'] == 'parent') {
      $route_default = $item;
      break;
    }
  }
  ?>
  <article id="node-<?php print $node->nid; ?>" class="node-<?php print $node->nid; ?> inline node promo trip">
    <div class="row">
      <div class="col left user">
        <?php

        $driver = user_load($node->uid);
        print daway_profile_get_avatar($driver, TRUE);
        //				$user_view_tmp = user_view($driver, 'modal');
        //				print render($user_view_tmp);
        ?>
      </div>
      <div class="col left info">
        <div
          class="title"><?php print l(geopoint_name($route_default['route']->pid1, 'short') . ' →<br>' . geopoint_name($route_default['route']->pid2, 'short'), 'node/' . $node->nid, array('html' => TRUE)); ?></div>
        <div class="price">
          <span><?php print theme('daway_language_currency_field', array('value' => $route_default['price'])) ?></span>
        </div>
      </div>
    </div>
  </article>
<?php }
elseif ($view_mode == 'favorite') { ?>
  <?php
  $class = array();
  if (isset($node->field_trip_type['und'])) {
    $class[] = $node->field_trip_type['und'][0]['tid'];
  }

  $routes_items = daway_trip_get_routes_tree_by_node($node);
  $route_default = $routes_items['parent'];

  $price = $routes_items['parent']['price'];
  $date = format_date(time(), 'custom', 'H:i,d.m.Y');
  $seats = 0;
  if (isset($node->field_trip_passenger['und'])) {
    $seats = $node->field_trip_passenger['und'][0]['value'];
  }

  $is_autor = ($user->uid == $node->uid) ? TRUE : FALSE;
  ?>
  <article id="node-<?php print $node->nid; ?>"
           class="node-<?php print $node->nid; ?> inline node viewsmall trip favorite">
    <div class="row">
      <div class="col left icon"><a href="#"></a></div>
      <div
        class="col left title"><?php print l(routes_get_normalise_name($routes_items['parent']['route'], ' →<br>'), 'node/' . $node->nid, array('html' => TRUE)) ?></div>
      <div class="col left time"><span><?php print str_replace(',', ',<br>', $date) ?></span></div>
      <div class="col left places"><a class="count-<?php print $seats ?>" href="#"><?php print $seats ?></a></div>
      <div class="col left price">
        <span><?php print theme('daway_language_currency_field', array('value' => $price)) ?></span></div>
      <div class="col left habits">
        <ul>
          <?php $habits = daway_user_get_habits($node->uid) ?>
          <?php foreach ($habits as $key => $value) { ?>
            <li class="<?php print $key . ' ' . $value['class'] ?>"><a
                href="#"><?php print $value['label'] . ' ' . $value['value'] ?></a></li>
          <?php } ?>
        </ul>
      </div>

      <?php if ($is_autor) { ?>
        <ul class="edit col right row">
          <li><a class="delete" data-tooltip="<?php print t('Delete') ?>" target="_blank"
                 href="<?php print url('node/' . $node->nid) ?>"><?php print t('Delete') ?></a></li>
        </ul>
      <?php } ?>

    </div>
  </article>
<?php }
elseif ($view_mode == 'contact') { ?>

  <article
    id="node-<?php print $node->nid; ?>"
    class="<?php print $class ?>">
    <div class="row">
      <div class="col left icon"><a href="#"></a></div>
      <div class="col left user"><?php print $trip_theme_user ?></div>
      <div class="col left time"><span><?php ?></span></div>
      <div class="col left places">1</div>
      <div class="col left price">
        <span><?php print theme('daway_language_currency_field', array('value' => 10)) ?></span></div>
    </div>
  </article>

<?php
}


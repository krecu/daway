<?php

unset($page['content']['system_main']['default_message']);
$tabs = render($tabs);
$right = render($page['right']);
$top = render($page['content_top']);
$bottom = render($page['content_bottom']);
$slider = render($page['slider']);
$class = array();
$class[] = '';
if ($right) {
  $class[] = 'sideRight';
}
if ($top) {
  $class[] = 'sideTop';
}
if ($bottom) {
  $class[] = 'sideBottom';
}
if ($slider) {
  $class[] = 'slider';
}
if ($tabs) {
  $class[] = 'tabs';
}


if (!empty($_GET['devel'])) {

//  $cars = carfield_get_manufactures_by_type(27847);
//  dsm($cars);
//  $years = carfield_get_years_by_model(552, 27847);
//  dsm($years);
//  $cars = carfield_get_models_by_year(552, 2012, 27847);
//  dsm($cars);

//      $cars = carfield_get_cars_by_params(13373);
//      dsm($cars);
}

$title_class = array();
if ((arg(0) == "node") && (is_numeric(arg(1)))) {
  $node = $variables['node'];
  if ($node->type == 'trip') {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $type = $node_wrapper->field_trip_type->value();
    $title_class[] = $type;
  }
}

?>
<?php if (drupal_is_front_page()) : ?><span id="site-desc">
  <h1><?php print t('Search for ride sharе or trip passеnger') ?></h1></span><?php endif; ?>
<div id="wrapper">
  <header id="header">
    <div id="lite-loader">
      <div class="peg"></div>
    </div>
    <div class="wrapper">
      <div class="row">
        <div class="col left" id="logo">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/><span
              class="site-slogan"><?php print t($site_slogan) ?></span>
          </a>

          <div id="like">
            <script type="text/javascript">(function () {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso == undefined) {
                  window.ifpluso = 1;
                  var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                  s.type = 'text/javascript';
                  s.charset = 'UTF-8';
                  s.async = true;
                  s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                  var h = d[g]('body')[0];
                  h.appendChild(s);
                }
              })();</script>
            <div class="pluso" data-background="none;"
                 data-options="small,square,line,horizontal,counter,sepcounter=1,theme=14"
                 data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,linkedin,livejournal"></div>
          </div>


        </div>
        <div class="col right" id="user-menu"><?php print render($page['user_menu']) ?></div>
        <div class="col left" id="main-menu"><?php print render($page['main_menu']) ?></div>
      </div>
    </div>
  </header>

  <section id="middle">

    <div id="container">
      <div id="content" class="<?php print implode(' ', $class) ?>">
        <div id="background"><?php print $slider; ?></div>
        <div class="wrapper">

          <?php print $top; ?>
          <div id="node" class="row">

            <div class="node-content <?php print implode(' ', $class) ?>">
              <?php unset($page['content']['system_main']['default_message']); ?>
              <?php print render($page['node_top']); ?>
              <?php
              if ($title && (!drupal_is_front_page())) {
                ?><h1 id="page-title"
                      class="<?php print implode(' ', $title_class) ?>"><?php print $title ?></h1><?php } ?>
              <?php

              if (arg(0) == 'geopoint') {
                $geopoint = geopoint_load(arg(1));
                $shema = array();
                $i = 1;
                $j = 0;
                foreach ($geopoint->shema as $type => $pid) {

                  if (($type != 'country') &&
                    ($type != 'administrative_area_level_1') &&
                    ($type != 'sublocality') &&
                    ($type != 'street_number') &&
                    ($type != 'route') &&
                    ($type != 'locality')
                  ) {
                    $j++;
                    continue;
                  }


                  $shema[] = l(geopoint_name($pid, 'short'), 'geopoint/' . $pid);
                  if ($i < count($geopoint->shema) - $j) {
                    $shema[] = '<span class="arrow">→</span>';
                  }
                  $i++;
                }
                print '<div id="breadcrumbs" class="geopoint row">' . theme('item_list', array('items' => $shema)) . '</div>';
              }
              ?>
              <?php print theme('daway_content_action_links') ?>

              <?php print render($page['content']); ?>
              <?php print render($page['front_promo']) ?>
              <?php print render($page['front_promo_info']) ?>
              <?php print render($page['node_bottom']); ?>


            </div>
            <?php
            if ($right) {
              print '<div class="node-sideRight">' . $right . '</div>';
            }
            ?>

          </div>

        </div>
        <?php print render($page['front_popular_routes']) ?>
        <?php print $bottom; ?>

      </div>
    </div>
</div>
</section>

</div>


<footer id="footer">
  <div class="wrapper">
    <div class="row">
      <div class="col left" id="footer-menu"><?php print render($page['footer_menu']) ?></div>
      <div class="col right" id="social-menu"><?php print render($page['social_menu']) ?></div>
    </div>
  </div>
</footer>

<div id="full-loader">
  <h2><?php print t('Loading') ?>... <a href="#"><?php print t('Close') ?></a></h2>

  <div class="peg"></div>
</div>

<div id="messages">
  <?php if ($messages) : ?><?php print $messages; ?><?php endif; ?>
</div>
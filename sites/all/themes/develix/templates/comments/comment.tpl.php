<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<div class="row">
		<div class="col left"><div class="avatar"></div></div>
		<div class="col right">
			<div class="comment-data">
				<div class="name"><strong><?php print $comment->name ?></strong>, <span class="created"><?php print format_date($comment->created, 'custom', 'd.m.Y h:m' ) ?></span></div>
				<div class="body">
					<?php print render($content['comment_body']); ?>
					<a class="reply" href="#">Цитировать</a>
				</div>
			</div>
		</div>
	</div>
</article>

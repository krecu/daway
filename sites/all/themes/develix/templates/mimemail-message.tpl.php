<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
  <?php endif; ?>
</head>

<body style="margin: 0; padding: 0;" id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td>
      <table align="center" border="0" cellpadding="0" cellspacing="0" width="801">
        <tr>
          <td>
            <table border="0" cellpadding="0" cellspacing="0" width="801">
              <tr>
                <td height="42" width="15">
                  <a href="http://www.daway.com">
                    <img src="http://daway.com/sites/all/themes/develix/images/mail/bg-hl-mail.jpg" alt=""/>
                  </a>
                </td>
                <td height="42">
                  <a href="http://www.daway.com">
                    <img src="http://daway.com/sites/default/files/logo_0.png" alt=""/>
                  </a>
                </td>
                <td height="42" >
                  <img src="http://daway.com/sites/all/themes/develix/images/mail/bg-h-mail.jpg" alt=""/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td bgcolor="#ecfafa" align="center" style="padding: 15px 15px 0 15px">
            <table border="0" cellpadding="0" cellspacing="0" width="771" >
              <tr>
                <td height="500" width="86" valign="top" bgcolor="white">
                  <h2><?php print $subject ?></h2>
                  <?php print $body ?>
                </td>
              </tr>
            </table>

          </td>
        </tr>

        <tr>
          <td width="71">
            <img src="http://daway.com/sites/all/themes/develix/images/mail/bg-f-mail.jpg" alt=""/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>

</html>

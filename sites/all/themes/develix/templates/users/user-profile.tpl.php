<?php

//	$pids = db_select('geopoint', 'g')->fields('g', array('pid'))->execute();
//
//	foreach ($pids as $pid) {
//		$radius = geopoint_get_radius($pid->pid);
//		geopoint_save_radius($pid->pid, $radius);
//	}


$account = $variables['elements']['#account'];
global $user;


$edit_class = '';
if (!user_is_anonymous() && ($account->uid == $user->uid)) {
  $edit_class = 'edit';
  $edit_profile = daway_profile_js_popup_link($account, 'personal');
  $edit_habits = daway_profile_js_popup_link($account, 'habits');
}
?>
<div class="profile">
  <div id="percent-profile">
    <?php $percent_profile = daway_user_get_percent_profile($account); ?>
    <span class="title"><?php print t('Profile completion') ?> <?php print $percent_profile ?>%</span>
    <span class="percent"><span style="width: <?php print $percent_profile ?>%"></span></span>
  </div>
  <div class="row">
    <div class="col left" id="profile-info">
      <div class="row">
        <div class="col left"><?php print daway_profile_get_avatar($account) ?></div>
        <ul class="col left info">
          <li class="info-item row">
            <div class="personal <?php print $edit_class ?>">
              <?php print $edit_profile; ?>
              <div class="item row">
                <?php print daway_profile_get_gender($account) ?>
                <?php print daway_profile_get_age($account) ?>
              </div>
            </div>
          </li>
          <li class="info-item row">
            <ul class="friends">
              <li class="facebook" data-tooltip="Facebook"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Facebook') ?></li>
              <li class="odnoklassniki" data-tooltip="Odnoklassniki"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Odnoklassniki') ?></li>
              <li class="livejournal" data-tooltip="Livejournal"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Livejournal') ?></li>
              <li class="vk" data-tooltip="Vkontakte"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Vkontakte') ?></li>
              <li class="twitter" data-tooltip="Twitter"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Twitter') ?></li>
              <li class="yandex" data-tooltip="Yandex"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Yandex') ?></li>
              <li class="google" data-tooltip="Google"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Google') ?></li>
              <li class="mailru" data-tooltip="Mailru"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Mailru') ?></li>
              <li class="linkedin" data-tooltip="Linkedin"><span
                  class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Linkedin') ?></li>
            </ul>
          </li>
        </ul>

      </div>


      <div class="row"><?php print daway_profile_get_about($account) ?></div>
    </div>
    <div class="col left" id="profile-security">
      <ul class="info">
        <?php $security = daway_user_get_security($account); ?>
        <li class="title"><?php print t('Trust level') ?>:</li>
        <?php foreach ($security as $key => $value) { ?>
          <li class="<?php print $key ?>">
            <?php
            if ($account->uid != $user->uid) {
              print '<a href="#"  class="' . $value['class'] . '">' . $value['label'] . '</a>';
            } else {
              if ($value['value'] == 1) {
                print '<a  class="' . $value['class'] . '">' . $value['label'] . '</a>';
              }
              else {
                if ($value['value'] == -1) {
                  print '<a  class="' . $value['class'] . '">' . $value['label'] . '</a>';
                }
                else {
                  print daway_profile_js_popup_link($account, $key, $value['class'], $value['label']);
                }
              }
            }
            ?>
          </li>
        <?php } ?>
      </ul>
    </div>

  </div>

  <?php


  //	$cars_tree = taxonomy_get_tree(10);
  //	dsm($cars_tree);
  $cars = daway_user_get_cars($account);
  global $user;
  if ($user->uid != arg(1)) {
    print theme('daway_user_cars', array('cars' => $cars, 'view_mode' => 'profile'));
  }
  else {
    print theme('daway_user_cars', array('cars' => $cars, 'view_mode' => 'admin'));
  }
  ?>

  <?php $reviews = daway_trip_get_review($account->uid) ?>
  <?php
  if ($path = libraries_get_path('scrollbar')) {
    drupal_add_js($path . '/jquery.mCustomScrollbar.js');
    drupal_add_css($path . '/jquery.mCustomScrollbar.css');
  }
  ?>
  <script>
    jQuery(document).ready(function ($) {
      $("#profile-rating ul.reviews").mCustomScrollbar({
        theme: "light",
        mouseWheelPixels: 50,
        scrollInertia: 500
      });
    })
  </script>
  <?php if (count($reviews)) { ?>
  <div id="profile-rating">
    <div class="title">Reviews:</div>
    <ul class="reviews">
      <?php foreach ($reviews as $review) { ?>
        <li class="review row">
          <div class="review-item col left user"><?php print daway_profile_get_avatar($account, TRUE) ?></div>
          <div class="review-item col left info">
            <div class="trip"><?php print l($review['trip']->title, 'node/' . $review['trip']->nid) ?></div>
            <div class="body"><?php print $review['body']['value'] ?></div>
            <div class="rate" style="display: none;"><?php print $review['rate'] ?></div>
            <?php if (FALSE) { ?>
              <div class="answer row">
                <div class="col left user"><?php print render(user_view($account, 'modal')) ?></div>
                <div class="col left answer-body"><?php print $review['answer'] ?></div>
              </div>
            <?php } ?>
          </div>
        </li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>
<div class="row">
  <br>
  <br>

</div>
</div>

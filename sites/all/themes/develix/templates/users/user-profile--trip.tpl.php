<script>
  jQuery(document).ready(function ($) {
    $('a', '#profile-tabs').click(function () {
      $('.profile-tab', '.profile.tripfull').removeClass('active');
      $('.profile-tab[data-panel="' + $(this).data('panel') + '"]', '.profile.tripfull').addClass('active');
      $('a', '#profile-tabs').removeClass('active');
      $(this).addClass('active');
    })
  })
</script>

<?php
//dsm($variables['elements']['#account']);
$account = user_load(1);
$account = $variables['elements']['#account'];
?>
<div class="profile tripfull">
  <div id="profile-info">
    <div class="row">
      <div class="col left">
        <?php print daway_profile_get_avatar($account); ?>
      </div>
      <ul class="col left info">
        <li class="info-item">
          <div class="personal">
            <div class="item row"><?php print daway_profile_get_name($account) ?></div>
            <div class="item row">
              <?php print daway_profile_get_gender($account) ?>
              <?php print daway_profile_get_age($account) ?>
            </div>
          </div>
        </li>
        <li class="info-item">
          <ul class="friends">
            <li class="facebook" data-tooltip="Facebook"><span
                class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Facebook') ?></li>
            <li class="odnoklassniki" data-tooltip="Odnoklassniki"><span
                class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Odnoklassniki') ?></li>
            <li class="vk" data-tooltip="Vkontakte"><span
                class="icon"></span><?php print daway_user_get_social_link($account->uid, 'Vkontakte') ?></li>
          </ul>
        </li>
      </ul>
    </div>
    <?php //print theme('daway_extra_list', array('items' => daway_user_get_habits($account))); ?>
  </div>

  <ul id="profile-tabs" class="row">
    <li><a class="active first" data-panel="security" href="#security"><?php print t('Trust') ?></a></li>
    <li><a class="last" data-panel="rating" href="#rating"><?php print t('Rating') ?></a></li>
  </ul>

  <div id="profile-security" data-panel="security" class="profile-tab active">
    <ul class="info">
      <?php $security = daway_user_get_security($account) ?>
      <?php foreach ($security as $key => $value) { ?>
        <li class="<?php print $key ?>"><a class="<?php print $value['class'] ?>"
                                           href="#"><?php print $value['label'] ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <div id="profile-rate-list" data-panel="rating" class="profile-tab">
    <?php $reviews = daway_trip_get_review($account->uid, FALSE, 4) ?>
    <?php if (count($reviews)) { ?>
    <ul>
      <?php foreach ($reviews as $review) { ?>
        <li class="row">
          <div class="col left user">
            <?php print daway_profile_get_avatar($account, TRUE) ?>
            <span class="icon <?php print ($review['rate'] > 0) ? 'plus' : 'minus' ?>"></span>
          </div>
          <div class="col left info">
            <div class="body"><?php print text_summary($review['body']['value'], NULL, 50) . '...' ?></div>
          </div>
        </li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
</div>
</div>

<?php
	$account = $variables['elements']['#account'];

	$rate = daway_profile_get_rate($account->uid);
	$max = daway_profile_get_maxrate();

	$rate = ($rate * 100)/$max;


?>
<div class="profile tripviewfull">
	<div class="row">
		<?php
		$thumbnail = theme('image_style', array(
			'style_name' => 'preview_94x94',
			'path' => $account->picture->uri,
			'alt' => $account->name,
		));
		?>
		<div class="avatar"><?php print $thumbnail ?></div>
		<ul class="info">
			<li class="name"><?php print l(format_username($account), 'user/'.$account->uid) ?></li>
			<li class="age"><?php print daway_user_get_age($account) ?></li>
			<li class="contact">

			</li>
		</ul>
	</div>

	<ul class="info row">
		<li class="user-rating row">
			<span class="label col left"><?php print t('Rating') ?>:</span>
			<span class="value col left"><?php print theme('daway_rate', array('average' => $rate)) ?></span></li>
		<li class="user-confidence row">
			<span class="label col left"><?php print t('Trust') ?>:</span>
			<span class="value col left">
				<?php $security = daway_user_get_security($account) ?>
				<?php foreach ($security as $key => $value) { ?>
					<?php if ($value['class'] != 'active') continue; ?>
					<a data-tooltip="<?php print $value['label'].' '.$value['value'] ?>" href="#"><?php print $value['label'] ?></a>
				<?php } ?>
			</span>
		</li>
	</ul>

</div>

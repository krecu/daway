<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div id="trips-container">
	<ul id="trips-tab" class="row">
		<li class="first"><a data-triptype="65540" href="#"><?php print t('Drivers') ?></a></li>
		<li><a data-triptype="65541" href="#"><?php print t('Passengers') ?></a></li>
		<li class="last"><a data-triptype="*" class="active" href="#"><?php print t('All') ?></a></li>
	</ul>

	<div class="<?php print $classes; ?>">
		<div class="row view-header">
			<div class="col left">
				<div id="trips-sorting" class="form-item sort">
					<label><?php print t('Sort by') ?></label>
					<select>
						<option value="date"><?php print t('Date') ?></option>
						<option value="price"><?php print t('Price') ?></option>
						<option value="user"><?php print t('User') ?></option>
						<option value="rating"><?php print t('Rating') ?></option>
					</select>
					<input name="sorting" type="checkbox">
				</div>
			</div>
			<div class="col right">
				<?php
					$display = 'grid';
					if (isset($_GET['display'])) $display = $_GET['display'];

				?>
				<ul id="trips-display" class="row">
					<li class="first"><a class="grid <?php print ($display=='grid') ? 'active' : '' ?>" data-tripdisplay="grid" href="?display=grid"><span class="icon"></span>grid</a></li>
					<li><a class="inline <?php print ($display=='inline') ? 'active' : '' ?>" data-tripdisplay="inline" href="?display=inline"><span class="icon"></span>inline</a></li>
					<li class="last"><a data-tripdisplay="map" class="map <?php print ($display=='map') ? 'active' : '' ?>" href="#"><span class="icon"></span>map</a></li>
				</ul>
			</div>
		</div>

	<?php if ($attachment_before): ?>
		<div class="attachment attachment-before">
			<?php print $attachment_before; ?>
		</div>
	<?php endif; ?>

	<div id="trips-list">
	<?php if ($rows): ?>
		<div class="view-content">
			<?php print $rows; ?>
		</div>
	<?php elseif ($empty): ?>
		<div class="view-empty">
			<?php print $empty; ?>
		</div>
	<?php endif; ?>
	</div>
	<div id="paginator-container">
		<?php if ($pager): ?>
			<?php print $pager; ?>
		<?php endif; ?>
	</div>

	<?php if ($attachment_after): ?>
		<div class="attachment attachment-after">
			<?php print $attachment_after; ?>
		</div>
	<?php endif; ?>

	<?php if ($more): ?>
		<?php print $more; ?>
	<?php endif; ?>

	<?php if ($footer): ?>
		<div class="view-footer">
			<?php print $footer; ?>
		</div>
	<?php endif; ?>

	<?php if ($feed_icon): ?>
		<div class="feed-icon">
			<?php print $feed_icon; ?>
		</div>
	<?php endif; ?>

	</div><?php /* class view */ ?>
</div>
<?php
/**
 * @file
 * Add stylesheets that are only needed when Zen is the enabled theme.
 *
 * Don't do something this dumb in your sub-theme. Stylesheets should be added
 * using your sub-theme's .info file. If you desire styles that apply
 * conditionally, you can conditionally add a "body class" in the
 * preprocess_page function. For an example, see how wireframe styling is
 * handled in zen_preprocess_html() and wireframes.css.
 */

/**
 * If the user is silly and enables Zen as the theme, manually add some stylesheets.
 */
function _develix_preprocess_html(&$variables, $hook) {
  $directory = drupal_get_path('theme', 'develix') . '/includes';
  $stylesheets = array(
    'reset.css',
  );
  foreach ($stylesheets as $stylesheet) {
    drupal_add_css($directory . '/css/' . $stylesheet, array('group' => CSS_THEME, 'every_page' => TRUE));
  }

	$javascripts = array(
		'html5.js',
	);
	foreach ($javascripts as $javascript) {
		drupal_add_css($directory . '/js/' . $javascript, array('group' => JS_THEME, 'every_page' => TRUE));
	}
}


/**
 * Implements HOOK_theme().
 *
 * We are simply using this hook as a convenient time to do some related work.
 */
function _develix_theme(&$existing, $type, $theme, $path) {
	return array();
}
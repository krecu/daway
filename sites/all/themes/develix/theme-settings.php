<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function develix_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['develix'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Develix settings'),
  );

	$form['develix']['supermenu'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Super Menu'),
	);
	$form['develix']['supermenu']['develix_supermenu_speed'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Speed'),
		'#default_value' => theme_get_setting('develix_supermenu_speed'),
	);


}

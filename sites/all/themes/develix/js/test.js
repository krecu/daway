
$(function () {
    $(document).on("click", ".js-link", function (e) {
        e.preventDefault();
        document.location = $(this).attr("data-href")
    });
    $(".wrapped.dropdown").dropdown("toggle");
    $(".text-short").each(function () {
        ShortenText.init($(this))
    });
    $(".geo-autocomplete").completeLocation();
    $(".input-number").inputNumber();
    $(".trip-search-form .reverse").on("click", function (h) {
        h.preventDefault();
        var f = $("#simple_search_from_name").val();
        var g = $("#simple_search_from_coordinates_latitude").val();
        var i = $("#simple_search_from_coordinates_longitude").val();
        $("#simple_search_from_name").val($("#simple_search_to_name").val());
        $("#simple_search_from_coordinates_latitude").val($("#simple_search_to_coordinates_latitude").val());
        $("#simple_search_from_coordinates_longitude").val($("#simple_search_to_coordinates_longitude").val());
        $("#simple_search_to_name").val(f);
        $("#simple_search_to_coordinates_latitude").val(g);
        $("#simple_search_to_coordinates_longitude").val(i)
    });
    bbc.initDatePicker = function (f) {
        var e = {selectOtherMonths: true, showAnim: "", minDate: new Date(), onSelect: function (h, g) {
            $(this).trigger("onSelect", h)
        }};
        if (f.attr("data-datepicker-minDate")) {
            e.minDate = f.attr("data-datepicker-minDate")
        }
        f.datepicker(e)
    };
    $(".date-picker, .datepicker").each(function () {
        bbc.initDatePicker($(this))
    });
    var b = function () {
        return confirm($(this).data("confirm"))
    };
    $("a.js-confirm[data-confirm]").on("click", b);
    $("form.js-confirm[data-confirm]").on("submit", b);
    $("input[placeholder], textarea[placeholder]").placeholder();
    $(".tip").each(function c() {
        var e = $(this);
        e.tipTip({defaultPosition: e.data("placement") || "top", activation: e.data("trigger") || "hover", delay: e.data("delay") || (e.data("trigger") === "focus") ? 0 : 400})
    });
    $(".tip-input").tipTip({defaultPosition: "right", activation: "focus", delay: 0});
    $(".tip-label").tipTip({defaultPosition: "top"});
    $(".popup").click(function () {
        var k = $(this).data("popup-title") || "", g = $(this).data("popup-width") || 600, e = $(this).data("popup-height") || 400, j = ($(window).width() - g) / 2, i = ($(window).height() - e) / 2, f = this.href, h = "status=1,width=" + g + ",height=" + e + ",top=" + i + ",left=" + j;
        window.open(f, k, h);
        return false
    });
    $(".toggler").live("click", function a() {
        $("#" + $(this).data("id")).toggle();
        return false
    });
    $(".confirm").live("click", function d() {
        return confirm($(this).data("confirm"))
    });
    $(".js-time-select").each(function () {
        var h = $(this);
        var f = h.find("select");
        var g = f.first();
        var e = f.last();
        g.on("change", function () {
            if ("" === e.val()) {
                e.val(0)
            }
        })
    });
    $("a[data-toggle=popover], button[data-toggle=popover]").popover().click(function (f) {
        f.preventDefault()
    })
});
function googleMapsLoaded() {
    $(document).trigger("googlemaps-loaded")
}
function loadGoogleMapScript() {
    if (googleMapsUrl === undefined) {
        console && console.error("Variable googleMapsUrl is missing")
    }
    var a = document.createElement("script");
    a.type = "text/javascript";
    a.src = googleMapsUrl + "&callback=googleMapsLoaded";
    document.body.appendChild(a)
}
function ensureGoogleMapLoaded(a) {
    if (typeof google != "undefined" && typeof google.maps != "undefined") {
        a();
        return
    }
    $(document).on("googlemaps-loaded", function () {
        a()
    });
    loadGoogleMapScript()
}
(function (a) {
    a.fn.completeLocation = function () {
        var b = a(this);
        b.on("focus", function () {
            ensureGoogleMapLoaded(function () {
                b.googleAutocomplete({preventSubmit: true})
            })
        })
    };
    a.fn.showMap = function () {
        var j = new google.maps.DirectionsService();
        var f = a("li", this);
        var k = f.first().attr("data-latitude") + ", " + f.first().attr("data-longitude");
        var h = f.last().attr("data-latitude") + ", " + f.last().attr("data-longitude");
        var g = [];
        if (2 < f.length) {
            for (var e = 1; e < f.length - 1; e++) {
                g.push({location: f.eq(e).attr("data-latitude") + ", " + f.eq(e).attr("data-longitude"), stopover: true})
            }
        }
        var c = "0" === this.attr("data-highway") ? true : false;
        var m = {center: new google.maps.LatLng(48, 8566, 2.3522), mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl: false, panControl: true};
        var b = new google.maps.Map(this.get(0), m);
        var d = {origin: k, destination: h, waypoints: g, avoidHighways: c, travelMode: google.maps.TravelMode.DRIVING};
        var l = new google.maps.DirectionsRenderer();
        j.route(d, function (i, n) {
            if (n == google.maps.DirectionsStatus.OK) {
                l.setDirections(i)
            }
        });
        l.setMap(b)
    }
})(jQuery);
$(document).ready(function () {
    $(".dropdown-toggle").dropdown();
    var a = $(".slider ul");
    if (a.length) {
        a.cycle({fx: "scrollHorz", prev: $(".homepage-extra-press .prev"), next: $(".homepage-extra-press .next"), timeout: 0})
    }
    var b = $(".homepage-feature-lowcost ul");
    if (b.length) {
        b.cycle({fx: "scrollVert", pause: true, timeout: 6 * 1000, speed: 2 * 1000})
    }
    $(".play-button").youtubeLink({target: "dialog"})
});
$(document).ready(function () {
    $(".login-toggle").bind("click", function (b) {
        b.preventDefault();
        var c = this.getAttribute("data-text");
        if (c) {
            $(".text-intro").html(c).css("display", "block")
        } else {
            $(".text-intro").html("").css("display", "none")
        }
        $(".login").toggle();
        return false
    });
    $(document).mouseup(function (c) {
        var d = !$(c.target).hasClass("login-toggle");
        var b = $(".login");
        if (d && b.has(c.target).length === 0) {
            b.hide()
        }
    });
    $(".display-map").bind("click", function () {
        var b = this.getAttribute("data-origin");
        var c = this.getAttribute("data-alt");
        if ($(".maps").is(":visible")) {
            $("a.display-map").html(b)
        } else {
            $("a.display-map").html(c)
        }
        $(".maps").slideToggle()
    });
    $(".close-map").bind("click", function () {
        var b = this.getAttribute("data-origin");
        var c = this.getAttribute("data-alt");
        if ($(".maps").is(":visible")) {
            $("a.display-map").html(b)
        } else {
            $("a.display-map").html(c)
        }
        $(".maps").slideToggle()
    });
    $(".map-string").bind("click", function () {
        var b = this.getAttribute("data-origin");
        var c = this.getAttribute("data-alt");
        if ($(".maps").is(":visible")) {
            $("a.display-map").html(b)
        } else {
            $("a.display-map").html(c)
        }
        $(".maps").slideToggle()
    });
    $(".display-map").bind("click.map", function () {
        ensureGoogleMapLoaded(function () {
            $(".geo-map", $(".maps")).showMap();
            $(".display-map").unbind("click.map")
        })
    });
    $(".contact-me-dialog").each(function () {
        var b = $(this).hasClass("auto-open");
        $(this).dialog({autoOpen: b, dialogClass: "contact-dialog", width: 614, modal: true})
    });
    $(".contact-me").bind("click", function () {
        $(".contact-me-dialog").dialog("open")
    });
    $(function () {
        var b = $(".countdown");
        $(b).each(function () {
            var e = this.getAttribute("data-date").split(" "), g = parseFloat(e[0]), h = parseFloat(e[1]) - 1, d = parseFloat(e[2]), c = parseFloat(e[3]), f = parseFloat(e[4]);
            $(this).countdown({until: new Date(g, h, d, c, f), compact: true, layout: "{hnn}{hl} {mnn}{ml} {snn}{sl}"})
        })
    });
    $(".duplicate-publishing .btn-close").on("click", function () {
        $(this).closest(".duplicate-publishing").hide()
    });
    $(".return-publishing .btn-close").on("click", function () {
        $(this).closest(".return-publishing").hide()
    });
    function a() {
        if ($("#add-seat").length == 1) {
            var i = $(".input-number-passengers input"), e = i.data("number-min"), k = i.data("number-max"), c = $(".input-number-passengers .btn-plus"), j = $(".input-number-passengers .btn-minus"), f = $("#add-seat"), d = $("#remove-seat"), g = $("#full-seat"), b = i.data("message-max"), h = i.data("message-min");
            c.on("click", function () {
                $inputData = $(".input-number-passengers input").val();
                if ($inputData < k) {
                    f.modal("show")
                }
                if ($inputData >= k) {
                    i.tooltip({title: b, animation: true, placement: "top"}).tooltip("show");
                    setTimeout(function l() {
                        i.tooltip("hide");
                        i.tooltip("destroy")
                    }, 5000)
                }
            });
            j.on("click", function () {
                $inputData = $(".input-number-passengers input").val();
                if ($inputData > e) {
                    d.modal("show")
                }
                if ($inputData == 1) {
                    g.modal("show")
                }
            });
            $(".add-seat-passenger").on("click", function () {
                $inputDataNew = $(".input-number-passengers input").val();
                i.val(parseInt($inputDataNew) + 1);
                if ($(".label-full").is(":visible")) {
                    $(".label-full").addClass("hide")
                }
                f.modal("hide")
            });
            $(".remove-seat-passenger").on("click", function () {
                $inputDataNew = $(".input-number-passengers input").val();
                i.val(parseInt($inputDataNew) - 1);
                d.modal("hide")
            });
            $(".full-seat-passenger").on("click", function () {
                $inputDataNew = $(".input-number-passengers input").val();
                i.val(parseInt($inputDataNew) - 1);
                g.modal("hide");
                $(".label-full").toggleClass("hide")
            })
        }
    }

    a();
    $(".cancel-passenger").submit(function () {
        var d = $(".cancel-passenger select").val(), b = $(".cancel-passenger textarea").val(), c = $(".cancel-passenger .btn-validation");
        $(c).find('img[src$="/ajax-loader-blue.gif"]').remove();
        if ((d == 0) && (b == "")) {
            $(".alert-select").show();
            $(".alert-textearea").show();
            return false
        } else {
            $(".alert-select").hide();
            $(".alert-textearea").hide()
        }
        if (d == 0) {
            $(".alert-select").show();
            return false
        } else {
            $(".alert-select").hide()
        }
        if (b == "") {
            $(".alert-textearea").show();
            return false
        } else {
            $(".alert-textearea").hide()
        }
        $(c).prepend('<img src="/images/ajax-loader-blue.gif" style="margin-right:5px; display:none" />');
        return true
    });
    $(".cancel-trip").submit(function () {
        var d = $(".cancel-trip select").val(), b = $(".cancel-trip textarea").val(), c = $(".cancel-trip .btn-validation");
        $(c).find('img[src$="/ajax-loader-blue.gif"]').remove();
        if ((d == 0) && (b == "")) {
            $(".alert-select").show();
            $(".alert-textearea").show();
            return false
        } else {
            $(".alert-select").hide();
            $(".alert-textearea").hide()
        }
        if (d == 0) {
            $(".alert-select").show();
            return false
        } else {
            $(".alert-select").hide()
        }
        if (b == "") {
            $(".alert-textearea").show();
            return false
        } else {
            $(".alert-textearea").hide()
        }
        $(c).prepend('<img src="/images/ajax-loader-blue.gif" style="margin-right:5px; display:none" />');
        return true
    });
    $('a[href="#cancel-passenger"]').on("click", function () {
        $(".alert-select").hide();
        $(".alert-textearea").hide()
    });
    $('a[href="#delete-trip"]').on("click", function () {
        $(".alert-select").hide();
        $(".alert-textearea").hide()
    })
});
var trip_details = (function (b) {
    function a() {
        var d = {btn: b(".btn-return-trip"), close: b(".return-trip-container .close"), container: b(".return-trip-container")};
        var c = {btn: b(".btn-duplicate-trip"), close: b(".duplicate-trip-container .close"), container: b(".duplicate-trip-container")};
        var e = function (g, f) {
            g.btn.on("click", function (h) {
                h.preventDefault();
                g.btn.toggleClass("active");
                f.btn.removeClass("active");
                g.container.toggleClass("active");
                f.container.removeClass("active")
            });
            g.close.on("click", function (h) {
                h.preventDefault();
                g.btn.removeClass("active");
                g.container.removeClass("active")
            })
        };
        e(d, c);
        e(c, d)
    }

    return{init: function () {
        if (b(".trip-management-container").length) {
            a()
        }
    }}
})(jQuery);
contact = (function (e) {
    var d = {};

    function c() {
        if (e("#contact_category").length) {
            lib = e('#contact_categoryFilter option[value=""]').text();
            libCat = '<option value="' + lib + '" selected="selected" class="first">' + lib + "</option>";
            var g = e("#contact_category").clone();
            g.attr("id", "contact_category_hidden").attr("name", "").hide();
            e("#contact_category").parent().append(g);
            a();
            e("#contact_categoryFilter").live("change", a)
        }
    }

    function a() {
        var g = "";
        var h = e("#contact_categoryFilter").val();
        e("#contact_category").parent().parent().toggle((h != g));
        if (h != g) {
            e("#contact_category > option").remove();
            e("#contact_category_hidden > option").each(function () {
                if (e(this).val().indexOf(h) == 0) {
                    e("#contact_category").append(e(this).clone())
                }
            });
            e("#contact_category").prepend(libCat)
        }
    }

    function f() {
        if (e(this).val() == e(this).find("option").first().val()) {
            e(this).addClass("default")
        } else {
            e(this).removeClass("default")
        }
    }

    e(function () {
        e(".select-contact").on("change", f);
        e(".select-contact").trigger("change")
    });
    function b() {
        c()
    }

    return{init: function () {
        b()
    }}
})(jQuery);
var publication = (function (z) {
    var y = z("#map1");
    var x = z("#map2");
    var c = 6;
    var k = 2;
    var g = c;

    function K() {
        var T = z("input.from").attr("id").replace(new RegExp("_name$", "gm"), "");
        return l("#" + T)
    }

    function L() {
        var T = z("input.to").attr("id").replace(new RegExp("_name$", "gm"), "");
        return l("#" + T)
    }

    function e() {
        var T = [];
        z(".stages-list > li .stage").each(function () {
            var V = z(this).attr("id").replace(new RegExp("_name$", "gm"), "");
            if (z("#" + V + "_name").length > 0) {
                var U = l("#" + V);
                if (U) {
                    T.push({location: U})
                }
            }
        });
        return T
    }

    function l(V) {
        var T = z(V + "_name").val();
        var W = z(V + "_coordinates_latitude").val();
        var U = z(V + "_coordinates_longitude").val();
        if (W && U) {
            W = parseFloat(W);
            U = parseFloat(U);
            return new google.maps.LatLng(W, U)
        }
        return T
    }

    function R() {
        if (z(".highway-container input").length < 1) {
            return false
        }
        return !z(".highway-container input").is(":checked")
    }

    function O() {
        y.gmap3({clear: {}});
        var V = K();
        var U = L();
        var T = e();
        if (V && U) {
            y.gmap3({getroute: {options: {origin: V, destination: U, waypoints: T, avoidHighways: R(), travelMode: google.maps.DirectionsTravelMode.DRIVING}, callback: function (W) {
                if (!W) {
                    return
                }
                y.gmap3({directionsrenderer: {options: {directions: W}}})
            }}})
        } else {
            if (V) {
                y.gmap3({marker: {latLng: V}})
            } else {
                if (U) {
                    y.gmap3({marker: {latLng: U}})
                }
            }
        }
    }

    function i() {
        y.gmap3({map: {options: {center: [y.attr("data-latitude"), y.attr("data-longitude")], mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl: false, zoom: 5, mapTypeControl: false}}});
        z(".from").on("onPlaceChanged", function () {
            O();
            d()
        });
        z(".from").on("geocoded", function () {
            O();
            d()
        });
        z(".to").on("onPlaceChanged", function () {
            O();
            d()
        });
        z(".to").on("geocoded", function () {
            O();
            d()
        });
        z(document).on("onPlaceChanged, geocoded", ".stage", function () {
            O()
        });
        z(".stage").on("onPlaceChanged", function () {
            O()
        });
        z(".from").on("place_unset", function (T, U) {
            if (!U) {
                O()
            }
        });
        z(".to").on("place_unset", function (T, U) {
            if (!U) {
                O()
            }
        });
        z(document).on("place_unset", ".stage", function (T, U) {
            if (!U) {
                O()
            }
        })
    }

    function d() {
        var Z = K();
        var Y = L();
        if (!(Z instanceof google.maps.LatLng) || !(Y instanceof google.maps.LatLng)) {
            return
        }
        var W = z(".suggestions");
        var V = W.find("p");
        var T = W.find(".suggestions-list");
        var U = W.attr("data-url");
        var X = ["from=" + Z.lat() + "," + Z.lng(), "to=" + Y.lat() + "," + Y.lng()];
        z.ajax({method: "GET", url: U + "?" + X.join("&")}).done(function (ae) {
            var aa = ae;
            T.empty();
            if (0 === aa.length) {
                V.hide();
                return
            }
            V.show();
            for (var ad in aa) {
                var ac = aa[ad];
                var ab = z("<a />").text(ac.label).attr("data-latitude", ac.latitude).attr("data-longitude", ac.longitude);
                z("<li />").append(ab).appendTo(T)
            }
        })
    }

    function b() {
        z(document).on("click", ".suggestions .suggestions-list a", function (V) {
            V.preventDefault();
            var Y = z(this);
            var X = j() || P();
            if (!X) {
                return
            }
            var T = X.find('input[type="text"]');
            var Z = X.find('input[type="hidden"]:eq(0)');
            var ab = X.find('input[type="hidden"]:eq(1)');
            T.val(Y.text());
            Z.val(Y.attr("data-latitude"));
            ab.val(Y.attr("data-longitude"));
            O();
            z(this).closest("li").remove();
            var U = z(".suggestions");
            var aa = U.find("p");
            var W = U.find(".suggestions-list");
            if (W.is(":empty")) {
                aa.hide()
            }
        })
    }

    function n() {
        var W = x.find("li").first();
        var U = x.find("li").last();
        var V = x.find("li").slice(1, -1);
        var aa = [W.attr("data-latitude"), W.attr("data-longitude")];
        var ab = [U.attr("data-latitude"), U.attr("data-longitude")];
        var ac = "0" === z("#map2").attr("data-freeway") ? false : true;
        var Y = [];
        for (var X = 0; X < V.size(); X++) {
            var Z = V[X].getAttribute("data-latitude");
            var T = V[X].getAttribute("data-longitude");
            Y.push({location: new google.maps.LatLng(Z, T)})
        }
        x.gmap3({map: {options: {center: [x.attr("data-latitude"), x.attr("data-longitude")], mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl: false, scrollwheel: false, zoomControl: false, mapTypeControl: false, disableDoubleClickZoom: true, zoom: 5}}, getroute: {options: {origin: aa, destination: ab, waypoints: Y, avoidHighways: !ac, travelMode: google.maps.DirectionsTravelMode.DRIVING}, callback: function (ad) {
            if (!ad) {
                return
            }
            z(this).gmap3({directionsrenderer: {options: {preserveViewport: false, draggable: false, directions: ad}}})
        }}})
    }

    function S() {
        h();
        itineraryManager.init()
    }

    function A() {
        z("input.from").on("onPlaceChanged", function () {
            z("input.to").focus()
        });
        z(".departure-date input.date-picker").on("onSelect", function (U, T) {
            z(".return-date input.date-picker").datepicker("option", "minDate", z.datepicker.parseDate(z(this).datepicker("option", "dateFormat"), T))
        })
    }

    function I() {
        z(".simple-round-choice").on("click", function () {
            var V = z(".simple-round-choice").attr("data-origin"), U = z(".simple-round-choice").attr("data-alt"), T = z(".legend-green-label").text();
            if (T !== U) {
                z(".legend-green-label").text(U)
            } else {
                z(".legend-green-label").text(V)
            }
        })
    }

    function q() {
        z('input[name="simple-choice"]').on("click", function () {
            z(".simple-choice-label").toggleClass("green-label")
        });
        z('input[name="round-choice"]').on("click", function () {
            z(".round-choice-label").toggleClass("blue-label")
        })
    }

    function h() {
        var U = z("ol.stages-list");
        var T = U.find(".plus-button");
        F(U, T);
        E()
    }

    function D(T) {
        g = +T
    }

    function H(T) {
        if (typeof T === "undefined") {
            T = (z("ol.stages-list .stage").size() < g)
        }
        z(".stage-add")[T ? "show" : "hide"]()
    }

    function F(U, T) {
        H();
        T.bind("click", function (V) {
            V.preventDefault();
            P()
        })
    }

    function j() {
        $inputs = z('ol.stages-list input[type="text"]');
        if (1 === $inputs.length && "" === $inputs.first().val()) {
            return $inputs.first().closest("li")
        }
        var T = $inputs.filter("[value=]");
        if (T.length === 0) {
            return null
        }
        return T.first().closest("li")
    }

    function P() {
        var X = z("ol.stages-list");
        var T = X.find(".plus-button");
        if (z("ol.stages-list .stage").size() >= g) {
            z(".stage-add").hide();
            return
        }
        var V = X.attr("data-prototype");
        var W = V.replace(/__name__/g, X.children().length);
        var U = z("<li>" + W + "</li>");
        X.find("li:last-child").before(U);
        U.find(".geo-autocomplete").completeLocation();
        U.bind("onPlaceChanged", function () {
            O()
        });
        U.bind("blur", function () {
            O()
        });
        if (z("ol.stages-list .stage").size() >= g) {
            z(".stage-add").hide()
        }
        return U
    }

    function f() {
        z(".stages-list .stage").each(function () {
            z(this).closest("li").remove()
        });
        z(".stages-list .plus-button").trigger("click");
        O();
        if (z("ol.stages-list .stage").size() >= g) {
            z(".stage-add").hide()
        } else {
            z(".stage-add").show()
        }
    }

    function E() {
        z(".stages-list").on("click", ".close", function (U) {
            U.preventDefault();
            var T = z(this).closest("li");
            T.remove();
            if (z("ol.stages-list .stage").size() < g) {
                z(".stage-add").show()
            }
            O()
        })
    }

    function o() {
        z(".input-price").inputNumber()
    }

    function G() {
        z(".publication-price-container input[data-range]").each(function () {
            Q(z(this))
        });
        z(".publication-price-container").on("onNumberChanged", "input[data-range]", function () {
            B();
            Q(z(this))
        })
    }

    function Q(aa) {
        var ae = aa.closest(".publication-price-container");
        var U = aa.data("range");
        var ad = +aa.val();
        var X;
        var T = [];
        var Y, W, ab;
        if (!U) {
            return
        }
        var Z = aa.data("range").split("|");
        for (W = 0, ab = Z.length; W < ab; W++) {
            Y = Z[W].split(":");
            var ac = Y[0], V = Y[1];
            T.push(V);
            if (ad >= ac) {
                X = V
            }
        }
        if (X) {
            for (W = 0, ab = T.length; W < ab; W++) {
                var V = T[W];
                ae.removeClass("price-" + V)
            }
            ae.addClass("price-" + X)
        }
    }

    function B() {
        var T = 0;
        z(".price-stages-list input").each(function () {
            var U = +z(this).val();
            if (!isNaN(U)) {
                T += Math.floor(U)
            }
        });
        $totalPriceInput = z("#total-price");
        $totalPriceInput.val(T);
        Q($totalPriceInput)
    }

    function s() {
        if (z("#new_publication_step1_frequency_0").length == 0) {
            return
        }
        z("#new_publication_step1_frequency_0, #new_publication_step1_frequency_1").on("change", function () {
            var U = z(this).val() == "REGULAR";
            J(U);
            D(U ? k : c);
            f()
        });
        var T = z("#new_publication_step1_frequency_1").is(":checked");
        J(T);
        D(T ? k : c)
    }

    function J(T) {
        z("#publication-unique").toggle(!T);
        z("#publication-regular").toggle(T)
    }

    function N() {
        if (true === z(".simple-round-container input.trip-mode").is(":checked")) {
            z(".round-container").show()
        } else {
            z(".round-container").hide()
        }
    }

    function M() {
        z(".simple-round-container input.trip-mode").on("change", N);
        N()
    }

    function r() {
        z(".highway-container input").on("change", function () {
            O()
        })
    }

    function t() {
        var T = z(".same-comments-container input[type=checkbox]").is(":checked");
        z(".comments-return-container").toggle(!T)
    }

    function p() {
        z(".trip-same-comment").on("change", t);
        t()
    }

    function u() {
        var T = z(".more-infos");
        T.bind("click", function () {
            var V = z(this).attr("data-origin");
            var W = z(this).attr("data-alt");
            var U = z(this).parents(".expand-container:first").find(".expand");
            if (U.is(":visible")) {
                z(this).text(V)
            } else {
                z(this).text(W)
            }
            if (z(this).hasClass("single-use")) {
                U.slideDown()
            } else {
                U.slideToggle()
            }
        })
    }

    function m() {
        var U = z('input[type="radio"].hide-optionnal');
        var T = z(".expand-optionnal");
        U.bind("click", function () {
            if (T.is(":visible")) {
                T.slideUp()
            }
        })
    }

    function w() {
        var U = z(".total-coupon-container");
        var T = z('input[type="radio"].hide-total');
        z(T).bind("click", function () {
            U.fadeOut()
        })
    }

    function a() {
        var U = z(".total-coupon-container");
        var T = z('input[type="radio"].show-total');
        z(T).bind("click", function () {
            U.fadeIn()
        })
    }

    function v() {
        var T = z('input[type="checkbox"].uncheck-popin');
        T.bind("click", function U() {
            if (T.is(":checked")) {
            } else {
                z("#total-one-time").modal()
            }
        })
    }

    function C() {
        var T = z(".check-total");
        T.bind("click", function () {
            z('input[type="checkbox"].uncheck-popin').attr("checked", true)
        })
    }

    return{initStep1: function () {
        ensureGoogleMapLoaded(function () {
            s();
            S();
            i();
            O();
            A();
            M();
            r();
            I();
            q();
            b()
        })
    }, initStep2: function () {
        ensureGoogleMapLoaded(function () {
            S();
            B();
            o();
            G();
            p();
            n()
        })
    }, initStep3: function () {
        ensureGoogleMapLoaded(function () {
            S();
            u();
            v();
            m();
            w();
            a();
            C();
            n()
        })
    }}
})(jQuery);
var itineraryManager = {$btnAddWeekly: null, $btnAddSingle: null, init: function () {
    itineraryManager.$btnAddWeekly = $("#btnAddWeekly");
    itineraryManager.$btnAddSingle = $("#btnAddSingle");
    itineraryManager.bindEvents()
}, bindEvents: function () {
    itineraryManager.$btnAddSingle.on("click", function () {
        itineraryManager.addTripContainer(1);
        itineraryManager.updateForm();
        return false
    });
    itineraryManager.$btnAddWeekly.on("click", function () {
        itineraryManager.addTripContainer(itineraryManager.countRemaining());
        itineraryManager.showReminderTip("on");
        itineraryManager.updateForm();
        return false
    })
}, showReminderTip: function (a) {
    if (a == "on") {
        $(".reminder-weekly").removeClass("hide")
    } else {
        $(".reminder-weekly").addClass("hide")
    }
}, removeTripContainer: function (a) {
    $("#" + a).remove()
}, addTripContainer: function (b) {
    for (var a = 0; a < b; a++) {
        if (b == 1) {
            itineraryManager.cloneTripContainer("more-date")
        } else {
            itineraryManager.cloneTripContainer()
        }
    }
}, changeButtonText: function (b, a) {
    $("#" + a + " span").text(b)
}, toggleButton: function (a, b) {
    if (a.toLowerCase() == "hide") {
        $(b).hide()
    } else {
        $(b).show()
    }
}, updateForm: function () {
    var a = itineraryManager.countTripContainers() - 1;
    if (a >= 5) {
        var b = itineraryManager.$btnAddWeekly.attr("data-text-alt");
        b = b.replace("[count]", itineraryManager.countRemaining);
        itineraryManager.changeButtonText(b, itineraryManager.$btnAddWeekly.attr("id"))
    } else {
        itineraryManager.changeButtonText(itineraryManager.$btnAddWeekly.attr("data-text"), itineraryManager.$btnAddWeekly.attr("id"))
    }
    if (a >= 10) {
        itineraryManager.toggleButton("hide", itineraryManager.$btnAddWeekly);
        itineraryManager.toggleButton("hide", itineraryManager.$btnAddSingle)
    } else {
        itineraryManager.toggleButton("show", itineraryManager.$btnAddWeekly);
        itineraryManager.toggleButton("show", itineraryManager.$btnAddSingle)
    }
    if (a == 0) {
        itineraryManager.showReminderTip("off")
    }
}, cloneTripContainer: function (c) {
    $("#container-template").clone().appendTo("#trip-containers");
    var a = $("#container-template")[0];
    var b = itineraryManager.countTripContainers() - 1;
    $(a).attr("id", "trip" + b);
    $(a).find("#dep-date").attr("name", "dep-date-trip" + b);
    $(a).find("#dep-hour").attr("name", "dep-hour-trip" + b);
    $(a).find("#dep-min").attr("name", "dep-min-trip" + b);
    $(a).find("#ret-date").attr("name", "ret-date-trip" + b);
    $(a).find("#ret-hour").attr("name", "ret-hour-trip" + b);
    $(a).find("#ret-min").attr("name", "ret-min-trip" + b);
    if (c == "more-date") {
        $(a).addClass("single-date")
    }
    $(a).find("#btn-remove").on("click", function () {
        itineraryManager.removeTripContainer($(this).parent($("li.trip")).parent().attr("id"));
        itineraryManager.updateForm();
        return false
    });
    $(a).removeClass("hide")
}, countTripContainers: function () {
    return $(".trip").length
}, countRemaining: function () {
    var c = itineraryManager.countTripContainers() - 1;
    var a = 0;
    var b = c / 5;
    if (b < 1) {
        a = (5 - c)
    } else {
        a = (10 - c)
    }
    return a
}};
$(".reminder-weekly .close").on("click", function () {
    $(this).parent().addClass("hide")
});
window.funnelPublication = (function (d, c, h) {
    var a = {val: function () {
        return d(".js-seatCount-trigger").val()
    }, set: function (l) {
        d(".seat-placeholder").text(l);
        g.update();
        var k = d(".list-seats-available").html("");
        var m = k.data("prototype");
        for (var j = a.val() - 1; j >= 0; j--) {
            k.append(m)
        }
    }, update: function () {
        this.set(this.val())
    }};
    var b = {val: function () {
        return d(".js-price-trigger").val()
    }, set: function (i, j) {
        d(".js-price-placeholder").text(j.replace("%value%", i));
        g.update()
    }, update: function () {
        this.set(this.val(), d(".js-price-trigger").data("price-format"))
    }};
    var g = {val: function () {
        return Math.max(1, Math.round(b.val() / a.val()))
    }, set: function (i, j) {
        d(".js-price-passenger").text(j.replace("%value%", i))
    }, update: function () {
        this.set(this.val(), d(".js-price-trigger").data("price-format"))
    }};
    var f = {hours: function () {
        return d("#publication_step1_from_date_time_hour option:selected").text()
    }, minutes: function () {
        return d("#publication_step1_from_date_time_minute option:selected").text()
    }, set: function (i, j) {
        if ("" == i || "" == j) {
            d(".hour-container").text("")
        }
        d(".hour-container").text(i + ":" + j)
    }, update: function () {
        this.set(this.hours(), this.minutes())
    }};
    var e = function () {
        d(".js-seatCount-trigger").on("onNumberChanged", function () {
            a.update()
        });
        d(".js-price-trigger").on("onNumberChanged", function () {
            b.update()
        });
        d("#publication_step1_from_date_time_hour, #publication_step1_from_date_time_minute").on("change", function () {
            f.update()
        })
    };
    return{init: function () {
        e()
    }}
})(jQuery, window);
window.funnel = (function (e, g, c) {
    function d(l, m) {
        this.value = l;
        this.format = m
    }

    d.prototype.toString = function () {
        return this.format.replace("123", this.value)
    };
    var j = {departureInput: "#funnel_publication_departurePlace_name", arrivalInput: "#funnel_publication_arrivalPlace_name", stopoverInputs: '#stopovers-collection li input[type="text"]', $departureInput: null, $arrivalInput: null, $stopoverInputs: null, stopoversHolder: null, map: null};
    var k;
    var h = function () {
        k = new Binder({suggestedPrice: null, passengerCount: parseInt(e("#funnel_publication_seatCount").val(), 10), price: null, departurePlace: null, arrivalPlace: null, stopovers: [], departureDate: null, departureHour: null, departureMinute: null, departureTime: function (m) {
            var l = m.get("departureHour");
            var n = m.get("departureMinute");
            if (null === l || null === n) {
                return null
            }
            l = ("0" + l).slice(-2);
            n = ("0" + n).slice(-2);
            return l + ":" + n
        }, isRoundTrip: e("#funnel_publication_roundTrip").is(":checked"), departureDateDay: function (l) {
            if (!l.get("departureDate")) {
                return null
            }
            return e.datepicker.formatDate("d", l.get("departureDate"))
        }, departureDateMonth: function (l) {
            if (!l.get("departureDate")) {
                return null
            }
            return e.datepicker.formatDate("M", l.get("departureDate"))
        }, totalPrice: function (l) {
            var n = l.get("passengerCount");
            var m = l.get("price");
            return(m && n) ? new d(n * m.value, g.BLABLACAR_PRICE_FORMAT) : null
        }, showPreview: function (l) {
            return l.get("departurePlace") && l.get("arrivalPlace") && l.get("departureDate") && l.get("price")
        }});
        j.$departureInput = e(j.departureInput);
        j.$arrivalInput = e(j.arrivalInput);
        j.$stopoverInputs = e(j.stopoverInputs);
        j.stopoversHolder = new CollectionHolder("#stopovers-collection", {addItemCallback: function (l) {
            e(l).find(".geo-autocomplete").completeLocation()
        }, maxItemCount: 7});
        j.map = new RideMap("#ride-map")
    };
    var a = function () {
        var m = new google.maps.Geocoder();
        j.$departureInput.on("place_changed geocoded", function (o) {
            b();
            f()
        });
        j.$arrivalInput.on("place_changed geocoded", function (o) {
            b();
            f()
        });
        e(document).on("place_changed geocoded", j.stopoverInputs, function (o) {
            b()
        });
        j.$departureInput.on("place_unset", function (o, p) {
            if (!p) {
                b()
            }
        });
        j.$arrivalInput.on("place_unset", function (o, p) {
            if (!p) {
                b()
            }
        });
        e(document).on("place_unset", j.stopoverInputs, function (o, p) {
            if (!p) {
                b()
            }
        });
        e(document).on("remove_item", "#stopovers-collection", function (o) {
            b()
        });
        e(".js-seatCount-trigger").on("onNumberChanged", function (o, p) {
            k.set("passengerCount", p)
        });
        e(".js-price-trigger").on("onNumberChanged", function (o, p) {
            k.set("price", new d(p, g.BLABLACAR_PRICE_FORMAT))
        });
        e("#funnel_publication_departureDate_date").on("onSelect", function (p, o) {
            i()
        });
        function n() {
            var p = e("#funnel_publication_roundTrip").is(":checked");
            var o = ["#funnel_publication_returnDate_date", "#funnel_publication_returnDate_time_hour", "#funnel_publication_returnDate_time_minute"];
            k.set("isRoundTrip", p);
            if (p) {
                e.each(o, function (q, r) {
                    e(r).removeAttr("disabled")
                })
            } else {
                e.each(o, function (q, r) {
                    e(r).attr("disabled", "disabled")
                })
            }
        }

        n();
        e("#funnel_publication_roundTrip").on("change", n);
        i();
        function l() {
            var p, o = null, q = null;
            p = e("#funnel_publication_departureDate_time_hour").val();
            if (p) {
                o = p
            }
            p = e("#funnel_publication_departureDate_time_minute").val();
            if (p) {
                q = p
            }
            k.set("departureHour", o);
            k.set("departureMinute", q)
        }

        l();
        e("#funnel_publication_departureDate_time_hour, #funnel_publication_departureDate_time_minute").on("change", function (o) {
            l()
        })
    };

    function b() {
        var t = j.$departureInput.data("geo-location");
        var r = j.$arrivalInput.data("geo-location");
        var q = j.$departureInput.val();
        var o = j.$arrivalInput.val();
        if (t && q) {
            k.set("departurePlace", q)
        }
        if (r && o) {
            k.set("arrivalPlace", o)
        }
        j.map.setDeparture(t).setArrival(r);
        var m = [];
        j.$stopoverInputs = e(j.stopoverInputs);
        j.map.setStopovers([]);
        for (var p = 0, s = j.$stopoverInputs.length; p < s; p++) {
            var l = e(j.$stopoverInputs.get(p)).data("geo-location");
            if (l) {
                j.map.addStopover(l);
                m.push(l.name)
            }
        }
        k.set("stopovers", m);
        j.map.refreshView()
    }

    function f() {
        var m = j.$departureInput.data("geo-location");
        var l = j.$arrivalInput.data("geo-location");
        if (!m || !l) {
            return
        }
        e.ajax({url: e("#saving-block").data("url"), dataType: "json", data: {departure: m.coordinates.toString(), arrival: l.coordinates.toString()}, success: function (p) {
            if (p.error) {
                return
            }
            var q = parseInt(p.suggested_price);
            var o = parseInt(p.max_price);
            var n = parseInt(p.min_price);
            k.set("price", new d(q, g.BLABLACAR_PRICE_FORMAT));
            k.set("suggestedPrice", new d(q, g.BLABLACAR_PRICE_FORMAT));
            e(".js-price-trigger").val(q);
            e(".js-totalPrice").data("number-max", o);
            e(".js-totalPrice").data("number-min", n);
            e(".js-totalPrice").data("number-default", q);
            e(".js-totalPrice").inputNumber()
        }})
    }

    function i() {
        var m = e("#funnel_publication_departureDate_date");
        var n = m.val();
        if (!n) {
            return
        }
        var l = e.datepicker.parseDate(m.datepicker("option", "dateFormat"), n);
        e("#funnel_publication_returnDate_date").datepicker("option", "minDate", l);
        k.set("departureDate", new Date(l))
    }

    return{init: function () {
        ensureGoogleMapLoaded(function () {
            h();
            a()
        })
    }}
})(jQuery, window);
profilePicture = (function (d) {
    function c() {
        d("form.form-upload-auto-submit input[type=file]").bind("change", function () {
            var e = d(this).parents(".photo-choice-container").find("a.default-text");
            e.html(e.attr("data-loading-text")).addClass("loading-photo");
            d(this).parents("form").submit()
        })
    }

    function b() {
        d(".photo-fb").click(function () {
            var e = d(this).parents(".photo-choice-container").find("a.default-text");
            e.html(e.attr("data-loading-text")).addClass("loading-photo")
        })
    }

    function a() {
        b()
    }

    return{init: function () {
        a()
    }}
})(jQuery);
$(function () {
    $('select[name="he"], select[name="hb"]').change(function () {
        $(this).parents("form").submit()
    });
    $("#simple_search_from_name").on("onPlaceChanged", function c() {
        $("#simple_search_to_name").focus()
    });
    $("#simple_search_to_name").on("onPlaceChanged", function c() {
        if ($("#simple_search_dateBegin")) {
            $("#simple_search_dateBegin").focus()
        }
    });
    var a = function (g, j) {
        var e = "";
        var h;
        g = g.split("|");
        for (var f = 0, d = g.length; f < d; f++) {
            h = g[f].split(":");
            e += '<span><b class="' + h.shift() + '"></b> ' + h.join(":") + "</span>"
        }
        j.after('<div class="ui-datepicker-legend">' + e + "</div>")
    };
    $(".date-picker-heatmap").each(function b() {
        var h = $(this);
        var k = $(this).data("list");
        var g = $("#" + k);
        var j = {};
        var f = {};
        if (g) {
            g.find("option").each(function () {
                j[$(this).text()] = {value: this.value, "class": this.className};
                f[this.className] = 1
            })
        }
        var d;
        var i = function () {
            clearTimeout(d);
            d = setTimeout(function () {
                a(g.attr("data-legend"), $("#ui-datepicker-div .ui-datepicker-calendar"))
            }, 30)
        };
        var l = {selectOtherMonths: true, showAnim: "", minDate: new Date(), beforeShowDay: function e(m) {
            var o = $.datepicker.formatDate($.datepicker._defaults.dateFormat, m);
            if (j.hasOwnProperty(o)) {
                if (j[o].value == 1) {
                    var n = "data-label-trip-count"
                } else {
                    var n = "data-label-trips-count"
                }
                return[true, j[o]["class"], j[o].value + " " + $(this).attr(n)]
            } else {
                if (m >= new Date()) {
                    return[true, "data-label-notrip", $(this).attr("data-label-notrip")]
                }
            }
            return[false, "", ""]
        }, beforeShow: function () {
            i()
        }, onChangeMonthYear: function () {
            i()
        }, onSelect: function () {
            $(this).parents("form").submit()
        }};
        if (h.attr("data-datepicker-minDate")) {
            l.minDate = h.attr("data-datepicker-minDate")
        }
        h.datepicker(l)
    });
    $("body").on("submit", "form#alert_form", function (e) {
        e.preventDefault();
        var d = $(this);
        $.ajax({url: d.data("ajax-url"), data: d.serialize(), type: d.attr("method"), success: function (g) {
            $(".trip-alert-form-container").replaceWith(g);
            bbc.initDatePicker($("#alert_beginAt"))
        }, complete: function () {
            bbc.buttonLoader.btnEnable(d.find(".apply-btn-loader"))
        }, statusCode: {201: function f(h) {
            var g = $("#trip-alert-create-step2");
            g.modal();
            g.find("#bt-without-favoriteRoute").on("click", function () {
                $(".trip-alert-form-container").addClass("alert-created")
            })
        }}})
    })
});
profileCar = (function (f) {
    var e = {};

    function d() {
        f("form.form-upload-auto-submit input[type=file]").bind("change", function () {
            f(this).parents("form").submit()
        })
    }

    function c() {
        var h = f("#profile_car_make").val();
        var i = f("#profile_car_make").data("car-model-url");
        if (h.length > 0) {
            i = i.replace("__MAKE__", h);
            f.getJSON(i, {ajax: "true"}, function (l) {
                var k = "";
                for (var m = 0; m < l.length; m++) {
                    k += '<option value="' + l[m] + '">' + l[m] + "</option>"
                }
                f("#profile_car_model").html(k)
            })
        } else {
            f("#profile_car_model").html("")
        }
    }

    function g() {
        if (f(this).val() == f(this).find("option").first().val()) {
            f(this).addClass("default")
        } else {
            f(this).removeClass("default")
        }
    }

    f(function () {
        f(".select-car").on("change", g);
        f(".select-car").trigger("change")
    });
    function b() {
        f("#profile_car_make").on("change", c)
    }

    function a() {
    }

    return{init: function () {
        b()
    }, initPicture: function () {
        a()
    }}
})(jQuery);
$(document).ready(function () {
    initPreferenceFocus();
    initForm();
    $(".phone-certification-modal").each(function () {
        $(this).dialog({autoOpen: true, dialogClass: "phone-certification-dialog", modal: true})
    })
});
function activateLi(a) {
    var d = a.childNodes;
    for (var b = 0; b < d.length; b++) {
        if (d[b].nodeName == "INPUT") {
            if (d[b].getAttribute("checked") != "checked") {
                d[b].setAttribute("checked", "checked");
                d[b].checked = true
            }
        } else {
            if (d[b].nodeName == "SPAN") {
                if (!d[b].className.match("active")) {
                    var c = d[b].getAttribute("class");
                    c += "active";
                    d[b].setAttribute("class", c)
                }
            }
        }
    }
}
function deactivateLi(a) {
    var d = a.childNodes;
    for (var b = 0; b < d.length; b++) {
        if (d[b].nodeName == "INPUT") {
            if (d[b].getAttribute("checked") != null) {
                d[b].removeAttribute("checked");
                d[b].checked = false
            }
        } else {
            if (d[b].nodeName == "SPAN") {
                if (d[b].className.match("active")) {
                    var c = d[b].getAttribute("class");
                    c = c.replace("active", "");
                    d[b].setAttribute("class", c)
                }
            }
        }
    }
}
$(function () {
    $(".profile-pref-container ul li").click(function (d) {
        if (d.target.parentNode.className.match("pref-label")) {
            return false
        }
        if (d.target.parentNode.nodeName == "LI") {
            var b = d.target.parentNode;
            var a = d.target.parentNode.parentNode.childNodes;
            for (var c = 0; c < a.length; c++) {
                if (a[c] == b) {
                    activateLi(a[c])
                } else {
                    deactivateLi(a[c])
                }
            }
        }
    })
});
function initPreferenceFocus() {
    var b = $(".profile-pref-container ul li :input");
    for (var a = 0; a < b.length; a++) {
        if (b[a].checked == true) {
            activateLi(b[a].parentNode)
        }
    }
}
function initForm() {
};
myTrip = (function (d) {
    var b = new Array();

    function a() {
        d(".input-number").each(function () {
            var l = d(this);
            var h = l.find(".btn-plus");
            var k = l.find(".btn-minus");
            var g = l.find("p.seats-available > span");
            var f = function (o) {
                var q = parseInt(g.text());
                var p = l.data("number-min");
                var r = l.data("number-max");
                q += o;
                if (typeof r !== "undefined" && r !== false) {
                    q = Math.min(q, parseInt(r))
                }
                if (typeof p !== "undefined" && p !== false) {
                    q = Math.max(q, parseInt(p))
                }
                b.push(q);
                console.log(b);
                g.trigger("onNumberChanged", q);
                var n = l.data("update-url");
                console.log(n);
                d.ajax(n, {type: "PUT", data: {seat: q}, statusCode: {404: function () {
                    alert("page not found")
                }}, success: function (s) {
                    g.html(q);
                    j(o)
                }})
            };
            var j = function (p) {
                var n = l.parent().find("ul.list-seats-available");
                if (p > 0) {
                    for (var o = 0; o < p; o++) {
                        n.append('<li class="empty"></li>')
                    }
                } else {
                    for (p; p < 0; p++) {
                        n.find("li.empty").last().remove()
                    }
                }
            };
            var m = function () {
                f(1)
            };
            var i = function () {
                f(-1)
            };
            h.bind("click", m);
            k.bind("click", i)
        })
    }

    function e() {
        d(".price-stages-list input").each(function () {
            d(this).bind("onNumberChanged", c)
        })
    }

    function c() {
        var f = 0;
        d(".price-stages-list input").each(function () {
            f += parseInt(d(this).val());
            d("#total-price").html(f)
        })
    }

    return{init: function () {
        c();
        a();
        e()
    }}
})(jQuery);
$(".publish-return").on("click", function showReturnTrip() {
    var c = $(this).attr("data-id");
    var a = "return-" + c;
    var b = "duplicate-" + c;
    $(this).toggleClass("active");
    $(".duplicate-my-trip").removeClass("active");
    $("#" + a).toggle();
    $("#" + b).hide()
});
$(".duplicate-my-trip").on("click", function showDuplicateTrip() {
    var c = $(this).attr("data-id");
    var a = "return-" + c;
    var b = "duplicate-" + c;
    $(this).toggleClass("active");
    $(".publish-return").removeClass("active");
    $("#" + b).toggle();
    $("#" + a).hide()
});
$(".btn-close").on("click", function hideReturnTrip() {
    $(this).parents("li.yellow-box").hide()
});
$(document).ready(function () {
    $("span#phoneCode").html($("option:selected", "select#registration_phoneInformations_country").attr("title"));
    var a = $("option:selected", "select#registration_phoneInformations_country").attr("data-example");
    $("input#registration_phoneInformations_input").attr("placeholder", a);
    $("select#registration_phoneInformations_country").change(function () {
        $("span#phoneCode").html($("option:selected", this).attr("title"));
        var c = $("option:selected", this).attr("data-example");
        $("input#registration_phoneInformations_input").attr("placeholder", c)
    });
    function b() {
        if ($(this).val() == $(this).find("option").first().val()) {
            $(this).addClass("default")
        } else {
            $(this).removeClass("default")
        }
    }

    $(function () {
        $(".select-register").on("change", b);
        $(".select-register").trigger("change")
    })
});
var onPhoneCountryChanged = function (c) {
    var b = c.find("span.phone-code:first");
    var e = c.find("input.phone_raw_input:first");
    var d = c.find("select.phone_country:first");
    var f = d.find("option:selected");
    var g = f.attr("title");
    var a = f.attr("data-example");
    b.html(g);
    e.attr("placeholder", a)
};
$(".mobile-phone").each(function () {
    var a = $(this);
    var c = a.find("input.phone_raw_input:first");
    if ("" === c.val()) {
        var b = a.find("select.phone_country:first");
        b.val(BLABLACAR_REGION);
        onPhoneCountryChanged(a)
    }
});
$("body").on("change", "select.phone_country", function (c) {
    c.preventDefault();
    var b = $(this);
    var a = b.parents("form:first");
    onPhoneCountryChanged(a)
});
$(document).ready(function () {
    $select = $("select.phone_country");
    if ($select.length === 0) {
        return
    }
    var a = $select.parents("form:first");
    onPhoneCountryChanged(a)
});
analyticsVirtualUrl = (function (d) {
    function c() {
        d("[data-ga-trackPageview]").on("click", function () {
            _gaq.push(["_trackPageview", d(this).data("ga-trackpageview")])
        })
    }

    function b() {
        d("[data-ga-event-category]").on("click", function () {
            var e = new Array();
            e[0] = "_trackEvent";
            e[1] = d(this).data("ga-event-category");
            e[2] = d(this).data("ga-event-action");
            if (typeof d(this).data("ga-event-label") !== "undefined") {
                e[3] = d(this).data("ga-event-label")
            }
            if (typeof d(this).data("ga-event-value") !== "undefined") {
                e[4] = d(this).data("ga-event-value")
            }
            if (typeof d(this).data("ga-event-count") !== "undefined") {
                e[5] = d(this).data("ga-event-count")
            }
            _gaq.push(e)
        })
    }

    function a() {
        c();
        b()
    }

    return{init: function () {
        a()
    }}
})(jQuery);
$(document).ready(function () {
    analyticsVirtualUrl.init()
});
$(document).ready(function () {
    $("[data-show-modal]").click(function (c) {
        c.preventDefault();
        var a = $(this);
        var b = $("#" + a.attr("data-show-modal"));
        if (a.data("modal-body")) {
            b.find(".modal-body > p:first").html(a.data("modal-body"))
        }
        $("div.modal-footer a.btn-primary, div.modal-footer a.btn-validation", b).attr("href", $(this).attr("data-url"));
        $("form", b).attr("action", $(this).attr("data-url"));
        b.modal()
    })
});
$(document).ready(function () {
    if ($('input[name="rating[role]"]').length) {
        var a = $('input[name="rating[role]"]:checked').val();
        if (a == "D") {
            if ($(".main-elements-container").is(":hidden")) {
                $(".main-elements-container").show()
            }
            $(".comment-rating-container").show();
            $(".driver-behavior-container").show()
        } else {
            if ((a == "P") || (a == "X")) {
                $(".driver-behavior-container").hide();
                $(".comment-rating-container").show();
                $(".main-elements-container").show()
            }
        }
    }
});
$(".review-answer").bind("click", function showAnwser() {
    var a = this.getAttribute("data-num");
    $("." + a).toggle()
});
$(".review-answer-close").bind("click", function hideAnswer() {
    var a = this.getAttribute("data-num");
    $("." + a).toggle()
});
if ($(".answer-text").length) {
    $(".answer-text").limit("200", ".chars-left")
}
if ($(".rating-comment-trip").length) {
    $(".rating-comment-trip").limit("200", ".chars-left")
}
jQuery.fn.resetForm = function () {
    $(this).each(function () {
        this.reset()
    })
};
$('input[name="rating[role]"]').on("click", function showPanel() {
    if ($(".main-elements-container").is(":hidden")) {
        $(".main-elements-container").show()
    }
    $(".drv-evaluation").attr("checked", false);
    $(".radio-drv-eval").each(function (a) {
        $(this).removeAttr("disabled");
        $(".radio-drv-eval").parent().removeClass("disabled");
        $(this).attr("checked", false)
    })
});
$(".show-rating").on("click", function hidePanel() {
    if ($(".driver-behavior-container").is(":hidden")) {
        $(".driver-behavior-container").show();
        $(".rating-comment-trip").removeClass("full-width")
    }
});
$(".hide-rating").on("click", function hidePanel() {
    $(".driver-behavior-container").hide();
    $(".rating-comment-trip").addClass("full-width")
});
if ($(".drv-evaluation").length) {
    $(".drv-evaluation").on("click", function toggleDisabled() {
        $(".radio-drv-eval").parent().toggleClass("disabled");
        $(".radio-drv-eval").each(function (a) {
            if ($(this).attr("disabled")) {
                $(this).removeAttr("disabled")
            } else {
                $(this).attr("disabled", "disabled")
            }
            if ($(this).attr("checked", true)) {
                $(this).removeAttr("checked")
            }
        })
    })
}
$(".rateit").bind("over", function (a, b) {
    if (b === null) {
    } else {
        $(".num-star").html(b)
    }
});
$(".rateit").mouseleave(function () {
    var a = $(this).attr("data-rateit-fullname");
    $(".num-star").html($('select[name="' + a + '"]').val())
});
$(".rateit").bind("rated", function (b, c) {
    var a = $(this).attr("data-rateit-fullname");
    $(".num-star").html(c);
    $('select[name="' + a + '"]').val(c)
});
var bbc = bbc || {};
bbc.buttonLoader = (function () {
    var a = {}, b = {};
    a.init = function () {
        $("body").on("click", "button.btn-validation, a.btn-validation, .apply-btn-loader", function () {
            var c = $(this);
            c.prepend('<img src="/images/ajax-loader-blue.gif" style="margin-right:5px; display:none" />');
            c.parents("form").on("submit", function (d) {
                if (c.prop("disabled") === false) {
                    b.btnDisable(c);
                    return true
                }
                d.preventDefault();
                var e = setTimeout(function () {
                    b.btnEnable(c);
                    return true
                }, 4000)
            })
        })
    };
    b.btnDisable = function (c) {
        c.find("img").show();
        c.addClass("disabled");
        c.attr("disabled", "disabled")
    };
    b.btnEnable = function (c) {
        c.find("img").hide();
        c.removeClass("disabled");
        c.removeAttr("disabled")
    };
    a.init();
    return b
}());
var Social = {offerRide: function (a, b) {
    SocialAjax.offerRide(a, b)
}, giveRating: function (b, a) {
}, receiveRating: function (b, a) {
}};
var SocialMenu = {selector: ".social-lnk a", tempStatus: "", tempClass: "", classEnabled: "dot green", classDisabled: "dot gray", classLoading: "loading", init: function () {
    if ($(SocialMenu.selector).length == 0) {
        return
    }
    SocialMenu.bindEvents()
}, bindEvents: function () {
    $(SocialMenu.selector).on("click", function (a) {
        SocialMenu.store($(this));
        SocialMenu.updateSender($(this), "load");
        Social.offerRide($(this), SocialMenu.getAction($(this)));
        a.stopPropagation();
        a.preventDefault()
    })
}, updateSender: function (d, c) {
    if (d == null || c == "") {
        return
    }
    var b = $(d);
    var a = $(d).attr("data-status");
    b.removeClass();
    switch (c.toLowerCase()) {
        case"enable":
            b.addClass(SocialMenu.classEnabled);
            b.attr("data-status", "enabled");
            break;
        case"disable":
            b.addClass(SocialMenu.classDisabled);
            b.attr("data-status", "disabled");
            break;
        case"fail":
            SocialMenu.restore(b);
            break;
        case"load":
            b.addClass(SocialMenu.classLoading);
            if (a.indexOf("-loading") < 0) {
                b.attr("data-status", a + ("-loading"))
            }
            break
    }
}, lnk_ClickedEventHandler: function (a, b) {
    b = b.toLowerCase();
    if (b == null || b == "failed") {
        return SocialMenu.updateSender(a, "fail")
    }
    if (b == "enabled") {
        return SocialMenu.updateSender(a, "enable")
    }
    SocialMenu.updateSender(a, "disable")
}, getAction: function (b) {
    var a = $(b).attr("data-status").toLowerCase();
    if (a == null || a == "") {
        a = "enabled"
    }
    if (a.indexOf("enabled") > -1) {
        return"disable"
    } else {
        return"enable"
    }
}, store: function (a) {
    SocialMenu.clearStore();
    SocialMenu.tempStatus = $(a).attr("data-status");
    SocialMenu.tempClass = $(a).attr("class")
}, restore: function (a) {
    $(a).attr("data-status", SocialMenu.tempStatus);
    $(a).attr("class", SocialMenu.tempClass)
}, clearStore: function () {
    SocialMenu.tempStatus = "";
    SocialMenu.tempClass = ""
}};
var SocialAjax = {offerRide: function (b, e) {
    if ($(b).length == 0) {
        return
    }
    var a = "";
    switch (e.toLowerCase()) {
        case"enable":
            a = $(b).attr("data-url-on");
            break;
        case"disable":
            a = $(b).attr("data-url-off");
            break
    }
    function d(f) {
        SocialMenu.lnk_ClickedEventHandler(b, JSON.parse(f.responseText))
    }

    var c = $.ajax({url: a, dataType: "text", type: "POST", data: {}, complete: d})
}};
var bbc = bbc || {};
bbc.regularTrip = (function () {
    var a = {dates: {simple: {}, round: {}}}, b = {};
    a.addRemoveDateRange = function (e, h, j) {
        var f = new Date(a.widgetDateStart.datepicker("getDate").getTime()), d = new Date(f.getTime()), k = f.getDay(), c = new Date(a.widgetDateStop.datepicker("getDate").getTime()), g;
        c.setDate(c.getDate());
        if (k <= j) {
            g = j - k
        } else {
            g = j + 7 - k
        }
        d.setDate(f.getDate() + g);
        while (d <= c) {
            a.addRemoveDate(e, h, new Date(d.getTime()));
            d.setDate(d.getDate() + 7)
        }
        var i = {1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 0: 6};
        if (h === "simple") {
            $("select.regular-way-in-days option[value=" + i[j] + "]").attr("selected", e == "add")
        } else {
            if (h === "round") {
                $("select.regular-return-days option[value=" + i[j] + "]").attr("selected", e == "add")
            }
        }
    };
    a.hasDate = function (c, e) {
        var d = (c === "simple") ? a.dates.simple : a.dates.round;
        return d[e] ? true : false
    };
    a.getDateKey = function (c) {
        return $.datepicker.formatDate("yymmdd", c)
    };
    a.getReverseDateKey = function (c) {
        return new Date(c.substr(0, 4), parseInt(c.substr(4, 2), 10) - 1, c.substr(6, 2))
    };
    a.addDateToForm = function (e, f) {
        if ($("#" + e + " input[value=" + f + "]").length > 0) {
            return
        }
        var c = $("#" + e).data("prototype");
        var d = c.replace(/__name__/g, f);
        $("#" + e).append(d);
        $("#" + e + "_" + f).attr("value", f)
    };
    a.addRemoveDate = function (f, e, d) {
        var g = a.getDateKey(d);
        var c = {simple: $(".regular-way-in").attr("id"), round: $(".regular-return").attr("id")};
        if (f === "add") {
            a.dates[e][g] = d;
            a.addDateToForm(c[e], g)
        } else {
            delete a.dates[e][g];
            $("#" + c[e] + " input[value=" + g + "]").remove()
        }
    };
    a.load = function () {
        a.widgetDays = {simple: $(".simple button"), round: $(".round button")};
        a.widgetCalendar = $("#regular-calendar");
        a.widgetChooseDate = $("#chooseDate");
        a.widgetDateStart = $("input.start-schedule:first");
        a.widgetDateStop = $("input.stop-schedule:first");
        a.widgetRoundTrip = $("input.simple-round-choice:first");
        a.widgetCalendarAll = $("#all-calendars");
        a.widgetAllModal = $("#seeAll");
        a.isEdition = $("#is_edition") && $("#is_edition").val() == 1;
        a.hasErrors = $("#has_errors") && $("#has_errors").val() == 1;
        a.displayDaysAndDates()
    };
    a.selectDay = function (c) {
        c.addClass("selected-day").addClass("active")
    };
    a.preSelectDays = function () {
        if (true == a.widgetRoundTrip.is(":checked")) {
            if ($(".simple button[class*=selected-day]").length == 0) {
                $(".simple button[data-day-range=5]").click()
            }
            if ($(".round button[class*=selected-day]").length == 0) {
                $(".round button[data-day-range=0]").click()
            }
        } else {
            if (false == a.isEdition && false == a.hasErrors) {
                a.uncheckDays("simple")
            }
        }
    };
    a.uncheckDays = function (c) {
        $.each(a.dates[c], function (e, d) {
            a.addRemoveDate("remove", c, a.getReverseDateKey(e))
        });
        $.each(a.widgetDays[c], function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("selected-day").removeClass("active")
            }
        });
        if (c == "simple") {
            $("select.regular-way-in-days option:selected").attr("selected", false)
        } else {
            $("select.regular-return-days option:selected").attr("selected", false)
        }
        a.widgetCalendar.datepicker("refresh")
    };
    a.displayDaysAndDates = function () {
        var c = {0: 1, 1: 2, 2: 3, 3: 4, 4: 5, 5: 6, 6: 0};
        $(".regular-way-in-days option:selected").each(function () {
            a.selectDay($(".simple button[data-day-range=" + c[$(this).val()] + "]"))
        });
        $(".regular-return-days option:selected").each(function () {
            a.selectDay($(".round button[data-day-range=" + c[$(this).val()] + "]"))
        });
        $(".regular-way-in input").each(function () {
            var d = a.getReverseDateKey($(this).val());
            a.addRemoveDate("add", "simple", d)
        });
        $(".regular-return input").each(function () {
            var d = a.getReverseDateKey($(this).val());
            a.addRemoveDate("add", "round", d)
        })
    };
    a.beforeShowDay = function (d) {
        var e = a.getDateKey(d), c = "";
        if (a.hasDate("simple", e)) {
            c += "simple-date-selected"
        }
        if (a.hasDate("round", e)) {
            c += " round-date-selected"
        }
        return[true, c]
    };
    a.updateDay = function (e, d) {
        var c = parseInt(d.attr("data-day-range"), 10);
        a.addRemoveDateRange(false === d.hasClass("selected-day") ? "remove" : "add", e, c)
    };
    a.toggleDay = function (d, c) {
        d.toggleClass("selected-day");
        a.updateDay(c, d);
        a.widgetCalendar.datepicker("refresh")
    };
    a.updateDays = function () {
        $.each(a.widgetDays.simple, function () {
            a.updateDay("simple", $(this))
        });
        $.each(a.widgetDays.round, function () {
            a.updateDay("round", $(this))
        })
    };
    a.arrayToObject = function (e) {
        for (var c = {}, d = 0; d < e.length; d++) {
            c[e[d]] = e[d]
        }
        return c
    };
    a.dateSqlToJs = function (c) {
        return new Date(parseInt(c.substr(0, 4), 10), parseInt(c.substr(5, 2), 10) - 1, parseInt(c.substr(8, 2), 10))
    };
    a.updateDateStart = function () {
        var c = a.widgetDateStart.datepicker("getDate");
        a.widgetCalendar.datepicker("option", "minDate", c);
        a.widgetCalendar.datepicker("option", "setDate", c);
        a.widgetCalendarAll.datepicker("option", "minDate", c);
        if (a.dateStartPrevious < c) {
            while (a.dateStartPrevious < c) {
                a.addRemoveDate("remove", "simple", a.dateStartPrevious);
                a.addRemoveDate("remove", "round", a.dateStartPrevious);
                a.dateStartPrevious.setDate(a.dateStartPrevious.getDate() + 1)
            }
        } else {
            a.dateStartPrevious = new Date(c.getTime())
        }
        a.updateDays();
        a.widgetCalendar.datepicker("refresh")
    };
    a.updateDateStop = function () {
        var c = a.widgetDateStop.datepicker("getDate");
        a.widgetCalendar.datepicker("option", "maxDate", c);
        a.widgetCalendarAll.datepicker("option", "maxDate", c);
        if (a.dateStopPrevious > c) {
            while (a.dateStopPrevious > c) {
                a.addRemoveDate("remove", "simple", a.dateStopPrevious);
                a.addRemoveDate("remove", "round", a.dateStopPrevious);
                a.dateStopPrevious.setDate(a.dateStopPrevious.getDate() - 1)
            }
        } else {
            a.dateStopPrevious = new Date(c.getTime())
        }
        a.updateDays();
        a.widgetCalendar.datepicker("refresh")
    };
    a.checkDateMax = function () {
        $(".stop-schedule").datepicker({onClose: function () {
            var e = $(this), g = new Date($(".start-schedule").datepicker("getDate")), h = new Date(e.datepicker("getDate")), f = new Date(g);
            f = new Date(f.setMonth(f.getMonth() + parseInt($(this).attr("data-date-max-end"))));
            if (h > f) {
                e.val($.datepicker.formatDate($.datepicker._defaults.dateFormat, f));
                e.tooltip({title: e.attr("data-date-too-far"), animation: true, trigger: "manual", placement: "top"}).tooltip("show");
                var d = setTimeout(function c() {
                    e.tooltip("destroy")
                }, 10000)
            }
        }})
    };
    b.init = function () {
        a.load();
        var c = {selectOtherMonths: true, showAnim: "", dateFormat: a.widgetCalendar.attr("data-date-format"), firstDay: "1"};
        a.widgetDateStart.datepicker(c);
        a.widgetDateStart.datepicker("setDate", a.widgetDateStart.val());
        a.widgetDateStart.datepicker("option", "onSelect", a.updateDateStart);
        a.widgetDateStart.on("change", a.updateDateStart);
        a.widgetDateStop.datepicker(c);
        a.widgetDateStop.datepicker("setDate", a.widgetDateStop.val());
        a.widgetDateStop.datepicker("option", "onSelect", a.updateDateStop);
        a.widgetDateStop.on("change", a.updateDateStop);
        if (a.widgetDateStart.length) {
            a.dateStartPrevious = new Date(a.widgetDateStart.datepicker("getDate").getTime());
            a.dateStopPrevious = new Date(a.widgetDateStop.datepicker("getDate").getTime())
        }



        // datepicker created
        a.widgetCalendar.datepicker(jQuery.extend({}, c, {
            defaultDate: a.widgetDateStart.datepicker("getDate"),
            minDate: a.widgetDateStart.datepicker("getDate"),
            maxDate: a.widgetDateStop.datepicker("getDate"),
            beforeShowDay: a.beforeShowDay,
            onSelect: function () {
                var e = a.getDateKey(a.widgetCalendar.datepicker("getDate"));
                a.widgetChooseDate.attr("data-day", e);
                if (a.widgetDays.round.is(":visible")) {
                    a.widgetChooseDate.modal("show");
                    if (a.hasDate("simple", e)) {
                        $("input[name=simple-choice]").attr("checked", "checked");
                        $(".simple-choice-label").addClass("green-label")
                    }
                    if (a.hasDate("round", e)) {
                        $("input[name=round-choice]").attr("checked", "checked");
                        $(".round-choice-label").addClass("blue-label")
                    }
                } else {
                    var d = a.getReverseDateKey(a.widgetChooseDate.attr("data-day"));
                    if (a.hasDate("simple", a.widgetChooseDate.attr("data-day"))) {
                        a.addRemoveDate("remove", "simple", d)
                    } else {
                        a.addRemoveDate("add", "simple", d)
                    }
                }
            }
        }));

        $(".see-all-dashboard").on("click", function () {
            var j = $(this);
            var d = j.parents("article");
            var k = d.find(".all-calendars-dashboard");
            if (!k.hasClass("hasDatepicker")) {
                var e = d.find(".startDateCal").val();
                var f = d.find(".endDateCal").val();
                var l = a.dateSqlToJs(e);
                var i = a.dateSqlToJs(f);
                var g = d.find(".dateSimpleDashboard").val().split(",");
                var h = d.find(".dateRoundDashboard").val().split(",");
                k.hide();
                a.dates.simple = a.arrayToObject(g);
                a.dates.round = a.arrayToObject(h);
                k.datepicker(jQuery.extend({}, c, {defaultDate: l, minDate: l, maxDate: i, numberOfMonths: [2, 3], beforeShowDay: a.beforeShowDay}))
            }
            j.text(k.is(":visible") ? j.data("origin") : j.data("alt"));
            k.slideToggle()
        });
        $('a[href="#seeAll"]').on("click", function () {
            a.widgetAllModal.modal("show");
            a.widgetCalendarAll.datepicker(jQuery.extend({}, c, {numberOfMonths: [2, 3], beforeShowDay: a.beforeShowDay, minDate: a.widgetDateStart.datepicker("getDate"), maxDate: a.widgetDateStop.datepicker("getDate")}));
            a.widgetCalendarAll.datepicker("refresh")
        });
        a.widgetRoundTrip.on("change", function () {
            a.preSelectDays();
            if (false == a.widgetRoundTrip.is(":checked")) {
                a.uncheckDays("round")
            }
        });
        a.widgetDays.simple.click(function () {
            a.toggleDay($(this), "simple")
        });
        a.widgetDays.round.click(function () {
            a.toggleDay($(this), "round")
        });
        a.preSelectDays();
        a.widgetChooseDate.find("button.btn-validation").click(function () {
            var f = a.getReverseDateKey(a.widgetChooseDate.attr("data-day")), h = a.widgetChooseDate.find("input[name=simple-choice]"), d = a.widgetChooseDate.find("input[name=round-choice]");
            var e = h.is(":checked") ? "add" : "remove";
            a.addRemoveDate(e, "simple", f);
            var g = d.is(":checked") ? "add" : "remove";
            a.addRemoveDate(g, "round", f);
            a.beforeShowDay(f);
            a.widgetCalendar.datepicker("refresh");
            h.attr("checked", false);
            $(".simple-choice-label").removeClass("green-label");
            d.attr("checked", false);
            $(".round-choice-label").removeClass("blue-label")
        })
    };
    a.checkDateMax();
    return b
}());
var showmore = function () {
    $(".showmore .showmore-link").on("click", function (e) {
        e.preventDefault();
        var d = $(this), a = d.parent(), f = a.find(".showmore-ellipsis"), c = a.find(".showmore-rest"), b = "deployed";
        if (a.hasClass(b)) {
            a.removeClass(b);
            f.show();
            c.hide("1000");
            d.text(d.attr("data-label-show"))
        } else {
            a.addClass(b);
            f.hide();
            c.show("1000");
            d.text(d.attr("data-label-hide"))
        }
    })
};
window.payment = (function (b, a, c) {
    return{init: function () {
        var d = b("#driver-confirmation");
        var i = b(".slider").slider({min: 1, max: b("#driver-confirmation option").length, range: "min", value: d[0].selectedIndex + 1, slide: function (j, k) {
            d[0].selectedIndex = k.value - 1;
            b(".slider-result").html(d.val())
        }});
        b("#driver-confirmation").change(function () {
            i.slider("value", this.selectedIndex + 1)
        });
        var g = b(".slider-label.min").width();
        var h = b(".slider-label.max").width();
        var f = Math.round(g / 2);
        var e = Math.round(h / 2);
        b(".slider-label.min").css("margin-left", -f + 3);
        b(".slider-label.max").css("margin-right", -e - 3)
    }}
})(jQuery, window);
$("#payment-solutions a").click(function (a) {
    a.preventDefault();
    $(this).tab("show")
});
window.postpayment = (function (b, a, c) {
    return{init: function () {
        b(function d() {
            var f = b(".postpayment-container .comment-trip");
            var h = 200;
            var e = f.html();
            var g = e.substr(0, h);
            e = g + '<div class="hide hidden-text">' + e.substr(h, e.length) + "</div>";
            f.html(e)
        });
        b(".show-hidden-text").bind("click", function () {
            var e = this.getAttribute("data-origin");
            var f = this.getAttribute("data-alt");
            if (b(".hidden-text").is(":visible")) {
                b(".show-hidden-text").html(e)
            } else {
                b(".show-hidden-text").html(f)
            }
            b(".comment-trip .hidden-text").slideToggle()
        })
    }}
})(jQuery, window);
$(".sample-creditcard").bind("click", function (c) {
    c.preventDefault();
    if ($(this).hasClass("sample-creditcard-mastercard")) {
        var b = "5105105105105100";
        var a = "9"
    } else {
        if ($(this).hasClass("sample-creditcard-maestro")) {
            var b = "5018820677196842";
            var a = "8"
        } else {
            if ($(this).hasClass("sample-creditcard-visa")) {
                var b = "4012888888881881";
                var a = "10"
            } else {
                if ($(this).hasClass("sample-creditcard-visa-3dsecure")) {
                    var b = "4562000000000200";
                    var a = "10"
                }
            }
        }
    }
    var d = new Date();
    d.setFullYear(d.getFullYear() + 1);
    $("#creditcard_number").attr("value", b);
    $("#creditcard_type").val(a);
    $("#creditcard_cvv").attr("value", "123");
    $("#creditcard_expiration_date_month").val(1);
    $("#creditcard_expiration_date_year").val(d.getFullYear());
    return false
});
$(".step-warning").popover();
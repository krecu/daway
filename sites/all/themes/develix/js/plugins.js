function implode( glue, pieces ) {
    return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}

function in_array(needle, haystack, strict) {
    var found = false, key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}

function empty( mixed_var ) {
    return ( mixed_var === "" || mixed_var === 0   || mixed_var === "0" || mixed_var === null  || mixed_var === false  ||  ( is_array(mixed_var) && mixed_var.length === 0 ) );
}

function is_array( mixed_var ) {
    return ( mixed_var instanceof Array );
}

function str_replace ( search, replace, subject ) {
    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }
    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){
        replace[replace.length]='';
    }

    if(subject instanceof Array){
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }

    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }

    return subject;

}

(function ($) {
    $.fn.resizePhoto = function(width, height){
        var real_img = $(this);
        var img = new Image();
        img.onload = function() {
            if (img.width > width) {
                real_width = img.width;
                real_height = img.height;
                img.width = width;
                img.height = (width * real_height)/real_width;
            }

            if (img.height > height){
                real_width = img.width;
                real_height = img.height;
                img.height = height;
                img.width = (height * real_width)/real_height;
            }
            real_img.attr('width', img.width);
            real_img.attr('height', img.height);

            $(window).trigger('resize');
        }

        img.src = real_img.attr('src');
    }
})(jQuery);
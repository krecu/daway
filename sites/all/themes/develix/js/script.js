(function ($, Drupal, window, document, undefined) {

  var DAWAY = {};

  DAWAY.InterCity = function () {
    var self = this;
    this.conteiner = null;
    this.countries = null;
    this.init = function () {

      self.conteiner = $('#intercity');
      self.countries = $('ul.countries', self.conteiner);

      self.countries.bxSlider({
        minSlides: 1,
        maxSlides: 5,
        slideWidth: 149,
        pager: false
      });
    };

    this.init();
  }

  DAWAY.ToolTip = function () {
    this.init = function () {

      $('.qtip').remove();
      $('*').each(function () {
        if ($(this).data('tooltip')) {
          var $el = $(this);

          $el.qtip({
            content: $(this).data('tooltip'),
            style: {
              tip: {
                size: { x: 20, y: 8 },
                corner: 'bottomMiddle'
              },
              background: '#018181',
              color: '#fff',
              textAlign: 'center',
              border: {
                width: 1,
                radius: 2,
                color: '#018181'
              }
            },
            show: { delay: 50 },
            position: {
              corner: {
                target: 'topMiddle',
                tooltip: 'center'
              }
            }
          });
        }
      })
    };

    this.init();
  }

  DAWAY.FlagCallback = function () {
    this.init = function () {
      $(document).bind('flagGlobalAfterLinkUpdate', function (event, data) {
        $.get('/js/subscribe/getCountBookmark', null, function (data) {
          $('#user-menu .favorite .middle').text(data);
        });
      });
    };

    this.init();
  }

  DAWAY.SuperMenu = function () {
    var self = this;
    var opt = $.extend({ fx: "easeOutCubic", speed: 500, click: function () {
    } }, opt || {});
    this.container = null;
    this.menuconteiner = null;
    this.lavalamp = $('<li class="back"><div class="left"></div></li>');
    this.resizeitems = $('<li class="virtual expanded"><a class="more" href="">more</a><ul class="menu parent"></ul></li>');
    this.smilecar = $('<div id="smile-car"></div>');
    this.lavaselected = null;
    this.virtualitems = [];
    this.realitems = [];

    this.init = function () {
      setTimeout(
        function () {
          self.container = $('#main-menu');
          self.menuconteiner = $('.block-content > ul.menu', self.container);
          self.LavaLampInit();
          self.ResizeMenuInit();
          self.ShowMenu();
          self.ShowSmile();
        }, 500
      );
    };

    this.LavaLampMove = function ($el) {
      var offset = $el.position();
      self.lavalamp.dequeue();
      self.lavalamp.stop().animate({
        top: offset.top,
        height: $el.outerHeight() - 1,
        width: 112
      }, opt.speed, opt.fx);
    };

    this.LavaLampMoveMore = function ($el) {
      var offset = $el.position();
      self.lavalamp.dequeue();
      self.lavalamp.stop().animate({
          top: offset.top,
          height: $el.outerHeight() - 1
        },
        200,
        opt.fx,
        function () {
          self.lavalamp.animate({
            width: $el.find('ul').outerWidth() + 100
          }, 100, function () {
            $('ul', $el).show();
            $('ul', $el).css('height', $el.outerHeight());
          });
        }
      );


    };

    this.ShowSmile = function () {
      self.container.append(self.smilecar);
    }

    this.ShowMenu = function () {
      if (!$.cookie('SuperMenuInit')) {
        self.container.css('top', -self.container.outerHeight());
        self.container.animate({
          top: '42'
        }, develix_supermenu_speed, 'easeOutBounce');
      } else {
        self.container.css('top', '42px');
      }
      $.cookie('SuperMenuInit', true);
    };

    this.LavaLampHover = function () {
      $('li ul', self.menuconteiner).hide();
      $('li.hover', self.menuconteiner).removeClass('hover');
      self.LavaLampMove(self.lavaselected);
      self.lavaselected.addClass('hover');

      $('li.parent, li.virtual', self.menuconteiner).hover(
        function () {
          if ($(this).hasClass('hover')) {

          } else {
            $('li ul', self.menuconteiner).hide();
            $('li', self.menuconteiner).removeClass('hover');
            $(this).addClass('hover');
            $('.pac-container').hide();
          }

          if ($(this).hasClass('expanded')) {
            self.LavaLampMoveMore($(this));
          } else {
            self.LavaLampMove($(this));
          }
        }, function () {
//          if ($('.pac-container').is(":visible")) {
//
//          } else {
//            $('li ul', self.menuconteiner).hide();
//            $('li.hover', self.menuconteiner).removeClass('hover');
//            self.LavaLampMove(self.lavaselected);
//            self.lavaselected.addClass('hover');
//            $('.pac-container').hide();
//          }
        }
      );

    };

    this.LavaLampInit = function () {
      // определяем активный элемент
      if ($('a.active', self.menuconteiner).length && ($('a.active', self.menuconteiner).attr('href') != '/' + Drupal.settings.currentlang)) {
        self.lavaselected = $('a.active', self.menuconteiner).parent('li');
      } else {
        self.lavaselected = $('li.first', self.menuconteiner);
        $('li.first a', self.menuconteiner).addClass('active-trail');
      }

      self.lavaselected.addClass('hover');

      // переносим LAVA к активному элементу
      self.menuconteiner.append(self.lavalamp);
      // вставляем контейнер для временных элементов
      self.menuconteiner.append(self.resizeitems);
      // пряем контейнер и снимаем события с него
      self.resizeitems.hide();
      $('a', self.resizeitems).click(function () {
        return false;
      });

      // перемещаем указатель на выбранный по умолчанию элемент
      self.LavaLampMove(self.lavaselected);
    };

    this.ResizeMenuInit = function () {
      // перебеираем все родительские элементы и строим из массив
      $('li.parent', self.menuconteiner).each(function () {
        $(this).addClass('show');
        $el = [];
        $el['index'] = $(this).data('mlid');
        $el['elem'] = $(this);
        $el['status'] = 'show';
        $el['height'] = $(this).outerHeight();
        self.realitems.push($el);
      });

      self.ResizeMenuApply();

      $(window).resize(function () {
        self.ResizeMenuApply();
      });
    }

    this.ResizeMenuApply = function () {
      $height_win = $(window).height();

      $height_items = 280;
      $items_hidden = 0;
      for (var i in self.realitems) {
        $height_items += self.realitems[i]['height'];
        if ($height_items > $height_win) {
          self.realitems[i]['status'] = 'hide';
          self.realitems[i]['elem'].hide();
          $clone = self.realitems[i]['elem'].clone();
          $clone.removeClass('parent');
          $clone.show();
          // если временный элемент не в списке то добавлем его
          if ($('li[data-mlid="' + self.realitems[i]['index'] + '"]', self.resizeitems).length == 0) {
            $('ul.parent', self.resizeitems).append($clone);
          }
          $items_hidden++;
        } else {
          $('li[data-mlid="' + self.realitems[i]['index'] + '"]', self.resizeitems).remove();
          self.realitems[i]['status'] = 'show';
          self.realitems[i]['elem'].show();

        }
      }
      if ($items_hidden) {
        self.resizeitems.show();
      } else {
        self.resizeitems.hide();
      }
      self.LavaLampHover();
    };


    this.init();
  };

  DAWAY.animationPromoRoute = function () {
    if ($('.region-front-popular-routes').length) {
      $('.region-front-popular-routes .block').hover(
        function () {
          $block = $(this);
          $block.addClass('active');
          $height = $('ul.routes', $block)[0].scrollHeight;
          if ($height > 80) {
            $('ul.routes', $block).stop().animate({
              height: $height
            }, 400);
          }

        },
        function () {
          $block = $(this);

          if ($height > 80) {
            $('ul.routes', $block).stop().animate({
              height: 80
            }, 400, function () {
              $(this).parents('.block').removeClass('active');
            });
          } else {
            $block.removeClass('active');
          }
        }
      )
    }
  }

  DAWAY.animationPromoInfo = function () {

    // Maybe carusel ???
    if ($('.region-front-promo-info').length) {
      $('.region-front-promo-info ul.text').bxSlider({
        minSlides: 2,
        maxSlides: 3,
        slideWidth: 'auto',
        slideMargin: 20,
        pager: false,
      });
    }
  }

  DAWAY.MultiForm = function () {
    var self = this;
    this.container = null;

    this.init = function () {
      self.container = $('#daway-trip-multi-form');
      self.attach();
    };
    this.attach = function () {
      $('ul.tabs a', '#daway-trip-multi-form').click(function () {
        $('ul.tabs a', '#daway-trip-multi-form').removeClass('active');
        $(this).addClass('active');
        $('.tab', '#daway-trip-multi-form').hide();
        $('.tab[data-tab="' + $(this).data('tab') + '"]', '#daway-trip-multi-form').show();
        return false;
      });
    };
    this.init();
  }

  DAWAY.Messenger = function () {
    var self = this;
    this.init = function () {
      self.container = $('#dialog-list');
      $('li.dialog', self.container).click(function () {
        window.location.href = '/user/messenger/' + $(this).data('user');
      });

      setInterval(function () {
        self.NewMessages()
      }, 1000);

    };

    this.NewMessages = function () {
      $container = $('#user-menu ul.menu a.messenger');
      $('sup', $container).animate({ backgroundColor: "#ff0000"}, 800).animate({ backgroundColor: "#c30000"}, 800);
    }

    this.init();
  }

  $.fn.ChosenUpdate = function ($field) {
    console.log('chosen update');
    $($field).trigger("liszt:updated");
  }

  $.fn.ChosenReinit = function ($field) {
    console.log('chosen reinint');
    $($field).chosen({
      disable_search: true
    });
  }


  $.fn.CustomFormItemReinit = function ($field) {
    $('input', $field).each(function () {
      var input = $(this);
      if (input.hasClass('dawayCheckbox')) {
        input.iphoneStyle({
          checkedLabel: 'YES',
          uncheckedLabel: 'NO'
        });
      }

      if (input.hasClass('form-checkbox') && !input.hasClass('dawayCheckbox') && !input.hasClass('simple')) {
        $form_item = input.parent();
        $form_item.addClass('simpleCheckbox');
        if (!$('label', $form_item).length) $('<label for="' + input.attr('id') + '"></label>').insertAfter(input);
      }

      if (input.hasClass('form-radio')) {
        input.parent().addClass('simpleRadiobox');
      }
    })
  }


  DAWAY.UserMenu = function () {
    var self = this;
    var menu = $('#user-menu');

    this.init = function () {
      self.ToolTip();
    }

    this.ToolTip = function () {
      $('ul.menu a', menu).each(function () {
        $title = $(this).attr('title');
        $(this).attr('title', '');
        if ($title) {
          $(this).qtip({
            content: $title,
            style: {
              classes: {
                tooltip: 'user-menu'
              },
              tip: {
                size: { x: 20, y: 8 },
                corner: 'topMiddle'
              },
              background: '#018181',
              color: '#fff',
              textAlign: 'center',
              border: {
                width: 1,
                radius: 2,
                color: '#018181'
              }
            },
            show: { delay: 50 },
            position: {
              corner: {
                target: 'bottomMiddle',
                tooltip: 'center'
              }
            }
          });
        }

      });
    }

    this.init();

  }

  DAWAY.FormItem = function ($form) {
    var self = this;
    var form = $form;


    this.init = function () {
      self.PreProcessed();

    };
    this.PreProcessed = function () {
      var index = 0;

      $('input', form).each(function () {
        var input = $(this);
        if (input.hasClass('tooltip-date')) {
          var now = moment();
          container = input.parents('.date');

          if (input.hasClass('time')) {
            input.datepicker({
              showOn: "both",
              buttonClass: 'date',
              showTime: true,
              dateFormat: 'dd.mm.yy',
              stepMinutes: 15,
              time24h: true,
              minDate: now.format('DD.MM.YYYY')
            });
          } else {

            input.datepicker({
              showOn: "both",
              buttonClass: 'date',
              dateFormat: 'dd.mm.yy',
              minDate: now.format()
            });
            if (container.data('default')) {
              //todo when and why???
              //input.val(container.data('default'));
            }

          }
        }

        if (input.hasClass('geopoint')) {
          input.geocomplete();
        }

        if (input.hasClass('mobile')) {
          input.intlTelInput();
        }

        if (input.hasClass('tooltip-radius')) {

          container = $('div.form-item.' + input.attr('data-radius'));
          if (container.data('default')) {
            $('input', container).val(container.data('default'));
          }

          button = $('<button type="button" class="ui-radius-trigger ' + input.attr('data-radius') + '">0 Km</button>');
          button.insertAfter(input);
          button.click(function () {
            $('div.form-item.' + input.attr('data-radius')).slideToggle();
          })
        }
        if (input.hasClass('hidden-radius')) {
          $('button.' + input.attr('name')).text(input.val() + ' Km');
          slider = $('<div data-slider="slider" class="slider">');
          label = $('<label>');
          label.text(input.attr('data-label'));
          slider.insertAfter(input);
          label.insertAfter(input);
          slider.slider({
            range: "max",
            min: 1,
            max: 150,
            value: input.val(),
            slide: function (event, ui) {
              $('button.' + input.attr('name')).text(ui.value + ' Km');
              input.val(ui.value).trigger('change');
            }
          });

          input.parent().hide();
        }

        if (input.hasClass('tooltip-price')) {

          container = input.parents('.price');
          if (!$('.slider', container).length) {
            slider = $('<div data-slider="slider" class="slider">');
            container.append(slider);

            $("#edit-price-1").val(container.data('min'))
            $("#edit-price-2").val(container.data('max'))
            slider.slider({
              range: true,
              min: container.data('min'),
              max: container.data('max'),
              values: [ $("#edit-price-1").val(), $("#edit-price-2").val() ],
              slide: function (event, ui) {
                $("#edit-price-1").val(ui.values[ 0 ]).trigger('change');
                $("#edit-price-2").val(ui.values[ 1 ]).trigger('change');
              }
            });
          }
        }

        if (input.hasClass('hidden-slider')) {

          if (input.hasClass('range')) {
            slider = $('<div data-slider="slider" class="slider">');
            label = $('<label>');
            label.html(input.attr('data-label') + ' from <span id="rate-from">0</span> to <span id="rate-to">0</span>');

            slider.insertAfter(input);
            label.insertAfter(input);
            slider.slider({
              range: true,
              min: 0,
              max: 5,
              values: [ 0, 5 ],
              slide: function (event, ui) {
                $("#rate-from").text(ui.values[ 0 ]);
                $("#rate-to").text(ui.values[ 1 ]);
                input.val(ui.values[ 0 ] + ',' + ui.values[ 1 ]).trigger('change');
              }
            });
          } else {
            slider = $('<div data-slider="slider" class="slider">');
            label = $('<label>');
            vvv = (input.val()) ? input.val() : '0';
            label.html(input.data('label') + '(<span class="val">' + vvv + '</span>)');
            slider.insertAfter(input);
            label.insertAfter(input);
            slider.slider({
              range: "max",
              min: 0,
              max: input.data('max'),
              value: input.val(),
              slide: function (event, ui) {
                input.val(ui.value).trigger('change');
                $('.val', label).text(ui.value);
              }
            });
          }

        }

        index++;
      });
    };

    this.init();
  };

  DAWAY.Search = function () {
    var self = this;
    this.listitems = null;
    this.container = null;
    this.pager = null;
    this.display = null;
    this.ajaxRequests = [];
    this.userLocation = null;
    this.subscibe = null;
    this.searchform = null;

    this.init = function () {

      setTimeout(function () {
        self.container = $('#trips-container');
        self.subscibe = $('#daway-subscribe-form');
        self.listitems = $('#trips-list');
        self.display = $('a.active', '#trips-display').attr('data-tripdisplay');
        self.searchform = $('#daway-trip-search-form');
        if ($('#paginator-container', self.container).length) {
          self.pager = $('#paginator-container', self.container);
        }


        $('#full-loader a').click(function () {
          $('#full-loader').hide();
          $('#modalBackdrop1').remove();
        });

        self.SubscribeUpdate();
        self.AttachValidation();
        self.AttachAjax();
        self.IsoTopeInit();
        self.getfilters();
      }, 1000);

    };

    this.SubscribeUpdate = function () {
      if ($('.node', self.listitems).length == 0) {
        self.subscibe.addClass('noresult');
        $(window).trigger('resize');
      } else {
        self.subscibe.removeClass('noresult');
      }
    }

    this.AttachValidation = function () {
      self.searchform.validate({
        onkeyup: true,
        errorClass: "error",
        rules: {
          price_1: {
            number: true
          },
          price_2: {
            number: true
          }
        },
        messages: {
          price_1: {
            required: 'Only number'
          }
        },
        errorPlacement: function (error, element) {
        }
      });
    }

    this.getUserPosition = function () {

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          self.userLocation = [position.coords.latitude, position.coords.longitude];
          self.AjaxExecute('sort');
        });
      }
      ;
    }

    this.AjaxExecute = function (type) {

      $url_items = self.getfilters();
      $url = implode('&', $url_items);


      $('input[name="filters"]', self.subscibe).val($url);

      for (var i = 0; i < self.ajaxRequests.length; i++) self.ajaxRequests[i].abort();

      self.ajaxRequests.push(
        $.ajax({
          xhr: function () {
            function getXmlHttp() {
              var xmlhttp;
              try {
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
              } catch (e) {
                try {
                  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (E) {
                  xmlhttp = false;
                }
              }
              if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
                xmlhttp = new XMLHttpRequest();
              }
              return xmlhttp;
            }

            var xhr = new getXmlHttp();

            xhr.addEventListener("progress", function (evt) {
              if (evt.lengthComputable) {
                var percent = (evt.loaded * 100) / evt.total;
                $('#lite-loader').animate({
                  width: percent + '%'
                }, 200);
              }
            }, false);
            return xhr;
          },
          type: 'get',
          dataType: 'json',
          url: "/search.php?" + $url,
          cache: false,
          beforeSend: function () {
            $('#lite-loader').stop().show().css('width', '1%');
            $('#full-loader').show();
            if (!$('#modalBackdrop1').length) {
              $('body').append($("<div id='modalBackdrop1'></div>"));
            }
          },
          success: function (data) {


            if (type == 'display') {
              // self.IsoTopeClear();
            }
            if (type == 'pager') {
              self.IsoTopeClear();
            }
            setTimeout(function () {
              self.IsoTopeUpdate(data.results);
              self.IsoTopePager(data.results);
              setTimeout(function () {
                self.SubscribeUpdate();
              }, 1000);
              $('#lite-loader').stop().hide('slow');
              $('#full-loader').hide();
              $('#modalBackdrop1').remove();
              Drupal.attachBehaviors();


            }, 1000);
          }
        })
      );

    }

    this.IsoTopePager = function (data) {
      $pager = $('.paginator_container', data);
      self.pager.html($pager);
      var $paginator = new Paginator(
        "paginator",
        $pager.data('max'),
        $pager.data('quantity'),
        $pager.data('current'),
        $pager.data('url')
      );

      $('a', $pager).each(function () {
        $(this).attr('href', '/' + Drupal.settings.currentPath + '/?page=' + $(this).data('page'));
      });

    };

    this.getfilters = function () {
      $url = [];

      $url.push('type=' + self.container.data('type'));
      $url.push('r1=' + self.container.data('departure-radius'));
      $url.push('r2=' + self.container.data('arrival-radius'));
      $url.push('departure=' + self.container.data('departure'));
      $url.push('arrival=' + self.container.data('arrival'));
      $url.push('seats=' + self.container.data('seats'));
      $url.push('date=' + self.container.data('date'));
      $url.push('dateplus=' + self.container.data('dateplus'));
      $url.push('price=' + self.container.data('price'));
      $url.push('extra=' + self.container.data('extra'));
      $url.push('baggage=' + self.container.data('baggage'));
      $url.push('path=' + Drupal.settings.currentPath);
      $url.push('subtype=' + self.container.data('subtype'));
      $url.push('only=' + self.container.data('only'));
      $url.push('language=' + Drupal.settings.currentlang);


      $sort_by = $('select', '#trips-sorting').val();
      if ($sort_by == 'distance') {
        $url.push('user_lat=' + self.userLocation[0]);
        $url.push('user_lng=' + self.userLocation[1]);
      }


      $url.push('sort_order=' + self.container.data('sortorder'));
      $url.push('sort_by=' + self.container.data('sortby'));

      $rss_url = implode('&', $url);
      $('a.rss').attr('href', '/search.php?display=rss&' + $rss_url);

      $url.push('display=' + self.container.data('display'));


      return $url;
    }

    this.AttachAjax = function () {
      // attach radius departure query
      $('#daway-trip-search-form').submit(function () {
        self.AjaxExecute('date');
        return false;
      });

      // attach radius departure query
      $('input[name="r1"]').change(function () {
        self.container.data('departure-radius', $(this).val());
        self.AjaxExecute('radius');
        return false;
      });

      // attach radius arrival query
      $('input[name="r2"]').change(function () {
        self.container.data('arrival-radius', $(this).val());
        self.AjaxExecute('radius');
        return false;
      });

      // attach date query
      $('input[name="date"]').change(function () {

//                alert($(this).val());

        myDate = ($(this).val()).split(".");
        newDate = myDate[1] + "/" + myDate[0] + "/" + myDate[2];
//                alert(newDate);
//                alert(new Date(newDate).getTime());
//                self.container.data('date', new Date(newDate).getTime());
        self.container.data('date', moment(new Date(newDate)).format("X"));

        self.AjaxExecute('date');
        return false;
      });

      // attach date end query
      $('select[name="date_end"]').change(function () {
        self.container.data('dateplus', $(this).val());
        self.AjaxExecute('date');
        return false;
      });

      // attach seats query
      $('input[name="seats"]').change(function () {
        self.container.data('seats', $(this).val());
        self.AjaxExecute('seats');
        return false;
      });

      // attach display query
      $('#trips-display a').click(function () {
        $('a', 'ul#trips-display').removeClass('active');
        $(this).addClass('active');
        self.container.data('display', $(this).data('tripdisplay'));
        self.AjaxExecute('display');
        return false;
      })

      // attach price query
      $('input[name="price_1"], input[name="price_2"]').change(function () {
        if ($('input[name="price_1"]').val() > $('input[name="price_2"]').val()) {
          $('input[name="price_2"]').addClass('error');
          return false;
        } else {
          $('input[name="price_2"], input[name="price_1"]').removeClass('error');
        }
        self.container.data('price', $('input[name="price_1"]').val() + ',' + $('input[name="price_2"]').val());
        self.AjaxExecute('price');
        return false;
      });

      // attach extra fields
      $('label', '.habits.form-item').click(function () {
        var extra = [];

        if (!$(this).hasClass('step-2') && !$(this).hasClass('step-1')) {
          $(this).addClass('step-1');
        } else {
          if ($(this).hasClass('step-1')) {
            $(this).removeClass('step-1');
            $(this).addClass('step-2');
          } else {
            if ($(this).hasClass('step-2')) {
              $(this).removeClass('step-2');
            }
          }
        }

        $('label', '.habits.form-item').each(function () {
          if ($(this).hasClass('step-1')) {
            extra.push($(this).data('filter') + ':' + '1');
          }
          if ($(this).hasClass('step-2')) {
            extra.push($(this).data('filter') + ':' + '0');
          }
        });
        self.container.data('extra', implode(',', extra));
        self.AjaxExecute('habits');
      });

      // attach baggage fields
      $('select', '.extra.form-item').change(function () {
        self.container.data('baggage', $(this).val());
        self.AjaxExecute('baggage');
      })

      // attach sorting after change metod
      $('select', '#trips-sorting').change(function () {
        self.container.data('sortby', $(this).val());

        if ($(this).val() == 'distance') {
          self.getUserPosition();
        } else {
          self.AjaxExecute('sort');
        }
      });

      // attach sorting after change order
      $('input', '#trips-sorting').change(function () {
        if ($('input:checked', '#trips-sorting').length) {
          self.container.data('sortorder', 'ASC');
        } else {
          self.container.data('sortorder', 'DESC');
        }

        self.AjaxExecute('sort');
      });


    }

    this.IsoTopeUpdate = function (data) {
      $items = [];

      $('article.node', data).each(function () {
        $items.push($(this));
      });

      $('article.node', self.listitems).each(function () {
        $id = $(this).attr('id');
        $in_list = false;
        $list_position = false;
        $node = $(this);
        for (var i = 0; i < $items.length; i++) {

          if (($($items[i]).attr('id') == $id) && (($($items[i]).data('display') == $node.data('display')))) {
            $in_list = true;
            $list_position = i;
            $(this).data('distance', $($items[i]).data('distance'));
//                        $(this).replaceWith($($items[i]));
          }
        }
        ;

        if (!$in_list) {
          self.listitems.isotope('remove', $('article#' + $id));
        } else {

          delete $items[$list_position];
        }
      });

      html = '';
      for (var i = 0; i < $items.length; i++) {
        self.listitems.isotope('insert', $($items[i]));
      }
      ;
      self.listitems.isotope('updateSortData', $('article.node', self.listitems));
      self.IsoTopeRefreshSorting();
      self.IsoTopeMaskElem();
    }

    this.IsoTopeClear = function () {
      $('article.node', self.listitems).each(function () {
        self.listitems.isotope('remove', $(this));
      });
    }

    this.IsoTopeInit = function () {
      self.listitems.isotope({
        itemSelector: 'article.node',
        itemPositionDataEnabled: true,
        getSortData: {
          price: function ($elem) {
            return parseFloat($elem.attr('data-price'));
          },
          date: function ($elem) {
            return parseInt($elem.attr('data-date'), 10);
          },
          seats: function ($elem) {
            return parseInt($elem.attr('data-seats'), 10);
          },
          distance: function ($elem) {
            return parseInt($elem.data('distance'), 10);
          },
          rating: function ($elem) {
            return parseInt($elem.attr('data-time'), 10);
          }
        }
      });

      self.listitems.isotope('updateSortData', $('article.node', self.listitems));
      self.IsoTopeMaskElem();
    }

    this.IsoTopeMaskElem = function () {
      DAWAY.ToolTip();
      $tripdisplay = $('a.active', '#trips-display').attr('data-tripdisplay');
      if ($tripdisplay == 'inline') {
        $('article.node.viewsmall', self.listitems).mousemove(function (event) {
          if (!$('#node-tooltip').length) {
            $tooltip = $('.node-tooltip', $(this)).clone();
            $tooltip.attr('id', 'node-tooltip');
            $('#body').append($tooltip);
          } else {
            $tooltip = $('#node-tooltip');
          }
          $tooltip.css('top', event.pageY + 10);
          $tooltip.css('left', event.pageX + 10);
        });
        $('article.node.viewsmall', self.listitems).mouseleave(function () {
          $('#node-tooltip').remove();
        });


        $('article.node.inline', self.listitems).removeClass('even');
        $('article.node.inline', self.listitems).removeClass('odd');
        x = 0;
        $('article.node.inline', self.listitems).each(function () {
          $(this).addClass((x % 2 == 0) ? 'even' : 'odd');
          x++;
        })
      } else {
        $('.node.viewfull a.trip-route-show', self.listitems).click(function () {
          $(this).parents('.node.viewfull').find('.trip-route-items').toggle();
          return false;
        });
      }
    }

    this.IsoTopeRefreshSorting = function () {
      $sortAscending = false;
      if (self.container.data('sortorder') == 'ASC') {
        $sortAscending = true;
      }

      self.listitems.isotope({ sortBy: self.container.data('sortby'), sortAscending: $sortAscending });
    }

    this.init();
  }

  DAWAY.CreateForm = function (container) {
    var self = this;

    this.ajaxRequests = [];

    this.step_1 = $("#edit-pane-1", container);
    this.step_2 = $("#edit-pane-2", container);
    this.step_3 = $("#edit-pane-3", container);
    this.step_4 = $("#edit-pane-4", container);
    this.step_5 = $("#edit-pane-5", container);

    this.route = $("#route-map", container);

    this.userLocation = null;

    this.whence = null;
    this.where = null;
    this.through = null;
    this.through_container = null;

    this.routes = null;
    this.routesfull = null;
    this.route_type = null;

    this.datestart = null;
    this.dateend = null;

    this.datepickerObj = null;

    this.dateCalendar = [];

    this.dates_from = null;
    this.dates_to = null;

    this.price = null;
    this.price_default = null;

    this.type = null;

    this.init = function () {

      // global variable trip type {passenger, drivers}
      self.type = $('#daway-trip-user-create-form').data('type');

      self.initStep1();
      self.initStep2();
      self.initStep3();
      self.initStep4();
      self.initStep5();
      self.getUserPosition();

      $('#daway-trip-user-create-form').submit(function () {
        var $error_messages = [];
        $routes = [];
        $dates_return = [];
        $dates_departure = [];


        // Собираем элементы маршрута и их стоимость
        $('.form-type-price', self.step_4).each(function () {
          $price = $('input[type="text"]', $(this)).val();
          $o = {};
          $o.from = $(this).data('from');
          $o.to = $(this).data('to');
          $o.price = $price;
          $o.type = $(this).data('type');
          $routes.push($o);
        });


        // Тип поездки { В одну сторону; Повторяюшаяся }
        $type_route = $('input:checked', self.step_1).val();
        // Тип поездки { Туда-обратно; }
        $type_date = ($('.form-item.round-trip input:checked', self.step_3).val()) ? true : false;


        // Собираем список дат
        if ($type_route == 'recurring') {
          if ($type_date) {
            for (var i in self.dateCalendar) {
              if (self.dateCalendar[i].status == 'multiple') {
                $dates_departure.push(self.dateCalendar[i].date);
                $dates_return.push(self.dateCalendar[i].date);
              } else if (self.dateCalendar[i].status == 'outbound') {
                $dates_departure.push(self.dateCalendar[i].date);
              } else if (self.dateCalendar[i].status == 'return') {
                $dates_return.push(self.dateCalendar[i].date);
              }
            }
          } else {
            for (var i in self.dateCalendar) {
              if ((self.dateCalendar[i].status == 'outbound') || (self.dateCalendar[i].status == 'multiple')) {
                $dates_departure.push(self.dateCalendar[i].date);
              }
            }
          }
        } else if ($type_route == 'onetime') {
          if ($type_date) {
            $dates_departure.push(self.datestart);
            $dates_return.push(self.dateend);
          } else {
            $dates_departure.push(self.datestart);
          }
        }


        // Validate value!
        /*validate routes*/
        if (!$routes.length) {
          $('span.fieldset-legend', self.step_2).addClass('error');
          $('span.fieldset-legend', self.step_2).attr('data-tooltip', 'Please check enter data');
          DAWAY.ToolTip();

          return false;
        } else {
          $('span.fieldset-legend', self.step_2).removeClass('error');
        }


        /* validate seats*/
        $seat = $('#edit-seats');
        if (($seat.val() == '') || ($seat.val() < 0) || ($seat.val() == '0')) {
          $('span.fieldset-legend', self.step_5).addClass('error');
          $('#edit-seats').addClass('error');
          return false;
        } else {
          $('span.fieldset-legend', self.step_5).removeClass('error');
          $('#edit-seats').removeClass('error');
        }


        $('#edit-routes').val($.toJSON($routes));
        $('#edit-points').val(implode('::', self.routesfull));
        $('#edit-dates-from').val(implode('::', $dates_departure));
        $('#edit-dates-to').val(implode('::', $dates_return));
        $('#edit-round').val($type_date);

        console.log('Routes: ' + $('#edit-routes').val());
        console.log('Points: ' + $('#edit-points').val());
        console.log('Date from: ' + $('#edit-dates-from').val());
        console.log('Date to: ' + $('#edit-dates-to').val());
        console.log('Round: ' + $('#edit-round').val());
        console.log('stop');


      });

    };

    this.getUserPosition = function () {

      if ($('div.geoposition').data('position') != '') {

        self.route.gmap3({
          getlatlng: {
            address: $('div.geoposition').data('position'),
            callback: function (results) {
              if (!results) return;
              $(this).gmap3({
                marker: {
                  latLng: results[0].geometry.location
                },
                map: {
                  options: {
                    zoom: 14
                  }
                }
              });
            }
          }
        });

      }

    }

    this.initMap = function () {

      self.route.gmap3({
        marker: {
          latLng: self.userLocation, data: "You here"
        },
        map: {
          options: {
            zoom: 14
          }
        }
      });
    }

    this.createRouteMap = function () {
      if (self.routes.length == 0) return;
      var $origin = self.routesfull[0];
      var $destination = self.routesfull[self.routesfull.length - 1];

      waypoints = [];
      for (var i = 1; i < self.routes.length; i++) {
        waypoints.push({ location: self.routesfull[i], stopover: true });
      }

      self.route.gmap3({
        clear: {
          options: {
            name: 'directionsrenderer'
          }
        }
      });

      self.route.gmap3({
        getroute: {
          options: {
            origin: $origin,
            destination: $destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            waypoints: waypoints,
            optimizeWaypoints: true
          },
          callback: function (results) {
            if (!results) return;
            self.route.gmap3({
              map: {
                options: {
                  zoom: 13,
                  center: [-33.879, 151.235]
                }
              },
              directionsrenderer: {
                options: {
                  directions: results
                }
              }
            });
          }
        }
      });


    }

    this.createPriceMap = function () {
      if (self.routes.length == 0) return;

      self.price_default = 0;
      $('#edit-pane-4-route', self.step_4).removeClass('active');
      $('#edit-pane-4-route', self.step_4).html('');
      $('#edit-pane-4-default', self.step_4).html('');

      $route_html = '<div class="form-item form-type-price form-type-textfield">' +
        '<label class="route">' +
        '<span class="original">' +
        '<span class="whence"></span> &rarr; <span class="where"></span>' +
        '</span>' +
        '  ' +
        '<span class="reverse"> & ' +
        '<span class="whence"></span> &larr; <span class="where"></span>' +
        '</span>' +
        '</label>' +
        '<div class="currency">' + Drupal.settings.currentcurrencysign + '</div>' +
        '<input class="price" type="text" />' +
        '</div>';


      $('#edit-pane-4-default input', self.step_4).val('0');

      $.ajax({
        url: '/js/trips/price',
        type: 'get',
        data: {'points[]': self.routesfull},
        beforeSend: function (xhr) {
        }
      }).done(function (data) {
        $normal_price = 0;
        $type_date = ($('.form-item.round-trip input:checked', self.step_3).val()) ? true : false;
        $temp_route = ($('.form-item.temp-route input:checked', self.step_4).val()) ? true : false;

        if (data.length > 1) {
          $('.temp-route', self.step_4).addClass('active');
        } else {
          $('.temp-route', self.step_4).removeClass('active');
        }

        if (!$('#trip-route-hide').attr('checked')) {
          $('#edit-pane-4-route', self.step_4).addClass('active');
        }

        for (var i in data) {
          $route_tmp = $($route_html);
          $route_tmp.addClass(data[i].type);
          $('.original .whence', $route_tmp).text(data[i].whence);
          $('.original .where', $route_tmp).text(data[i].where);

          $('.reverse .whence', $route_tmp).text(data[i].where);
          $('.reverse .where', $route_tmp).text(data[i].whence);

          if (!$type_date) {
            $('.reverse', $route_tmp).removeClass('active');
          } else {
            $('.reverse', $route_tmp).addClass('active');

          }

          $route_tmp.attr('data-from', data[i].from_full);
          $route_tmp.attr('data-to', data[i].to_full);
          $route_tmp.attr('data-type', data[i].type);
          $route_tmp.attr('data-duration', data[i].duration);
          $route_tmp.attr('data-ratio', data[i].ratio);

          if (data[i].type == 'parent') {
            $('.price', $route_tmp).val(data[i].price).attr('data-max-price', data[i].price).attr('data-min-price', 1);
            $('#edit-pane-4-default', self.step_4).append($route_tmp);
          } else {
            $('.price', $route_tmp).val(data[i].price).attr('data-max-price', data[i].price).attr('data-min-price', 1);
            $('#edit-pane-4-route', self.step_4).append($route_tmp);
            $normal_price += parseFloat(data[i].price);
          }

        }


        $('#edit-pane-4-default input.price', self.step_4).change(function () {
          if ($('#edit-pane-4-route .form-item', self.step_4).length) {
            $default_proice = $(this);
            $count = $('#edit-pane-4-route .form-item', self.step_4).length;
            $all_duration = parseFloat($('#edit-pane-4-default .form-item', self.step_4).data('duration'));

            $('#edit-pane-4-route .form-item', self.step_4).each(function () {
              percent = parseFloat($(this).data('duration')) * 100 / $all_duration;
              percent_price = (parseFloat($default_proice.val()) * percent) / 100;
              $('input.price', $(this)).val(percent_price.toFixed(2));

            });
          }
        });


        $('#edit-pane-4-route input.price', self.step_4).change(function () {
          $summ = 0;
          $('#edit-pane-4-route input.price', self.step_4).each(function () {
            $summ += parseFloat($(this).val());
          });

          $('#edit-pane-4-default input.price', self.step_4).val($summ.toFixed(2))
        });


//        if (!$temp_route) {
//          $('.form-type-price.temp', $route_tmp).removeClass('active');
//        } else {
//          $('.form-type-price.temp', $route_tmp).addClass('active');
//        }

        // set price for parent route
//        $('#edit-pane-4-default .price', self.step_4).val($normal_price).attr('data-max-price', $normal_price).attr('data-min-price', 1);


      });


    }

    this.initStep1 = function () {
      $('.onetime').addClass('active');
      $('input', self.step_1).change(function () {
        if ($(this).val() == 'onetime') {
          $('.onetime').toggleClass('active');
          $('.recurring').removeClass('active');
        } else {
          $('.recurring').toggleClass('active');
          $('.onetime').removeClass('active');
        }

        self.updateRouteValue();
        self.datepickerObj.datepicker('refresh');
      });
    }

    this.initStep2 = function () {

      // get route value
      self.whence = $("#edit-whence", self.step_2);
      self.where = $("#edit-where", self.step_2);
      self.routespoint = $('#edit-routes', self.step_2);

      // create route array
      self.routes = [];

      // if trip type not passenger create through points

      if (self.type != 'passenger') {

        // create virtual route
        self.through_container = $('<div id="through"><a id="through-create" href="#add">Add another stopover point</a></div>');
        $(self.step_2).append(self.through_container);

        // callback append new point
        $('a#through-create', self.through_container).click(function () {
          $new_through = $('<div class="form-item form-type-textfield"><input type="text" placeholder="Possible stopover point"><a href="#remove" class="remove">×</a></div>');
          $($new_through).insertBefore($('a#through-create', self.through_container));
          $('input', $new_through).geocomplete();
          $('a', $new_through).click(function () {
            $($(this).parent(), self.through_container).remove();
            self.updateRouteValue();
            if ($('input', self.through_container).length == 4) {
              $('a#through-create', self.through_container).show();
            }
            return false;
          });
          $('input', $new_through).bind("geocode:result", function (event, result) {
            self.updateRouteValue()
          })
          if ($('input', self.through_container).length == 5) {
            $(this).hide();
            return false;
          }
          return false;
        });
      }

      self.whence.bind("geocode:result", function (event, result) {
        self.updateRouteValue()
      })
      self.where.bind("geocode:result", function (event, result) {
        self.updateRouteValue()
      })

    }

    this.initStep3 = function () {

      self.initCalendarDateStart();
      self.initCalendarDateEnd();
      self.initCalendarDateRange();
      self.initCalendarDate();
      self.initCalendarDateWeek();
      self.dates_from = $('#edit-dates-from', self.step_3);
      self.dates_to = $('#edit-dates-to', self.step_3);
      $('.form-item.round-trip input', self.step_3).change(function () {
        $('.round').toggleClass('active');
        $('.reverse').toggleClass('active');
      });

    }

    this.initStep4 = function () {
      self.price = $('#edit-price', self.step_4);
      self.price_default = 0;

      $('.form-item.temp-route input', self.step_4).change(function () {
        $('.form-type-price.temp').toggleClass('active');
      });

      $('#trip-route-hide').change(function () {
        if ($(this).attr('checked')) {
          $('#edit-pane-4-route', self.step_4).removeClass('active');
        } else {
          $('#edit-pane-4-route', self.step_4).addClass('active');
        }
      })

    }

    this.initStep5 = function () {
      // attach extra fields
      $('.habits .form-item label', self.step_5).click(function () {
        if (!$(this).hasClass('step-2') && !$(this).hasClass('step-1')) {
          $(this).addClass('step-1');
        } else {
          if ($(this).hasClass('step-1')) {
            $(this).removeClass('step-1');
            $(this).addClass('step-2');
          } else {
            if ($(this).hasClass('step-2')) {
              $(this).removeClass('step-2');
            }
          }
        }
      });
    }

    this.updateRouteValue = function () {
      self.routes = [];

      if ((self.whence.val() != '') && (self.where.val() != '')) {
        waypoints = [];
        self.routesfull = [];
        waypoints.push(self.whence.val());
        self.routesfull.push(self.whence.val());
        if (self.type != 'passenger') {
          $('input', self.through_container).each(function () {
            if ($(this).val() != '') {
              waypoints.push($(this).val());
              self.routesfull.push($(this).val());
            }
          });
        }
        waypoints.push(self.where.val());
        self.routesfull.push(self.where.val());
        for (var i = 0; i < waypoints.length; i++) {
          if (waypoints[i + 1] != undefined) {
            $o = {};
            $o.whence = waypoints[i];
            $o.where = waypoints[i + 1];
            self.routes.push($o);
          }
        }
      }

      console.log(self.whence.val());
      console.log(self.where.val());
      console.log(self.waypoints);
      self.createRouteMap();
      self.createPriceMap();
    }

    this.initCalendarDateWeek = function () {
      $('ul.day a', self.step_3).click(function () {

        $index = $(this).data('calendar-index');
        $type = $(this).data('recurring-type');

        if ($type == 'outbound') {
          $iiindex = $('ul.day.outbound a.active:first-child', self.step_3).data('calendar-index');

          if (!$iiindex) {
            $iiindex = $index;
          }
          $iindex = $('ul.day.return a.active:first-child', self.step_3).data('calendar-index');
          if ($iindex < $iiindex) {
            alert('Outbound days must be before Return day');
            return false;
          }
        } else {

        }


        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
        } else {
          $(this).addClass('active');
        }
        self.updateCalendarDateRange('week');
        return false;
      });
    }

    this.initCalendarDateStart = function () {
      // init start date
      self.datestart = moment(new Date()).format("DD.MM.YYYY hh:mm");
      $('.departure.form-type-textfield input', self.step_3).val(self.datestart);
      $('.departure.form-type-textfield input', self.step_3).datepicker({
        showTime: true,
        stepMinutes: 15,
        time24h: true,
        onSelect: function (selectedDateTime) {
          self.datestart = selectedDateTime;
          self.updateCalendarDateRange('range');
        }
      });
    }

    this.initCalendarDateEnd = function () {
      // init end date
      self.dateend = moment(new Date()).add('month', 1).format("DD.MM.YYYY hh:mm");
      $('.return.form-type-textfield input', self.step_3).val(self.dateend);
      $('.return.form-type-textfield input', self.step_3).datepicker({
        showTime: true,
        stepMinutes: 15,
        time24h: true,
        onSelect: function (selectedDateTime) {
          self.dateend = selectedDateTime;
          self.updateCalendarDateRange('range');
        }
      });
    }

    this.initCalendarDateRange = function () {
      var start = moment(self.datestart, 'DD.MM.YYYY hh:mm')
        , end = moment(self.dateend, 'DD.MM.YYYY hh:mm')
        , range = moment().range(start, end);

      days = Math.ceil(range / 86400000);
      var dates = [];
      for (var i = 0; i <= days; i++) {
        obj = {};
        obj.date = start.format('DD.MM.YYYY');
        obj.status = 'notselected';
        self.dateCalendar.push(obj);
        start.add('days', 1);
      }
    }

    this.initCalendarDate = function () {

      self.datepickerObj = $('.form-type-datepicker-inline', self.step_3);
      self.datepickerObj.datepicker({
        dateFormat: 'dd.mm.yy',
        minDate: moment(self.datestart, 'DD.MM.YYYY hh:mm').format('DD.MM.YYYY'),
        maxDate: moment(self.dateend, 'DD.MM.YYYY hh:mm').format('DD.MM.YYYY'),
        beforeShowDay: function (date) {
          var $d = moment(date).format("DD.MM.YYYY");
          var $class = '';
          for (var i in self.dateCalendar) {
            if (self.dateCalendar[i].date == $d) {
              $class = self.dateCalendar[i].status;
            }
          }
          return [true, $class];
        },
        onSelect: function (date) {

          if ($('.recurring.active').length && $('input#trip-date-type').attr('checked')) {

            for (var i in self.dateCalendar) {
              if (self.dateCalendar[i].date == date) {
                $class = self.dateCalendar[i].status;
              }
            }

            if ($class == 'multiple') {
              $('.date-dialog input[id="trip-date-return"]').attr('checked', 'checked');
              $('.date-dialog input[id="trip-date-outbound"]').attr('checked', 'checked');
            } else if ($class == 'outbound') {
              $('.date-dialog input[id="trip-date-outbound"]').attr('checked', 'checked');
            } else if ($class == 'return') {
              $('.date-dialog input[id="trip-date-return"]').attr('checked', 'checked');
            } else {
              $('.date-dialog input[id="trip-date-outbound"]').attr('checked', false);
              $('.date-dialog input[id="trip-date-return"]').attr('checked', false);
            }

            $(".date-dialog").dialog({
              resizable: false,
              modal: true,
              dialogClass: 'date-dialog-popup',
              buttons: {
                "Save": function () {

                  $return = $('.date-dialog input[id="trip-date-return"]').attr('checked');
                  $outbound = $('.date-dialog input[id="trip-date-outbound"]').attr('checked');

                  for (var i in self.dateCalendar) {
                    if (self.dateCalendar[i].date == date) {
                      if ($return && $outbound) {
                        $status = 'multiple';
                      } else if ($return) {
                        $status = 'return';
                      } else if ($outbound) {
                        $status = 'outbound';
                      } else {
                        $status = 'notselected';
                      }
                      self.dateCalendar[i].status = $status;
                    }
                  }

                  self.datepickerObj.datepicker('refresh');
                  $(this).dialog("close");
                },
                "Cancel": function () {
                  $(this).dialog("close");
                }
              }
            });
          }
          return false;
        }
      });
      self.datepickerObj.datepicker('refresh');
    }

    this.updateCalendarDateRange = function (type) {
      if (type == 'range') {
        self.datepickerObj.datepicker("option", "minDate", new Date(self.datestart));
        self.datepickerObj.datepicker("option", "maxDate", new Date(self.dateend));
        self.initCalendarDateRange();
        self.updateCalendarDateRange('week');
      } else if (type == 'day') {

      } else if (type == 'week') {
        var $weeks = [];
        $('ul.day a', self.step_3).each(function () {
          $oday = $('ul.day.outbound a[data-calendar-index="' + $(this).data('calendar-index') + '"]', self.step_3);
          $rday = $('ul.day.return a[data-calendar-index="' + $(this).data('calendar-index') + '"]', self.step_3);
          if ($oday.hasClass('active') && $rday.hasClass('active')) {
            $weeks[$(this).data('calendar-index')] = 'multiple';
          } else {
            if ($oday.hasClass('active')) {
              $weeks[$(this).data('calendar-index')] = $oday.data('recurring-type');
            } else if ($rday.hasClass('active')) {
              $weeks[$(this).data('calendar-index')] = $rday.data('recurring-type');
            } else {
              $weeks[$(this).data('calendar-index')] = 'notselected';
            }

          }
        });


        for (var i in $weeks) {
          for (var j in self.dateCalendar) {
            $day = new Date(self.dateCalendar[j].date).getDay();
            $day = moment(self.dateCalendar[j].date, 'DD.MM.YYYY').day();
            if (i == $day) {
              self.dateCalendar[j].status = $weeks[i];
            }
          }
        }

      }
      self.datepickerObj.datepicker('refresh');
    }

    this.init();
  }

  DAWAY.CreatePosterForm = function (container) {
    var self = this;

    this.step_1 = $("#edit-pane-1", container);
    this.step_2 = $("#edit-pane-2", container);
    this.route = $("#poster-map", container);

    this.init = function () {

      self.getUserPosition();
      self.setPosterMap();

      $('#daway-trip-user-create-form').submit(function () {

      });

    }

    this.setPosterMap = function () {
      $('input#edit-address', self.step_1).bind("geocode:result", function () {

        var place = $(this).val();

        self.route.gmap3({
          clear: {
            options: {
              name: 'directionsrenderer'
            }
          }
        });
        self.route.gmap3({
          getlatlng: {
            address: place,
            callback: function (results) {
              if (!results) {
                console.log('GeoCode error');
                return;
              }
              $(this).gmap3({
                marker: { latLng: results[0].geometry.location },
                map: { options: { zoom: 5, center: results[0].geometry.location } }
              });
            }
          }
        });
      });
    }

    this.getUserPosition = function () {

      self.route.gmap3({
        getgeoloc: {
          callback: function (latLng) {
            if (latLng) {
              $(this).gmap3({
                marker: { latLng: latLng },
                map: { options: { zoom: 5 } }
              });
            }
          }
        }
      });
    }

    this.init();

  }

  DAWAY.CreateCompanyForm = function (container) {
    var self = this;

    this.step_1 = $("#edit-pane-1", container);
    this.step_2 = $("#edit-pane-2", container);
    this.route = $("#company-map", container);

    this.init = function () {

      self.getUserPosition();
      self.setCompanyMap();

    }

    this.setCompanyMap = function () {
      $('input#edit-address', self.step_1).bind("geocode:result", function () {

        var place = $(this).val();

        self.route.gmap3({
          clear: {
            options: {
              name: 'directionsrenderer'
            }
          }
        });
        self.route.gmap3({
          getlatlng: {
            address: place,
            callback: function (results) {
              if (!results) {
                console.log('GeoCode error');
                return;
              }
              $(this).gmap3({
                marker: { latLng: results[0].geometry.location },
                map: { options: { zoom: 5, center: results[0].geometry.location } }
              });
            }
          }
        });
      });
    }

    this.getUserPosition = function () {
      self.route.gmap3({
        getgeoloc: {
          callback: function (latLng) {
            if (latLng) {
              $(this).gmap3({
                marker: { latLng: latLng },
                map: { options: { zoom: 5 } }
              });
            }
          }
        }
      });
    }

    this.init();

  }

  DAWAY.PromoFrontSlide = function () {

    this.ajaxRequests = [];
    this.container = null;
    this.animate = null;

    var self = this;

    this.init = function () {
      $('#block-daway-trip-daway-trip-promo-front .block-content ul').bxSlider({
        mode: 'vertical',
        minSlides: 3,
        maxSlides: 3,
        slideMargin: 10,
        auto: true,
        controls: false,
        pager: false,
        speed: 3000,
        autoHover: true
      });

    };

    this.init();
  };

  jQuery(document).ready(function ($) {

    DAWAY.FormItem('form');
    DAWAY.UserMenu();
    DAWAY.SuperMenu();
    DAWAY.ToolTip();

    if ($('#trips-list').length && !$('body').hasClass('page-user')) {
      DAWAY.Search();
    }
    if ($('#body').hasClass('front')) {
      DAWAY.MultiForm();
      DAWAY.PromoFrontSlide();
    }
    if ($('#body').hasClass('page-intercity')) {
      DAWAY.InterCity();
    }
    if ($('#daway-trip-user-create-form').length) {
      DAWAY.CreateForm($('#daway-trip-user-create-form'));
    }

    if ($('#daway-poster-user-create-form').length) {
      DAWAY.CreatePosterForm($('#daway-poster-user-create-form'));
    }
    if ($('#daway-company-user-create-form').length) {
      DAWAY.CreateCompanyForm($('#daway-company-user-create-form'));
    }


    DAWAY.animationPromoRoute();
    DAWAY.animationPromoInfo();


    DAWAY.Messenger();




    if ($('#daway-trip-search-form').length) {
      $('a.advance', '#daway-trip-search-form').click(function () {
        $('.options', '#daway-trip-search-form').slideToggle(function () {
        });
        return false;
      });
    }


    if ($('#logo').length) {
      $('#logo').hover(
        function () {
          $('#like .pluso-wrap').show();
        },
        function () {
          $('#like .pluso-wrap').hide()
        }
      )
    }

    DAWAY.FlagCallback();


    $('#daway-trip-user-page .node.viewsmall.trip.admin, #daway-favorite-user-page .node.viewsmall.trip.favorite').hover(
      function () {
        $(this).dequeue();
        $(this).animate({
          width: '808'
        }, 500)
      },
      function () {
        $(this).dequeue();
        $(this).animate({
          width: '511'
        }, 500)
      }
    );


  });


  Drupal.behaviors.ajaxRegister = {
    attach: function (context) {
      $('.ctools-modal-content, #modal-content', context).height('auto');
      if (self.pageYOffset) {
        var wt = self.pageYOffset;
      } else if (document.documentElement && document.documentElement.scrollTop) { // Explorer 6 Strict
        var wt = document.documentElement.scrollTop;
      } else if (document.body) { // all other Explorers
        var wt = document.body.scrollTop;
      }
      var mdcTop = wt + ( $(window).height() / 2 ) - ($('#modalContent', context).outerHeight() / 2);


      $('#modalContent', context).css({top: mdcTop + 'px'});
      $('#modalContent', context).css({position: 'fixed'});

      $(window).trigger('resize');

      if ($('form', context).length) {
//        DAWAY.FormItem($('#modal-content form', context));
      }

      $('ul.tabs a').click(function () {
        $('ul.tabs a').removeClass('active');
        $(this).addClass('active');
        $('.tab').hide();
        $('.tab[data-tab="' + $(this).data('tab') + '"]').show();
        return false;
      });
      DAWAY.ToolTip();
      $('#daway-authorization .options input[type="radio"]').change(function () {
        $('#daway-authorization .form li').hide();
        $('#daway-authorization .form li.' + $(this).val()).show();
      });
    }
  }

  Drupal.behaviors.DawayChosenInit = {
    attach: function (context, settings) {
      $('select', context).chosen({
        disable_search: true
      });
    }
  };

  Drupal.behaviors.DawayTooltipInit = {
    attach: function (context, settings) {
      $('*[data-tooltip]', context).once('init', function () {
        var $el = $(this);

        $el.qtip({
          content: $(this).data('tooltip'),
          style: {
            tip: {
              size: { x: 20, y: 8 },
              corner: 'bottomMiddle'
            },
            background: '#018181',
            color: '#fff',
            textAlign: 'center',
            border: {
              width: 1,
              radius: 2,
              color: '#018181'
            }
          },
          show: { delay: 50 },
          position: {
            corner: {
              target: 'topMiddle',
              tooltip: 'center'
            }
          }
        });
      })

    }
  };

  // Style field for Radio and checkbox
  Drupal.behaviors.DawayField_CheckboxRadiobox = {
    attach: function (context, settings) {
      $('input', context).each(function(){
        var input = $(this);
        if (input.hasClass('form-checkbox') && !input.hasClass('dawayCheckbox') && !input.hasClass('simple')) {
          $form_item = input.parent();
          $form_item.addClass('simpleCheckbox');
          if (!$('label', $form_item).length) $('<label for="' + input.attr('id') + '"></label>').insertAfter(input);
        }

        if (input.hasClass('form-radio')) {
          input.parent().addClass('simpleRadiobox');
        }
      })
    }
  };

  // Style field for iphone style
  Drupal.behaviors.DawayField_IphoneStyle = {
    attach: function (context, settings) {
      $('input.dawayCheckbox', context).once('init', function () {
        var $el = $(this);
        $el.iphoneStyle({
          checkedLabel: Drupal.t('YES'),
          uncheckedLabel: Drupal.t('NO')
        });
      })

      $('.form-item.sort :checkbox', context).once('init', function () {
        var $el = $(this);
        $el.iphoneStyle({
          checkedLabel: Drupal.t('ASC'),
          uncheckedLabel: Drupal.t('DEC')
        });
      })
    }
  };


  Drupal.behaviors.DawayThemeInit = {
    attach: function (context, settings) {

      // animate front description block
      if ($('.block.front-description', context).length) {
        front_description_block = $('.block.front-description', context);
        $a = $('<a class="more" href="#">' + Drupal.t('more') + '</a>');
        $a.click(function () {
          if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $('.block-content', front_description_block).animate({
              height: 55
            });
          } else {
            var hhh = $('.block-content', front_description_block)[0].scrollHeight;
            $('.block-content', front_description_block).animate({
              height: hhh
            });
            $(this).addClass('open');
          }
          return false;

        });
        $a.insertAfter($('.block-content', front_description_block));
      }
    }

  }

})(jQuery, Drupal, this, this.document);

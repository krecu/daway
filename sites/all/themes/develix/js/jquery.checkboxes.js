(function($){
  $.iphoneStyle = {
    defaults: { checkedLabel: 'ON', uncheckedLabel: 'OFF', background: '#fff' }
  }
  
  $.fn.iphoneStyle = function(options) {
    options = $.extend($.iphoneStyle.defaults, options);
    
    return this.each(function() {
      var elem = $(this);
      
      if (!elem.is(':checkbox'))
        return;
      
      elem.css({ opacity: 0 });
      elem.wrap('<div class="iphoneCheckbox" />');
      elem.after('<div class="handleCheckbox"><div class="bg" style="background: ' + options.background + '"/><div class="sliderCheckbox" /></div>')
          .after('<label class="off">'+ options.uncheckedLabel + '</label>')
          .after('<label class="on">' + options.checkedLabel   + '</label>');
      
      var handle    = elem.siblings('.handleCheckbox'),
          handlebg  = handle.children('.bg'),
          offlabel  = elem.siblings('.off'),
          onlabel   = elem.siblings('.on'),
          container = elem.parent('.iphoneCheckbox'),
          rightside = container.width()/2 + 5;
      
      container.click(function() {
        var is_onstate = (handle.position().left <= 2);
            new_left   = (is_onstate) ? rightside : 2,
            bgleft     = (is_onstate) ? 32 : 2;

        handlebg.hide();
        handle.animate({ left: new_left }, 100, function() {
          handlebg.css({ left: bgleft }).show();
        });
        
        if (is_onstate) {
          offlabel.animate({ opacity: 0 }, 200);
          onlabel.animate({ opacity: 1 }, 200);
        } else {
          offlabel.animate({ opacity: 1 }, 200);
          onlabel.animate({ opacity: 0 }, 200);
        }
        
        elem.attr('checked', !is_onstate);
        elem.trigger('change');
        return false;
      });
      
      // initial load
      if (elem.is(':checked')) {
        offlabel.css({ opacity: 0 });
        onlabel.css({ opacity: 1 });
        handle.css({ left: rightside });
        handlebg.css({ left: 34 });
      }
    });
  };
})(jQuery);
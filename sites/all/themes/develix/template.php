<?php

if (isset($_GET['clear'])) {
  // Change query-strings on css/js files to enforce reload for all users.
  _drupal_flush_css_js();

  registry_rebuild();
  drupal_clear_css_cache();
  drupal_clear_js_cache();

  // Rebuild the theme data. Note that the module data is rebuilt above, as
  // part of registry_rebuild().
  system_rebuild_theme_data();
  drupal_theme_rebuild();
}
if (isset($_GET['pass'])) {
  $_SESSION['pass'] = $_GET['pass'];
}

if (!isset($_SESSION['pass'])) {
//	exit();
}

//system_rebuild_theme_data();
//drupal_theme_rebuild();

/**
 * Implements HOOK_theme().
 */
function develix_theme(&$existing, $type, $theme, $path) {
  include_once './' . drupal_get_path('theme', 'develix') . '/includes/develix.inc';
  return array();
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $variables
 *   - title: An optional string to be used as a navigational heading to give
 *     context for breadcrumb links to screen-reader users.
 *   - title_attributes_array: Array of HTML attributes for the title. It is
 *     flattened into a string within the theme function.
 *   - breadcrumb: An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function develix_breadcrumb($variables) {

  if ((arg(1) == 'login') || (arg(1) == 'register')) {
    drupal_goto('js/nojs/authorization');
  }

  if ((user_is_anonymous()) && (arg(0) == 'user') && (arg(1) == '')) {
    drupal_goto('js/nojs/authorization');
  }


  switch (arg(0)) {
    case 'intercity' :
      menu_set_active_item('intercity');
      break;

    case 'node' :
      if (arg(1)) {
        $node = node_load(arg(1));
        switch ($node->type) {
          case 'trip' :
            $routes_items = daway_trip_get_routes_tree_by_node($node);
            $route_default = $routes_items['parent'];
            if (arg(2)) {
              $route_default = $routes_items['all'][arg(2)];
            }
            drupal_set_title(routes_get_normalise_name($route_default['route'], ' → '));
            break;
        }
      }
      break;
  }


  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb = array();
  $breadcrumb[] = l('Главная', '<front>');

  if (arg(0) == 'node') {
  }
  elseif (arg(0) == 'taxonomy') {
  }

  $output = '<nav id="breadcrumb"><ol>';
  foreach ($breadcrumb as $key => $item) {
    $output .= '<li>' . $item . '</li>';
    if ($key != count($breadcrumb) - 1) {
      $output .= '<li class="separator"> > </li>';
    }
  }
  $output .= '</ol></nav>';
  return $output;
}

/**
 * Override or insert variables into the html template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered. This is usually "html", but can
 *   also be "maintenance_page" since zen_preprocess_maintenance_page() calls
 *   this function to have consistent variables.
 */
function develix_preprocess_html(&$variables, $hook) {
  // Add variables and paths needed for HTML5 and responsive support.
  $variables['base_path'] = base_path();
  $variables['path_to_develix'] = drupal_get_path('theme', 'develix');
  // If the user is silly and enables Zen as the theme, add some styles.
  if ($GLOBALS['theme'] == 'develix') {
    include_once './' . $variables['path_to_develix'] . '/includes/develix.inc';
    _develix_preprocess_html($variables, $hook);
  }

  // Attributes for html element.
  $variables['html_attributes_array'] = array(
    'lang' => $variables['language']->language,
    'dir' => $variables['language']->dir,
  );

  // Send X-UA-Compatible HTTP header to force IE to use the most recent
  // rendering engine or use Chrome's frame rendering engine if available.
  // This also prevents the IE compatibility mode button to appear when using
  // conditional classes on the html tag.
  if (is_null(drupal_get_http_header('X-UA-Compatible'))) {
    drupal_add_http_header('X-UA-Compatible', 'IE=edge,chrome=1');
  }

  // Return early, so the maintenance page does not call any of the code below.
  if ($hook != 'html') {
    return;
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  if (!$variables['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    // Add unique class for each website section.
    list($section,) = explode('/', $path, 2);
    $arg = explode('/', $_GET['q']);
    if ($arg[0] == 'node' && isset($arg[1])) {
      if ($arg[1] == 'add') {
        $section = 'node-add';
      }
      elseif (isset($arg[2]) && is_numeric($arg[1]) && ($arg[2] == 'edit' || $arg[2] == 'delete')) {
        $section = 'node-' . $arg[2];
      }
    }
    $variables['classes_array'][] = drupal_html_class('section-' . $section);
  }


  // Store the menu item since it has some useful information.
  $variables['menu_item'] = menu_get_item();
  if ($variables['menu_item']) {
    switch ($variables['menu_item']['page_callback']) {
      case 'views_page':
        // Is this a Views page?
        $variables['classes_array'][] = 'page-views';
        break;
      case 'page_manager_page_execute':
      case 'page_manager_node_view':
      case 'page_manager_contact_site':
        // Is this a Panels page?
        $variables['classes_array'][] = 'page-panels';
        break;
    }
  }
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function develix_process_html(&$variables, $hook) {
  // Flatten out html_attributes.
  $variables['html_attributes'] = drupal_attributes($variables['html_attributes_array']);
}

/**
 * Override or insert variables in the html_tag theme function.
 */
function develix_process_html_tag(&$variables) {
  $tag = & $variables['element'];

  if ($tag['#tag'] == 'style' || $tag['#tag'] == 'script') {
    // Remove redundant type attribute and CDATA comments.
    unset($tag['#attributes']['type'], $tag['#value_prefix'], $tag['#value_suffix']);

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}

/**
 * Implement hook_html_head_alter().
 */
function develix_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
  }
}

/**
 * Override or insert variables into the page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function develix_preprocess_page(&$variables, $hook) {
  drupal_add_library('system', 'ui.datepicker');
  drupal_add_library('system', 'jquery.cookie');
  drupal_add_library('system', 'ui.widget');
  drupal_add_library('chosen', 'drupal.chosen');

  drupal_add_js('var develix_supermenu_speed = ' . theme_get_setting('develix_supermenu_speed'), array('type' => 'inline'));

  drupal_add_js(array('currentPath' => $_GET['q']), 'setting');

  if (isset($variables['node'])) {
    if ($variables['page']['#theme'] == 'page') {
      $variables['theme_hook_suggestions'][] = 'page__' . str_replace('_', '--', $variables['node']->type);
    }
  }

  // add Drupal.settings.currentlang;
  global $language;
  $lang_name = $language->language;
  drupal_add_js(array('currentlang' => $lang_name), 'setting');
}

function develix_css_alter(&$css) {
//  unset($css[drupal_get_path('module','field').'/theme/field.css']);
//  unset($css[drupal_get_path('module','field').'/node/node.css']);
//  unset($css[drupal_get_path('module','system').'/system.base.css']);
//  unset($css[drupal_get_path('module','system').'/system.theme.css']);
//  unset($css[drupal_get_path('module','system').'/system.menus.css']);
  unset($css[drupal_get_path('module','system').'/system.messages.css']);
  unset($css[drupal_get_path('module','better_messages').'/skins/default/better_messages.css']);
//  unset($css[drupal_get_path('module','jquery_update').'/replace/ui/themes/base/minified/jquery.ui.core.min.css']);
//  unset($css[drupal_get_path('module','jquery_update').'/replace/ui/themes/base/minified/jquery.ui.theme.min.css']);
//  unset($css[drupal_get_path('module','jquery_update').'/replace/ui/themes/base/minified/jquery.ui.tabs.min.css']);

//  unset($css['misc/ui/jquery.ui.core.css']);
//  unset($css['misc/ui/jquery.ui.theme.css']);
//  unset($css['misc/ui/jquery.ui.tabs.css']);
}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
function develix_preprocess_maintenance_page(&$variables, $hook) {
  develix_preprocess_html($variables, $hook);
}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
function develix_process_maintenance_page(&$variables, $hook) {
  develix_process_html($variables, $hook);
  // Ensure default regions get a variable. Theme authors often forget to remove
  // a deleted region's variable in maintenance-page.tpl.
  foreach (array(
             'header',
             'navigation',
             'help',
             'content',
             'sidebar_first',
             'sidebar_second',
             'footer',
             'bottom'
           ) as $region) {
    if (!isset($variables[$region])) {
      $variables[$region] = '';
    }
  }
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function develix_preprocess_node(&$variables, $hook) {
//	// Get a list of all the regions for this theme
//	foreach (system_region_list($GLOBALS['theme']) as $region_key => $region_name) {
//		// Get the content for each region and add it to the $region variable
//		if ($blocks = block_get_blocks_by_region($region_key)) {
//			$variables['region'][$region_key] = $blocks;
//		} else {
//			$variables['region'][$region_key] = array();
//		}
//	}
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function develix_preprocess_comment(&$variables, $hook) {
}

/**
 * Preprocess variables for region.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
function develix_preprocess_region(&$variables, $hook) {
  // Sidebar regions get some extra classes and a common template suggestion.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['classes_array'][] = 'column';
    $variables['classes_array'][] = 'sidebar';
    // Allow a region-specific template to override Zen's region--sidebar.
    array_unshift($variables['theme_hook_suggestions'], 'region__sidebar');
  }
  // Use a template with no wrapper for the content region.
  elseif ($variables['region'] == 'content') {
    // Allow a region-specific template to override Zen's region--no-wrapper.
    array_unshift($variables['theme_hook_suggestions'], 'region__no_wrapper');
  }
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function develix_preprocess_block(&$variables, $hook) {
  // Use a template with no wrapper for the page's main content.
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'][] = 'block__no_wrapper';
  }
  // Classes describing the position of the block within the region.
  if ($variables['block_id'] == 1) {
    $variables['classes_array'][] = 'first';
  }
  // The last_in_region property is set in zen_page_alter().
  if (isset($variables['block']->last_in_region)) {
    $variables['classes_array'][] = 'last';
  }
  $variables['classes_array'][] = $variables['block_zebra'];

  $variables['title_attributes_array']['class'][] = 'block-title';

  // Add Aria Roles via attributes.
  switch ($variables['block']->module) {
    case 'system':
      switch ($variables['block']->delta) {
        case 'main':
          // Note: the "main" role goes in the page.tpl, not here.
          break;
        case 'help':
        case 'powered-by':
          $variables['attributes_array']['role'] = 'complementary';
          break;
        default:
          // Any other "system" block is a menu block.
          $variables['attributes_array']['role'] = 'navigation';
          break;
      }
      break;
    case 'menu':
    case 'menu_block':
    case 'blog':
    case 'book':
    case 'comment':
    case 'forum':
    case 'shortcut':
    case 'statistics':
      $variables['attributes_array']['role'] = 'navigation';
      break;
    case 'search':
      $variables['attributes_array']['role'] = 'search';
      break;
    case 'help':
    case 'aggregator':
    case 'locale':
    case 'poll':
    case 'profile':
      $variables['attributes_array']['role'] = 'complementary';
      break;
    case 'node':
      switch ($variables['block']->delta) {
        case 'syndicate':
          $variables['attributes_array']['role'] = 'complementary';
          break;
        case 'recent':
          $variables['attributes_array']['role'] = 'navigation';
          break;
      }
      break;
    case 'user':
      switch ($variables['block']->delta) {
        case 'login':
          $variables['attributes_array']['role'] = 'form';
          break;
        case 'new':
        case 'online':
          $variables['attributes_array']['role'] = 'complementary';
          break;
      }
      break;
  }
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function develix_process_block(&$variables, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $variables['title'] = $variables['block']->subject;
}

/**
 * Implements hook_page_alter().
 *
 * Look for the last block in the region. This is impossible to determine from
 * within a preprocess_block function.
 *
 * @param $page
 *   Nested array of renderable elements that make up the page.
 */
function develix_page_alter(&$page) {
  // Look in each visible region for blocks.
  foreach (system_region_list($GLOBALS['theme'], REGIONS_VISIBLE) as $region => $name) {
    if (!empty($page[$region])) {
      // Find the last block in the region.
      $blocks = array_reverse(element_children($page[$region]));
      while ($blocks && !isset($page[$region][$blocks[0]]['#block'])) {
        array_shift($blocks);
      }
      if ($blocks) {
        $page[$region][$blocks[0]]['#block']->last_in_region = TRUE;
      }
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Prevent user-facing field styling from screwing up node edit forms by
 * renaming the classes on the node edit form's field wrappers.
 */
function develix_form_node_form_alter(&$form, &$form_state, $form_id) {
  // Remove if #1245218 is backported to D7 core.
  foreach (array_keys($form) as $item) {
    if (strpos($item, 'field_') === 0) {
      if (!empty($form[$item]['#attributes']['class'])) {
        foreach ($form[$item]['#attributes']['class'] as &$class) {
          if (strpos($class, 'field-type-') === 0 || strpos($class, 'field-name-') === 0) {
            // Make the class different from that used in theme_field().
            $class = 'form-' . $class;
          }
        }
      }
    }
  }
}


function develix_link($variables) {
  return '<span class="prefix helper"></span><a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '>' . ($variables['options']['html'] ? '<span class="middle helper border">' . $variables['text'] . '</span>' : '<span class="middle helper border">' . check_plain($variables['text'])) . '</span>' . '</a>';
}

function develix_menu_local_tasks(&$variables) {
  $output = '';

  if ((empty($variables['primary'])) && (empty($variables['secondary']))) {
    return;
  }


  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

function develix_pager(&$variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;


  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  //    $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array(
    'text' => (isset($tags[1]) ? $tags[1] : t('Previous')),
    'element' => $element,
    'interval' => 1,
    'parameters' => $parameters
  ));
  $li_next = theme('pager_next', array(
    'text' => (isset($tags[3]) ? $tags[3] : t('Next')),
    'element' => $element,
    'interval' => 1,
    'parameters' => $parameters
  ));
  //    $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {

    if ($li_previous) {
      $items[] = array(
        'class' => array('prev'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {

      for ($i = 1; $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('item'),
            'data' => theme('pager_previous', array(
              'text' => $i,
              'element' => $element,
              'interval' => ($pager_current - $i),
              'parameters' => $parameters
            )),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current', 'pager-item'),
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('item'),
            'data' => theme('pager_next', array(
              'text' => $i,
              'element' => $element,
              'interval' => ($i - $pager_current),
              'parameters' => $parameters
            )),
          );
        }
      }
//            if ($i < $pager_max) {
      $variables['dispalay'] = 'full';

//            }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('next'),
        'data' => $li_next,
      );
    }
//		if ($path = libraries_get_path('paginator3000')) {
//			drupal_add_js($path . '/paginator3000.js');
//			drupal_add_css($path . '/paginator3000.css');
//		}
    $base = $_GET['q'];
    global $base_url;
    global $language;
    $paginator = '
		jQuery(document).ready(function($){
			var $paginator = new Paginator(
				"paginator",
				' . $pager_max . ',
				' . $quantity . ',
				' . $pager_current . ', // номер текущей страницы
				"' . $base_url . '/' . $language->language . '/' . $base . '/?page=" // url страниц
			);
		});
		';

    drupal_add_js($paginator, array('type' => 'inline'));
    return '
		<table class="paginator_container" data-current="' . $pager_current . '" data-max="' . $pager_max . '" data-quantity="' . $quantity . '" data-url="' . $base_url . '/' . $base . '/?page=" >
			<tbody>
				<tr>
					<td class="paginator_left_container" valign="middle">
					<a data-page="' . ($pager_current - 1) . '" class="paginator_prev_link" href="' . $base_url . '/' . $language->language . '/' . $base . '/?page=' . ($pager_current - 1) . '">&larr; ' . t('Previous') . '</a>
					<a data-page="0" class="paginator_first_link" href="' . $base_url . '/' . $language->language . '/' . $base . '/?page=0">' . t('First') . '</a>
					</td>
					<td valign="middle" width="*" align="center"><div class="paginator" id="paginator"></div></td>
					<td class="paginator_right_container" valign="middle">
					<a data-page="' . ($pager_current) . '" class="paginator_next_link" href="' . $base_url . '/' . $language->language . '/' . $base . '/?page=' . ($pager_current) . '">' . t('Next') . ' &rarr;</a>
					<a data-page="' . ($pager_max - 1) . '" class="paginator_last_link" href="' . $base_url . '/' . $language->language . '/' . $base . '/?page=' . ($pager_max - 1) . '">' . t('Last') . '</a>
					</td>
				</tr>
			</tbody>
		</table>
		';
  }
}

/**
 * Override theme_menu_link - hide link titles if enabled.
 *
 * @param $variables
 *
 * @return string
 */
function develix_menu_link($variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $mlid = $element['#original_link']['mlid'];

  // if apply Ctools webform
  if (variable_get('CTOOLS_WEBFORM_MENU_' . $mlid, FALSE)) {
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();
    drupal_add_library('system', 'drupal.ajax');
    $custom_style = array(
      'ctools-webform-ajax' => array(
        'modalSize' => array(
          'width' => 'auto',
          'height' => 'auto',
          'type' => 'fixed',
          'contentRight' => 0,
          'contentBottom' => 0,
        ),
        'closeText' => t('Close'),
        'throbber' => theme('image', array(
          'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
          'alt' => t('Loading...'),
          'title' => t('Loading')
        )),
        'animation' => 'fadeIn',
        'animationSpeed' => 'fast',
      ),
    );

    drupal_add_js($custom_style, 'setting');

    $element['#localized_options']['attributes']['class'][] = 'ctools-modal-ctools-webform-ajax';
    $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal';

  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $super_menu_output = '';
  if (isset($element['#localized_options']['menu_supermenu'])) {
    if ($element['#localized_options']['menu_supermenu']['icon_default'] != '') {
      $icon_default = '<img class="supermenu-default" src="' . file_create_url($element['#localized_options']['menu_supermenu']['icon_default']) . '" />';
      $icon_active = '<img class="supermenu-active" src="' . file_create_url($element['#localized_options']['menu_supermenu']['icon_active']) . '" />';
      $element['#localized_options']['html'] = TRUE;
      $super_menu_output = '<span class="supermenu-icon">' . $icon_active . $icon_default . '</span>';
      $element['#title'] = '<span class="supermenu-title">' . $element['#title'] . '</span>';
    }
  }

  // If menu is main menu^ then attach super menu
  if (($element['#theme'] == 'menu_link__main_menu') ||
    ($element['#theme'] == 'menu_link__menu_main_men_ru_') ||
    ($element['#theme'] == 'menu_link__menu_main_menu_de_') ||
    ($element['#theme'] == 'menu_link__menu_main_menu_ua_')) {

    global $user;

    // if this item menu is not have parent
    if (!$element['#original_link']['plid']) {
      $element['#attributes']['class'][] = 'parent';
    }

    // custom wrapper for poster link
    if (($mlid == 351) ||
      ($mlid == 2167) ||
      ($mlid == 2158) ||
      ($mlid == 2163)) {
      $element['#attributes']['class'][] = 'expanded';
      $dgf_tmp_1 = drupal_get_form('daway_poster_search_form');
      $sub_menu = '<ul class="menu"><li class="form">' . drupal_render($dgf_tmp_1) . '</li></ul>';
    }

    // custom wrapper for "IN CITY" link
    if (($mlid == 324) ||
      ($mlid == 2157) ||
      ($mlid == 2169) ||
      ($mlid == 2162)) {

      // if default user position not set
      if (!isset($_SESSION['DAWAY_DEFAULT_GEOPOSITION'])) {
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        $custom_style = array(
          'daway-profile' => array(
            'modalSize' => array(
              'width' => 'auto',
              'height' => 'auto',
              'type' => 'fixed',
              'contentRight' => 0,
              'contentBottom' => 0,
            ),
            'closeText' => t('Close'),
            'throbber' => theme('image', array(
              'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
              'alt' => t('Loading...'),
              'title' => t('Loading')
            )),
            'animation' => 'fadeIn',
            'animationSpeed' => 'fast',
          ),
        );
        drupal_add_js($custom_style, 'setting');
        $element['#localized_options']['attributes']['class'][] = 'ctools-modal-daway-profile';
        $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal';
        $element['#original_link']['link_path'] = 'js/nojs/profile/' . $user->uid . '/geoposition/only';
        $element['#href'] = 'js/nojs/profile/' . $user->uid . '/geoposition/only';
      }
      //alse read and append position to link
      else {
        $element['#original_link']['link_path'] = 'geopoint/' . $_SESSION['DAWAY_DEFAULT_GEOPOSITION'];
        $element['#localized_options']['query'] = array('only' => 1);
        $element['#href'] = 'geopoint/' . $_SESSION['DAWAY_DEFAULT_GEOPOSITION'];
      }

      // append form
      $element['#attributes']['class'][] = 'expanded';
      $dgf_tmp_2 = drupal_get_form('daway_trip_city_form');
      $sub_menu = '<ul class="menu"><li class="form">' . drupal_render($dgf_tmp_2) . '</li></ul>';
    }

    // custom wrapper for create trip link
    if (($mlid == 353) ||
      ($mlid == 2166) ||
      ($mlid == 2160) ||
      ($mlid == 2155)) {

      // if user is not anonumouse show create link
      if (!user_is_anonymous()) {
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();

        $custom_style = array(
          'daway-trip-admin' => array(
            'modalSize' => array(
              'width' => 'auto',
              'height' => 'auto',
              'type' => 'fixed',
              'contentRight' => 0,
              'contentBottom' => 0,
            ),
            'closeText' => t('Close'),
            'animation' => 'slideDown',
            'animationSpeed' => 'fast',
            'throbber' => theme('image', array(
              'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
              'alt' => t('Loading...'),
              'title' => t('Loading')
            )),
          ),
        );
        drupal_add_js($custom_style, 'setting');

        $element['#localized_options']['attributes']['class'][] = 'ctools-modal-daway-trip-admin';
        $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal';
        $element['#original_link']['link_path'] = 'js/nojs/trips/0/createlist';
        $element['#href'] = 'js/nojs/trips/0/createlist';
      }
      // else show login link
      else {
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        drupal_add_library('system', 'drupal.ajax');

        $custom_style = array(
          'daway-authorization' => array(
            'modalSize' => array(
              'width' => 'auto',
              'height' => 'auto',
              'type' => 'fixed',
              'contentRight' => 0,
              'contentBottom' => 0,
            ),
            'closeText' => t('Close'),
            'animation' => 'slideDown',
            'animationSpeed' => 'fast',
            'throbber' => theme('image', array(
              'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
              'alt' => t('Loading...'),
              'title' => t('Loading')
            )),
          ),
        );
        drupal_add_js($custom_style, 'setting');

        $element['#localized_options']['attributes']['class'][] = 'ctools-modal-daway-authorization';
        $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal';
        $element['#original_link']['link_path'] = 'js/nojs/authorization';
        $element['#href'] = 'js/nojs/authorization';
      }
    }

  }

  // change user login link
  if ($element['#theme'] == 'menu_link__user_menu') {
    if ($mlid == 981) {
      if (!user_is_anonymous()) {
        return;
      }
      ctools_include('ajax');
      ctools_include('modal');
      ctools_modal_add_js();
      drupal_add_library('system', 'drupal.ajax');

      $custom_style = array(
        'daway-authorization' => array(
          'modalSize' => array(
            'width' => 'auto',
            'height' => 'auto',
            'type' => 'fixed',
            'contentRight' => 0,
            'contentBottom' => 0,
          ),
          'closeText' => t('Close'),
          'animation' => 'slideDown',
          'animationSpeed' => 'fast',
          'throbber' => theme('image', array(
            'path' => drupal_get_path('theme', 'develix') . '/images/ajax.gif',
            'alt' => t('Loading...'),
            'title' => t('Loading')
          )),
        ),
      );
      drupal_add_js($custom_style, 'setting');

      $element['#localized_options']['attributes']['class'][] = 'ctools-modal-daway-authorization';
      $element['#localized_options']['attributes']['class'][] = 'ctools-use-modal';
      $element['#original_link']['link_path'] = 'js/nojs/authorization';
      $element['#href'] = 'js/nojs/authorization';
    }
  }

  $output = l($super_menu_output . $element['#title'], $element['#href'], $element['#localized_options']);

  $element['#attributes']['data-mlid'] = $mlid;

  return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>";
}


function develix_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  if (isset($element['#title_attributes'])) {
    $attributes = array_merge($attributes, $element['#title_attributes']);
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array(
    '!title' => $title,
    '!required' => $required
  )) . "</label>\n";
}




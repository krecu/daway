<?php
ob_start();

define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


$type = isset($_GET['type']) ? $_GET['type'] : FALSE;
$date = isset($_GET['date']) ? $_GET['date'] : FALSE;
$dateend = isset($_GET['dateplus']) ? $_GET['dateplus'] : FALSE;
$price = isset($_GET['price']) ? $_GET['price'] : FALSE;
$seats = isset($_GET['seats']) ? $_GET['seats'] : FALSE;
$extra = isset($_GET['extra']) ? $_GET['extra'] : FALSE;
$baggage = isset($_GET['baggage']) ? $_GET['baggage'] : FALSE;
$path = isset($_GET['path']) ? $_GET['path'] : FALSE;
$subtype = isset($_GET['subtype']) ? $_GET['subtype'] : FALSE;
$r1 = isset($_GET['r1']) ? $_GET['r1'] : FALSE;
$r2 = isset($_GET['r2']) ? $_GET['r2'] : FALSE;
$only = isset($_GET['only']) ? $_GET['only'] : FALSE;


if (isset($_GET['display'])) {
  switch ($_GET['display']) {
    case 'inline' :
      $display = 'viewsmall';
      break;
    case 'grid' :
      $display = 'viewfull';
      break;
    case 'rss' :
      $display = 'rss';
      break;
    default :
      $display = 'viewsmall';
      break;
  }
}

// добавляем фильтр в список фильтров пользователя
if ($path && ($display != 'rss')) {

  $path = explode('/', $path);
  $path = $path[0] . '/' . $path[1];

  drupal_session_start();
  $user_filters = array();
  $user_filters['type'] = $type;
  $user_filters['date'] = $date; //+
  $user_filters['dateend'] = $dateend; //+
  $user_filters['price'] = $price; //+
  $user_filters['seats'] = $seats; //+
  $user_filters['display'] = $display; //+
  $user_filters['extra'] = $extra; //+
  $user_filters['baggage'] = $baggage; //+
  $user_filters['r1'] = $r1; //+
  $user_filters['r2'] = $r2; //+
//	$user_filters['only'] = $only; //+ not sense
  $_SESSION['USER_FILTERS'][$path] = $user_filters;
}

// собираем все поездки
$query = db_select('node', 'n')->fields('n', array('nid', 'title', 'uid'))->condition('n.type', 'trip')
  ->extend('PagerDefault');

// собираем все поля направления в поездках
$query->innerJoin('field_data_field_trip_routes', 'route_field', 'route_field.entity_id = n.nid');

// собираем все типы направлений в поездках
$query->innerJoin('field_data_field_ftr_type', 'route_type', 'route_type.entity_id = route_field.field_trip_routes_value');
$query->addField('route_type', 'field_ftr_type_value', 'type');

// собираем все направления по полям
$query->innerJoin('field_data_field_ftr_route', 'route_id', 'route_id.entity_id = route_field.field_trip_routes_value');
$query->innerJoin('routes', 'r', 'r.rid = route_id.field_ftr_route_target_id');
$query->addField('r', 'name', 'name');
$query->addField('r', 'rid', 'rid');

// собираем поля дат поездок
$query->innerJoin('field_data_field_ftr_datedeparture', 'date', 'date.entity_id = route_field.field_trip_routes_value');
$query->addField('date', 'field_ftr_datedeparture_value', 'datestart');
$query->addField('date', 'field_ftr_datedeparture_value2', 'dateend');

// собираем поля цен поездок
$query->innerJoin('field_data_field_ftr_price', 'price', 'price.entity_id = route_field.field_trip_routes_value');
$query->addField('price', 'field_ftr_price_value', 'price');

// собираем пользователей
$query->innerJoin('users', 'user', 'user.uid = n.uid');

// собираем значения рейтинга пользователей
$query->innerJoin('field_data_field_user_rate', 'rate', 'rate.entity_id = user.uid');

// собираем EXTRA поля
$query->leftJoin('field_data_field_trip_extra', 'extra_field', 'extra_field.entity_id = n.nid');

// собираем значение типа поездки
$query->innerJoin('field_data_field_trip_type', 'type', 'type.entity_id = n.nid');


switch ($type) {

  // если поиск производится для геопоита то нам нужна только одна точка
  case 'geopoint' :

    if (isset($_GET['departure'])) {
      $pid = geopoint_load($_GET['departure']);
    }

    $radius = isset($_GET['r1']) ? $_GET['r1'] : daway_trip_get_default_radius($pid->pid);
    if ($only) {
      $query->innerJoin('geopoint_index', 'g', 'g.pid = r.pid1 OR g.pid = r.pid2');
      $query->condition('g.pid_original', $pid->pid);
      $query->condition('g.pid', $pid->pid, '!=');
      $query->innerJoin('geopoint', 'p1', 'p1.pid = g.pid');

//		if ((($pid->type != 'street_address') && ($pid->type != 'street_number') && ($pid->type != 'route'))) {
      // select points items by index
//			$query->innerJoin('geopoint', 'gi');
//			$query->innerJoin('geopoint_index', 'gii', 'gii.pid = gi.pid');
//			$query->condition('gii.pid_original', $pid->pid);
//			$query->innerJoin('geopoint', 'p1', '(p1.pid = r.pid1 OR p1.pid = r.pid2) AND p1.pid = gi.pid');

    }
    else {
      // select points items
      $query->innerJoin('geopoint', 'p1', 'p1.pid = r.pid1 OR p1.pid = r.pid2');
      $query->addExpression(
        '
        CEIL(
            sqrt(
                ((' . $pid->latitude . ' - p1.latitude)   * (' . $pid->latitude . ' - p1.latitude)) +
					((' . $pid->longitude . ' - p1.longitude) * (' . $pid->longitude . ' - p1.longitude))
				) * 100
			)
			'
        , 'distance');

      // Потомучто с алиасами работать можно только через GROUP BY HAVING и ORDER ))) 3 часа на решение ушло(((
      $query->havingCondition('distance', $radius, '<=');
    }

    break;

  // если поиск производится для направления то нам нужны обе точки
  case 'routes' :

    if (isset($_GET['departure'])) {
      $pid1 = geopoint_load($_GET['departure']);
    }

    if (isset($_GET['arrival'])) {
      $pid2 = geopoint_load($_GET['arrival']);
    }


    $r1 = isset($_GET['r1']) ? $_GET['r1'] : geopoint_get_radius($pid1->pid);
    $r2 = isset($_GET['r2']) ? $_GET['r2'] : geopoint_get_radius($pid2->pid);


    // собираем все геопоинты поездок по PID1
    $query->innerJoin('geopoint', 'p1', 'p1.pid = r.pid1');
    $query->addField('p1', 'pid', 'route_pid1');
    $query->addField('p1', 'short', 'route_name1');

    // вычисляем дистанцию от точки поиска до точки в базе
    $query->addExpression(
      '
      CEIL(
          sqrt(
              ((' . $pid1->latitude . ' - p1.latitude)   * (' . $pid1->latitude . ' - p1.latitude)) +
					((' . $pid1->longitude . ' - p1.longitude) * (' . $pid1->longitude . ' - p1.longitude))
				) * 100
			)
			'
      , 'distancetopid1');


    // собираем все геопоинты поездок по PID2
    $query->innerJoin('geopoint', 'p2', 'p2.pid = r.pid2');
    $query->addField('p2', 'pid', 'route_pid2');
    $query->addField('p2', 'short', 'route_name2');

    // вычисляем дистанцию от точки поиска до точки в базе
    $query->addExpression(
      '
      CEIL(
          sqrt(
              ((' . $pid2->latitude . ' - p2.latitude)   * (' . $pid2->latitude . ' - p2.latitude)) +
					((' . $pid2->longitude . ' - p2.longitude) * (' . $pid2->longitude . ' - p2.longitude))
				) * 100
			)
			'
      , 'distancetopid2');


    $query->havingCondition('distancetopid1', $r1, '<=');
    $query->havingCondition('distancetopid2', $r2, '<=');

    break;
}


// накладываем фильтр по датам
if ($dateend == 'all') {
  $query->condition('date.field_ftr_datedeparture_value', $date, '>=');
}
else {
  $query->condition('date.field_ftr_datedeparture_value', array($date, $date + $dateend * 84000), 'BETWEEN');
}

// накладываем фильтр по цене
if ($price) {
  $price = explode(',', $price);
  if (!(($price[0] == 0) && ($price[1] == 0))) {
    $query->condition('price.field_ftr_price_value', array($price[0], $price[1]), 'BETWEEN');
  }
}

// накладываем фильтр по количеству свободных мест
if ($seats) {
  $query->innerJoin('field_data_field_trip_passenger', 'seat', 'seat.entity_id = n.nid');
  $query->condition('seat.field_trip_passenger_value', $seats);
}

// накладываем фильтр по EXTRA полям
if ($extra = explode(',', $extra)) {
  if (count($extra) > 0) {
    foreach ($extra as $extra_field) {
      if ($extra_field == '') {
        continue;
      }
      $extra_field = explode(':', $extra_field);
      $extra_field_val = $extra_field[1];
      $extra_field = $extra_field[0];
      $query->innerJoin('field_data_' . $extra_field, $extra_field . '_field', $extra_field . '_field.entity_id = extra_field.field_trip_extra_value');
      $query->condition($extra_field . '_field.' . $extra_field . '_value', $extra_field_val);
    }
  }
}


// накладываем фильтр по багажу
if ($baggage) {
  if ($baggage != 'all') {
    $query->innerJoin('field_data_field_fte_baggage', 'baggage_field', 'baggage_field.entity_id = extra_field.field_trip_extra_value');
    $query->condition('baggage_field.field_fte_baggage_value', $baggage);
  }
}

if ($subtype) {
  switch ($subtype) {
    case 'drivers' :
      $query->condition('type.field_trip_type_value', 'driver');
      break;
    case 'passengers' :
      $query->condition('type.field_trip_type_value', 'passenger');
      break;
    default :
      drupal_not_found();
  }
}

switch ($_GET['sort_by']) {
  case 'date' :
    $query->orderBy('date.field_ftr_datedeparture_value', $_GET['sort_order']);
    break;
  case 'rating' :
    $query->orderBy('rate.field_user_rate_value', $_GET['sort_order']);
    break;
  case 'price' :
    $query->orderBy('price.field_ftr_price_value', $_GET['sort_order']);
    break;
  case 'distance' :
    $userlatitude = $_GET['user_lat'];
    $userlongitude = $_GET['user_lng'];


    $query->addExpression(
      '
          sqrt(
              ((' . $userlatitude . ' - p1.latitude)   * (' . $userlatitude . ' - p1.latitude)) +
					((' . $userlongitude . ' - p1.longitude) * (' . $userlongitude . ' - p1.longitude))
				) * 100
			'
      , 'userdistance');


    $query->orderBy('userdistance', $_GET['sort_order']);
    break;
}

$query->groupBy('datestart');
$query->groupBy('route_field.field_trip_routes_value');

$result = $query->limit(variable_get('DAWAY_GEOPOINT_LIST_PERPAGE', 20))->execute();

if ($display != 'rss') {
  $trips = array();

  foreach ($result as $key => $item) {
    $node = node_load($item->nid);
    $data = array(
      'rid' => $item->rid,
      'date' => $item->datestart,
      'price' => $item->price,
    );
    if ($_GET['sort_by'] == 'distance') {
      $data['geo'] = array(
        'user_lat' => $userlatitude,
        'user_lng' => $userlongitude,
        'userdistance' => $item->userdistance,
      );
    }

    $node->route = $data;
    $class = array();
    $class[] = 'views-row';
    $trips[] = render(node_view($node, $display));
  }

  $data = array(
    'results' => '<div>' . implode('', $trips) . theme('pager') . '</div>',
    'refresh' => '',
  );


  drupal_json_output($data);

}
else {
  $xml = new SimpleXMLElement('<rss version="2.0"/>');

  if (isset($_GET['departure'])) {
    $pid1 = geopoint_load($_GET['departure']);
  }

  if (isset($_GET['arrival'])) {
    $pid2 = geopoint_load($_GET['arrival']);
  }


  if ($type != 'geopoint') {
    $route = routes_load_by_geopoint($pid1->pid, $pid2->pid);
    $channel = $xml->addChild('channel');
    $channel->addChild('path', url('routes/' . $route->rid, array('absolute' => TRUE)));
    $channel->addChild('title', $route->name);
  }
  else {
    $geopoint = geopoint_load($pid1->pid);
    $channel = $xml->addChild('channel');
    $channel->addChild('path', url('geopoint/' . $geopoint->pid, array('absolute' => TRUE)));
    $channel->addChild('title', $geopoint->name);
  }
  foreach ($result as $key => $item) {
    $node = node_load($item->nid);
    $route = $channel->addChild('route');
    $r = routes_load($item->rid);
    $route->addChild('title', routes_get_normalise_name($r, ' -> '));
    $route->addChild('date', format_date($item->datestart, 'custom', 'Y-m-d'));
    $route->addChild('price', $item->price . ' USD');
    $route->addChild('path', url('routes/' . $item->rid, array('absolute' => TRUE)));
  }
  Header('Content-type: text/xml');
  print($xml->asXML());
}


$length = ob_get_length();
header('Content-Length: ' . $length . "\r\n");
header('Accept-Ranges: bytes' . "\r\n");
ob_end_flush();
<?php
header('Content-type: application/json; charset=utf-8');

if (isset($_GET['op'])) {
  switch ($_GET['op']) {
    case 'geocode' :
      $language = $_GET['ln'];
      $arg = $_GET['arg'];
      $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($arg) . '&sensor=false&language=' . $language);
      print $geocode;
      break;
    case 'check' :
      $geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=california&sensor=false');
      print $geocode;
      break;
    case 'distancematrix' :
      $p1 = $_GET['p1'];
      $p2 = $_GET['p2'];
      $geocode = file_get_contents('http://maps.googleapis.com/maps/api/distancematrix/json?origins=' . urlencode($p1) . '&destinations=' . urlencode($p2) . '&sensor=false');
      print $geocode;
      break;
  }
}
exit();